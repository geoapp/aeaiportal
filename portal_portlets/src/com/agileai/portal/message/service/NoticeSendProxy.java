package com.agileai.portal.message.service;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(serviceName = "NoticeSend",targetNamespace = "http://service.message.portal.agileai.com/")
public interface NoticeSendProxy {
	
	@WebMethod
	public void sendByIds(@WebParam(name="userIds")List<String> userIds, @WebParam(name="msgTheme")String msgTheme, @WebParam(name="msgContent")String msgContent, @WebParam(name="senderId")String senderId);

	@WebMethod  
	public void sendByRoles(@WebParam(name="roleIds")List<String> roleIds, @WebParam(name="msgTheme")String msgTheme, @WebParam(name="msgContent")String msgContent, @WebParam(name="senderId")String senderId);
	
	@WebMethod  
	public void sendByGroups(@WebParam(name="groupIds")List<String> groupIds, @WebParam(name="msgTheme")String msgTheme, @WebParam(name="msgContent")String msgContent, @WebParam(name="senderId")String senderId);

	@WebMethod
	public void kickoutOnline(@WebParam(name="userId")String userId,@WebParam(name="loginTime")String loginTime);
}
