package com.agileai.portal.message;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnection;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.common.AppConfig;

public class MessageHandler {
	private Logger logger = Logger.getLogger(MessageHandler.class);
	
	public static class MessageType{
		public static final String NOTICE_MSG = "notice";
		public static final String KICKOUT_MSG = "kickout";
	}
	
	private PooledConnection connection = null;
	private boolean transacted = false;
	private boolean durable = false;
	private String clientID = null;	
	private int ackMode = Session.AUTO_ACKNOWLEDGE;
	
	public MessageHandler(AppConfig appConfig){
		try {
			String url = appConfig.getConfig("GlobalConfig", "activeMqURL");
			
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
			PooledConnectionFactory pooledFactory = new PooledConnectionFactory(connectionFactory);
			this.connection = (PooledConnection) pooledFactory.createConnection();	
			if (durable && clientID != null) {
				connection.setClientID(clientID);
			}
			connection.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void publishNotice(String[] usercodes) {
		if (usercodes == null || usercodes.length == 0) {
			return;
		}
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("type",MessageType.NOTICE_MSG);
			String textMessage = jsonObject.toString();
			Session session = connection.createSession(transacted, ackMode);
			Message message = session.createTextMessage(textMessage);
			
			for (String usercode : usercodes) {
				Destination topic = session.createTopic(usercode);
				MessageProducer producer = session.createProducer(topic);
				producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				producer.send(message);
			}
			
			session.close();
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
		}
	}
	
	public void publishKickout(String usercode,String loginTime){
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("type",MessageType.KICKOUT_MSG);
			jsonObject.put("loginTime",loginTime);
			String textMessage = jsonObject.toString();
			
			Session session = connection.createSession(transacted, ackMode);
			Message message = session.createTextMessage(textMessage);
			
			Destination topic = session.createTopic(usercode);
			MessageProducer producer = session.createProducer(topic);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			producer.send(message);
			
			session.close();
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
		}		
	}
	
	public void release() throws JMSException{
		if (this.connection != null){
			this.connection.close();
		}
	}	
}
