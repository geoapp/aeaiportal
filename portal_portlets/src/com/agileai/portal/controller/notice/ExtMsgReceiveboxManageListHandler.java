package com.agileai.portal.controller.notice;

import java.util.List;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import com.agileai.domain.*;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.notice.ExtMsgReceiveboxManage;

public class ExtMsgReceiveboxManageListHandler
        extends StandardListHandler {
	
    public ExtMsgReceiveboxManageListHandler() {
        super();
        this.editHandlerClazz = ExtMsgReceiveboxManageEditHandler.class;
        this.serviceId = buildServiceId(ExtMsgReceiveboxManage.class);
    }
    
    @PageAction
 	public ViewRenderer displayWin(DataParam param){
     	String userCode = request.getParameter("userCode");
     	param.put("userCode", userCode);
 		List<DataRow> dataList = getService().findWinRecords(param);
 		DataRow dataTotal = getService().findWinTotal(param);
 		
 		JSONArray jsonArray = new JSONArray();
 		if (dataList != null && dataList.size() > 0) {
 			for (int i = 0; i < dataList.size(); i++) {
 				
 				DataRow dataLine = dataList.get(i);
 				JSONObject jsonObject = new JSONObject();
 				jsonObject.put("id", dataLine.stringValue("RECEIVE_ID"));
 				jsonObject.put("theme", dataLine.stringValue("MSG_THEME"));
 				jsonObject.put("time", dataLine.stringValue("MSG_CREATE_TIME"));
 				jsonArray.add(jsonObject);
 			}
 		}
 		
 		JSONObject json = new JSONObject();
 		json.put("total", dataTotal.stringValue("TOTAL"));
 		json.put("list", jsonArray);
 		
 		return new AjaxRenderer(json.toString());
 	}

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "msgTheme", "");
    }

    protected ExtMsgReceiveboxManage getService() {
        return (ExtMsgReceiveboxManage) this.lookupService(this.getServiceId());
    }
}
