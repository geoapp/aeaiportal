package com.agileai.portal.controller.grid;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.grid.Grid4boxConfigManage;
import com.agileai.util.ListUtil;

public class Grid4boxConfigManageEditHandler
        extends StandardEditHandler {
    public Grid4boxConfigManageEditHandler() {
        super();
        this.listHandlerClass = Grid4boxConfigManageListHandler.class;
        this.serviceId = buildServiceId(Grid4boxConfigManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("EXISTS_CALLBACK",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("EXISTS_CALLBACK",
                                                                               "N")));
        this.setAttribute("WIDTH",this.getAttributeValue("WIDTH","700"));
        this.setAttribute("HEIGHT",this.getAttributeValue("HEIGHT","500"));
        this.setAttribute("PAGE_SIZE",this.getAttributeValue("PAGE_SIZE","15"));
        this.setAttribute("PAGE_SIZE_LIST",this.getAttributeValue("PAGE_SIZE_LIST","10,15,20,25,30"));
    }
	public ViewRenderer doCheckUniqueAction(DataParam param){
		String responseText = "";
		List<DataRow> records = getService().findRecords(param);
		if (!ListUtil.isNullOrEmpty(records)){
			responseText = defDuplicateMsg;
		}
		return new AjaxRenderer(responseText);
	}	
    protected Grid4boxConfigManage getService() {
        return (Grid4boxConfigManage) this.lookupService(this.getServiceId());
    }
}
