package com.agileai.portal.controller.grid;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.portal.bizmoduler.grid.Grid4boxConfigManage;

public class Grid4boxConfigManageListHandler
        extends StandardListHandler {
    public Grid4boxConfigManageListHandler() {
        super();
        this.editHandlerClazz = Grid4boxConfigManageEditHandler.class;
        this.serviceId = buildServiceId(Grid4boxConfigManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "gridName", "");
    }

    protected Grid4boxConfigManage getService() {
        return (Grid4boxConfigManage) this.lookupService(this.getServiceId());
    }
}
