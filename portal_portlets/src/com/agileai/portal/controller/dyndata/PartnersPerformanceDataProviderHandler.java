package com.agileai.portal.controller.dyndata;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.XmlUtil;

public class PartnersPerformanceDataProviderHandler extends SimpleHandler {
	public PartnersPerformanceDataProviderHandler () {
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		String resourceType = param.get("resourceType");
		PartnersPerformanceDataBuilder builder = new PartnersPerformanceDataBuilder();
		String datas = null;
		if(resourceType.equals("Histogram")){
			String date = param.get("year");
			String partner = param.get("partner");
			DataRow xmlRow = builder.getXmlData(date, partner);
			datas = this.buildHistogramXML(xmlRow,partner);
		}
		else if(resourceType.equals("PieChart")){
			String date = param.get("year");
			String partner = param.get("partner");
			DataRow xmlRow = builder.getXmlData(date, partner);
			datas = this.buildPieChartXML(xmlRow, partner);
		}
		else if(resourceType.equals("DataGrid")){
			String date = param.get("year");
			String partner = param.get("partner");
			DataSet gridSet = builder.getGridJsonData(date, partner);
			datas = this.buildDataGridJson(gridSet);
		}
		else if(resourceType.equals("BoxData")){
			String partner = param.get("partner");
			String classify = param.get("classify");
			DataSet boxSet = builder.getBoxJsonData(partner, classify);
			datas = this.buildBoxJson(boxSet);
		}
		else if(resourceType.equals("HtmlData")){
			String id = param.get("id");
			DataRow htmlRow = builder.getHtmlJsonData(id);
			datas = this.buildHtmlJson(htmlRow);
		}
		this.setAttribute("datas",datas);
		return new LocalRenderer(getPage());
	}
	
	private String buildHtmlJson(DataRow htmlRow){
		String result = null;
		String fields[] = {"ID","PER_PARTNER","TOTAL_DATE","ORG","CH_TASK","CH_REALITY","OPEN_TASK","OPEN_REALITY"};
		try {
			JSONObject object = new JSONObject ();
			for(int i=0;i<htmlRow.size();i++){
				object.put(fields[i], htmlRow.stringValue(fields[i]));
			}
			result = object.toString();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private String buildBoxJson(DataSet boxSet) {
		// TODO Auto-generated method stub
		String result = null;
		String fields[] = {"PER_PARTNER","TOTAL_DATE","ORG","TASK","REALITY"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<boxSet.size();i++){
				JSONObject object = new JSONObject();
				DataRow row = boxSet.getDataRow(i);
				for(int j=0;j<row.size();j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}
				jsonArray.put(object);
			}									
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	private String buildDataGridJson(DataSet gridSet) {
		// TODO Auto-generated method stub
		String result = null;
		String fields[] = {"ID","ORG","PER_PARTNER","CH_TASK","CH_REALITY","OPEN_TASK","OPEN_REALITY"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<gridSet.size();i++){
				DataRow row = gridSet.getDataRow(i);
				JSONObject object = new JSONObject();
				for(int j=0;j<row.size()-1;j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}	
				jsonArray.put(object);
			}
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	private String buildPieChartXML(DataRow xmlRow, String partner) {
		String count1 = xmlRow.stringValue("OPEN_TASK");			
		String count2 = xmlRow.stringValue("OPEN_REALITY");	
		String count3 = xmlRow.stringValue("OPEN_RATIO");
		String result = "";
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart");
		charElement.addAttribute("formatNumberScale","0");
		charElement.addAttribute("caption","合作伙伴开放业绩");
		charElement.addAttribute("clickURL", "javascript:doPopupWebPage('targetPage:03/PopupDataGridPartner.ptml,partner:"+partner+",classify:develop',{width:'900',height:'350'})");
		
		Element setElement1 = charElement.addElement("set");
		setElement1.addAttribute("value", count1);
		setElement1.addAttribute("label", "任务指标");
		
		Element setElement2 = charElement.addElement("set");
		setElement2.addAttribute("value", count2);
		setElement2.addAttribute("label", "实际完成");
		
		Element setElement3 = charElement.addElement("set");
		setElement3.addAttribute("value", count3);
		setElement3.addAttribute("label", "完成率(%)");
		result = document.asXML();
		return result;
	}
	private String buildHistogramXML(DataRow xmlRow, String partner) {
		// TODO Auto-generated method stub
		String count1 = xmlRow.stringValue("CH_TASK");			
		String count2 = xmlRow.stringValue("CH_REALITY");	
		String count3 = xmlRow.stringValue("CH_RATIO");
		String result = "";
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart");
		charElement.addAttribute("formatNumberScale","0");
		charElement.addAttribute("caption","合作伙伴收费业绩");
		charElement.addAttribute("clickURL", "javascript:doPopupWebPage('targetPage:03/PopupDataGridPartner.ptml,partner:"+partner+",classify:charge',{width:'900',height:'350'})");
		
		Element setElement1 = charElement.addElement("set");
		setElement1.addAttribute("value", count1);
		setElement1.addAttribute("label", "任务指标");
		
		Element setElement2 = charElement.addElement("set");
		setElement2.addAttribute("value", count2);
		setElement2.addAttribute("label", "实际完成");
		
		Element setElement3 = charElement.addElement("set");
		setElement3.addAttribute("value", count3);
		setElement3.addAttribute("label", "完成率 (%)");
		result = document.asXML();
		return result;
	}
}
