package com.agileai.portal.controller.dyndata;

import java.util.Date;

import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.util.DateUtil;

public class StaffChangeDataBuilder {
	private DataSet data = new DataSet();

	public StaffChangeDataBuilder(){
		String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL).substring(0,7);
		String lastDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(new Date(), DateUtil.MONTH, -1)).substring(0,7);
		String preDate =DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(new Date(), DateUtil.MONTH, -2)).substring(0,7);
		
		DataRow row1 = new DataRow("DATE",preDate,"BUS_NEWEMP","3","BUS_QUITS","1","BUS_PROMOTION","1","BUS_DEMOTION","2",
			    "SUB_NEWEMP","20","SUB_QUITS","5","SUB_PROMOTION","10","SUB_DEMOTION","3");
		DataRow row2 = new DataRow("DATE",lastDate,"BUS_NEWEMP","4","BUS_QUITS","2","BUS_PROMOTION","1","BUS_DEMOTION","3",
			    "SUB_NEWEMP","30","SUB_QUITS","4","SUB_PROMOTION","15","SUB_DEMOTION","7");
		DataRow row3 = new DataRow("DATE",currentDate,"BUS_NEWEMP","11","BUS_QUITS","2","BUS_PROMOTION","5","BUS_DEMOTION","3",
			    "SUB_NEWEMP","40","SUB_QUITS","5","SUB_PROMOTION","10","SUB_DEMOTION","2");
		
		data.addDataRow(row1);
		data.addDataRow(row2);
		data.addDataRow(row3);
	}
	
	public DataRow getXmlData(String date){
		DataRow xmlRow = new DataRow("BUS_NEWEMP","","BUS_QUITS","","BUS_PROMOTION","","BUS_DEMOTION","","SUB_NEWEMP","","SUB_QUITS","","SUB_PROMOTION","","SUB_DEMOTION","");
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			if (date.equals(row.get("DATE"))) {
				xmlRow = row;
			}
		}
		return xmlRow;
	}
	
	public DataSet getGridJsonData(String date){
		DataSet gridSet = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			if (date.equals(row.get("DATE"))) {
				DataRow row1 = new DataRow(),row2 = new DataRow(),row3 = new DataRow();
				row1.put("ROWID","1","DATE",date,"CLASSIFY","总部","NEWEMP",row.get("BUS_NEWEMP"),"QUITS",row.get("BUS_QUITS"),"PROMOTION",row.get("BUS_PROMOTION"),"DEMOTION",row.get("BUS_DEMOTION"));
				row2.put("ROWID","2","DATE",date,"CLASSIFY","分支机构","NEWEMP",row.get("SUB_NEWEMP"),"QUITS",row.get("SUB_QUITS"),"PROMOTION",row.get("SUB_PROMOTION"),"DEMOTION",row.get("SUB_DEMOTION"));
				String totalNEWEMP = String.valueOf(Integer.parseInt(row.getString("BUS_NEWEMP"))+Integer.parseInt(row.getString("SUB_NEWEMP")));
				String totalQUITS = String.valueOf(Integer.parseInt(row.getString("BUS_QUITS"))+Integer.parseInt(row.getString("SUB_QUITS")));
				String totalPROMOTION = String.valueOf(Integer.parseInt(row.getString("BUS_PROMOTION"))+Integer.parseInt(row.getString("SUB_PROMOTION")));
				String totalDEMOTION = String.valueOf(Integer.parseInt(row.getString("BUS_DEMOTION"))+Integer.parseInt(row.getString("SUB_DEMOTION")));
				row3.put("ROWID","3","DATE",date,"CLASSIFY","合计","NEWEMP",totalNEWEMP,"QUITS",totalQUITS,"PROMOTION",totalPROMOTION,"DEMOTION",totalDEMOTION);
				gridSet.addDataRow(row1);
				gridSet.addDataRow(row2);
				gridSet.addDataRow(row3);
			}		
		}
		return gridSet;
	}
	
	public DataSet getBoxJsonData(String classify){
		DataSet boxSet = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			DataRow boxRow = new DataRow();
			if(classify.equals("head")){
				boxRow.put("MONTH",row.get("DATE"),"NEWEMP", row.get("BUS_NEWEMP"),"QUITS",row.get("BUS_QUITS"),"PROMOTION",row.get("BUS_PROMOTION"),"DEMOTION",row.get("BUS_DEMOTION"));
			}
			else if(classify.equals("branch")){
				boxRow.put("MONTH",row.get("DATE"),"NEWEMP",row.get("SUB_NEWEMP"),"QUITS",row.get("SUB_QUITS"),"PROMOTION",row.get("SUB_PROMOTION"),"DEMOTION",row.get("SUB_DEMOTION"));
			}
			boxSet.addDataRow(boxRow);				
		}		
		return boxSet;
	}
}
