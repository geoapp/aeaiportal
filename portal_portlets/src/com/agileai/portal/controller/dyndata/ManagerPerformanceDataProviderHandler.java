package com.agileai.portal.controller.dyndata;



import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.XmlUtil;

public class ManagerPerformanceDataProviderHandler extends SimpleHandler {
	public ManagerPerformanceDataProviderHandler () {
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		String resourceType = param.get("resourceType");
		ManagerPerformanceDataBuilder builder = new ManagerPerformanceDataBuilder();
		String datas = null;
		if(resourceType.equals("Histogram")){
			String date = param.get("year");
			DataSet xmlSet = builder.getXmlData(date);
			datas = this.buildHistogramXML(xmlSet);
		}
		else if(resourceType.equals("PieChart")){
			String date = param.get("year");
			DataSet xmlSet = builder.getXmlData(date);
			datas = this.buildPieChartXML(xmlSet);
		}
		else if(resourceType.equals("DataGrid")){
			String date = param.get("year");
			DataSet gridSet = builder.getGridJsonData(date);
			datas = this.buildDataGridJson(gridSet);
		}
		else if(resourceType.equals("BoxData")){
			String classify = param.get("classify");
			DataSet boxSet = builder.getBoxJsonData(classify);
			datas = this.buildBoxJson(boxSet);
		}
		else if(resourceType.equals("HtmlData")){
			String id = param.get("id");
			DataRow htmlRow = builder.getHtmlJsonData(id);
			datas = this.buildHtmlJson(htmlRow);
		}
		this.setAttribute("datas",datas);
		return new LocalRenderer(getPage());
	}
	
	private String buildHtmlJson(DataRow htmlRow){
		String result = null;
		String fields[] = {"ID","MANAGER","DATE","DEPARTMENT","PLACE","CH_TASK","CH_REALITY"};
		try {
			JSONObject object = new JSONObject ();
			for(int i=0;i<htmlRow.size();i++){
				object.put(fields[i], htmlRow.stringValue(fields[i]));
			}
			result = object.toString();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private String buildBoxJson(DataSet boxSet) {
		String result = null;
		String fields[] = {"ID","DEPARTMENT","MANAGER","DATE","PLACE","PERFORMANCEDATA"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<boxSet.size();i++){
				JSONObject object = new JSONObject();
				DataRow row = boxSet.getDataRow(i);
				for(int j=0;j<row.size();j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}
				jsonArray.put(object);
			}									
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	private String buildDataGridJson(DataSet gridSet) {
		String result = null;
		String fields[] = {"ID","DATE","PLACE","MANAGER","DEPARTMENT","CH_TASK","CH_REALITY"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<gridSet.size();i++){
				DataRow row = gridSet.getDataRow(i);
				JSONObject object = new JSONObject();
				for(int j=0;j<row.size();j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}	
				jsonArray.put(object);
			}
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String buildPieChartXML(DataSet dataSet) {
		
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart");
		charElement.addAttribute("formatNumberScale","0");
		charElement.addAttribute("caption","实际完成情况");
		charElement.addAttribute("clickURL", "javascript:doPopupWebPage('targetPage:03/PopupDataGridManager.ptml,classify:reality',{width:'900',height:'350'})");
		
		for (int i = 0; i < dataSet.size(); i++) {
			DataRow row  = dataSet.getDataRow(i);
			String chreality = row.getString("CH_REALITY");
			String manager = row.getString("MANAGER");
			Element setElement1 = charElement.addElement("set");
			setElement1.addAttribute("value", chreality);
			setElement1.addAttribute("label", manager);
		}
		
		return document.asXML();
	}
	
	private String buildHistogramXML(DataSet dataSet) {
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart");
		charElement.addAttribute("formatNumberScale","0");
		charElement.addAttribute("caption","任务指标");
		charElement.addAttribute("clickURL", "javascript:doPopupWebPage('targetPage:03/PopupDataGridManager.ptml,classify:task',{width:'900',height:'350'})");
		
		for (int i = 0; i < dataSet.size(); i++) {
			DataRow row  = dataSet.getDataRow(i);
			String chtask = row.getString("CH_TASK");
			String manager = row.getString("MANAGER");
			Element setElement1 = charElement.addElement("set");
			setElement1.addAttribute("value", chtask);
			setElement1.addAttribute("label", manager);
		}
		
		return document.asXML();
	}
}
