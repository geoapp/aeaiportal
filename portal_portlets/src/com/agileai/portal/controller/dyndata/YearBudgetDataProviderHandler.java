package com.agileai.portal.controller.dyndata;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.XmlUtil;

public class YearBudgetDataProviderHandler extends SimpleHandler{
	
	public YearBudgetDataProviderHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param) {
		String resourceType = param.get("resourceType");
		YearBudgetDataBuilder builder = new YearBudgetDataBuilder();
		String datas = null;
		if (resourceType.equals("XMLData")){			
			String year = param.get("year");
			String chartType = param.get("chartType");
			DataRow xmlRow = builder.getXmlData(year,chartType);
			datas = this.buildChartXML(xmlRow,year,chartType);
		}			
		else if (resourceType.equals("DataGrid")){
			String year = param.get("year");
			DataSet gridSet = builder.getGridJsonData(year);
			datas = this.buildDataGridJson(gridSet);
		}
		else if (resourceType.equals("BoxData")){
			String year = param.get("year");
			String chartType = param.get("chartType");
			DataSet boxSet = builder.getBoxJsonData(year,chartType);
			datas = this.buildBoxJson(boxSet);
		}
		else if (resourceType.equals("HtmlData")){
			String id = param.get("id");
			DataRow htmlRow = builder.getHtmlJsonData(id);
			datas = this.buildHtmlJson(htmlRow);
		}
		this.setAttribute("datas",datas);
		return new LocalRenderer(getPage());
	}
	
	private String buildHtmlJson(DataRow row) {
		String result = null;
		String fields[] = {"ID","YEAR","PROJECT","BUDGET_IN","BUDGET_OUT","DESCRIPTION"};
		try {
			JSONObject object = new JSONObject();
			for(int i=0;i<row.size();i++){
				object.put(fields[i], row.stringValue(fields[i]));
			}
			result = object.toString();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private String buildDataGridJson(DataSet set) {
		String result = null;
		String fields[] = {"ID","YEAR","PROJECT","BUDGET_IN","BUDGET_OUT"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<set.size();i++){
				DataRow row = set.getDataRow(i);
				JSONObject object = new JSONObject();
				for(int j=0;j<row.size()-1;j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}	
				jsonArray.put(object);
			}
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String buildBoxJson(DataSet set) {
		String result = null;
		String fields[] = {"ID","YEAR","PROJECT","BUDGET"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<set.size();i++){
				JSONObject object = new JSONObject();
				DataRow row = set.getDataRow(i);
				for(int j=0;j<row.size();j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}
				jsonArray.put(object);
			}									
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private String buildChartXML(DataRow row,String year,String chartType){
		String count1 = row.stringValue("ERP_FLOW");			
		String count2 = row.stringValue("DOCK_FLOW");			
		String count3 = row.stringValue("WEB_FLOW");			
		String result = "";
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart");
		charElement.addAttribute("formatNumberScale","0");
		if(chartType.equals("line")){
			charElement.addAttribute("caption","收入预算");
		}else{
			charElement.addAttribute("caption","支出预算");
		}
		charElement.addAttribute("yAxisName","金额（万）");
		charElement.addAttribute("xAxisName","月份");
		charElement.addAttribute("showNames","1");
		charElement.addAttribute("clickURL", "javascript:doPopupWebPage('targetPage:03/YearBudgetGrid.ptml,year:"+year+",chartType:"+chartType+"',{width:'900',height:'350'})");
		
		Element setElement1 = charElement.addElement("set");
		setElement1.addAttribute("value", count1);
		setElement1.addAttribute("label", "01");
		
		Element setElement2 = charElement.addElement("set");
		setElement2.addAttribute("value", count2);
		setElement2.addAttribute("label", "02");
		
		Element setElement3 = charElement.addElement("set");
		setElement3.addAttribute("value", count3);
		setElement3.addAttribute("label", "03");
		
		result = document.asXML();
		return result;
	}
}