package com.agileai.portal.controller.bar;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.bar.ExtGeneralQueryConfigManage;

public class ExtGeneralQueryConfigManageListHandler
        extends StandardListHandler {
    public ExtGeneralQueryConfigManageListHandler() {
        super();
        this.editHandlerClazz = ExtGeneralQueryConfigManageEditHandler.class;
        this.serviceId = buildServiceId(ExtGeneralQueryConfigManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        initMappingItem("FIELD_TYPE",
                        FormSelectFactory.create("EXT_FIELD_TYPE").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "pageId", "");
        initParamItem(param, "portletId", "");
    }
    
    @PageAction
   	public ViewRenderer moveUp(DataParam param){
       	param.put("bigSortNoId",param.get("ID"));
       	List<DataRow> records = getService().findRecords(param);
       	if (records != null && records.size() <=1){
       		setErrorMsg("不能上移，请确认！");
       	}else{
       		List<DataParam> paramList = new ArrayList<DataParam>();
       		DataParam param0 = records.get(records.size()-1).toDataParam();
       		DataParam param1 = records.get(records.size()-2).toDataParam();
       		paramList.add(param0);
       		paramList.add(param1);
       		this.exchangeField(paramList,"SORT_NO");
       		getService().updateSortNO(paramList);
       	}
       	param.remove("bigSortNoId");
       	return prepareDisplay(param);
   	}
       @PageAction
   	public ViewRenderer moveDown(DataParam param){
       	param.put("smallSortNoId",param.get("ID"));
       	List<DataRow> records = getService().findRecords(param);
       	if (records != null && records.size() <=1){
       		setErrorMsg("不能下移，请确认！");
       	}else{
       		List<DataParam> paramList = new ArrayList<DataParam>();
       		DataParam param0 = records.get(0).toDataParam();
       		DataParam param1 = records.get(1).toDataParam();
       		paramList.add(param0);
       		paramList.add(param1);
       		this.exchangeField(paramList,"SORT_NO");
       		getService().updateSortNO(paramList);
       	}
       	param.remove("smallSortNoId");
       	return prepareDisplay(param);
   	}
       private void exchangeField(List<DataParam> paramList,String field){
       	DataParam param0 = paramList.get(0);
       	DataParam param1 = paramList.get(1);
       	String value0 = param0.get(field);
       	String value1 = param1.get(field);
       	param0.put(field,value1);
       	param1.put(field,value0);
       }

    protected ExtGeneralQueryConfigManage getService() {
        return (ExtGeneralQueryConfigManage) this.lookupService(this.getServiceId());
    }
}
