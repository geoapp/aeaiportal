package com.agileai.portal.controller.bar;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class ExtDefaultVariableAddHandler extends BaseHandler{

	public ExtDefaultVariableAddHandler() {
        super();
    }

	public ViewRenderer prepareDisplay(DataParam param){

		String idFields[] = {"BeginOfCurrentMonth","BeginOfCurrentYear","EndOfCurrentMonth","EndinOfCurrentYear","TheDayBeforeYesterday","Today","Yesterdy","Tomorrow","TheDayAfterTomorrow"};
		String nameFields[] = {"月初","年初","月末","年末","前天","今天","昨天","明天","后天"};
		List<DataRow> rsList = new ArrayList<DataRow>();
		for(int i=0; i<idFields.length; i++){
			DataRow row = new DataRow();
			row.put("DEFAULT_DATE_ID", idFields[i]);
			row.put("DEFAULT_DATE_NAME", nameFields[i]);
			rsList.add(row);
		}
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {
    	
    	String fieldType = param.get("fieldType");
    	setAttribute("fieldType", fieldType);
    }
}
