package com.agileai.portal.controller.bar;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.bar.ExtQueryBarConfigManage;

public class ExtQueryBarConfigManageListHandler
        extends StandardListHandler {
    public ExtQueryBarConfigManageListHandler() {
        super();
        this.editHandlerClazz = ExtQueryBarConfigManageEditHandler.class;
        this.serviceId = buildServiceId(ExtQueryBarConfigManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        initMappingItem("FIELD_TYPE",
                        FormSelectFactory.create("BAR_FIELD_TYPE").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "pageId", "");
        initParamItem(param, "portletId", "");
    }

    protected ExtQueryBarConfigManage getService() {
        return (ExtQueryBarConfigManage) this.lookupService(this.getServiceId());
    }
}
