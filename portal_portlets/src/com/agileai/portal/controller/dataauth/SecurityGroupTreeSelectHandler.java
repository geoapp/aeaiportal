package com.agileai.portal.controller.dataauth;

import java.util.List;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.portal.bizmoduler.dataauth.SecurityGroupTreeSelect;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;

public class SecurityGroupTreeSelectHandler
        extends TreeSelectHandler {
	
    public SecurityGroupTreeSelectHandler() {
        super();
        this.serviceId = buildServiceId(SecurityGroupTreeSelect.class);
        this.isMuilSelect = true;
        this.checkRelParentNode = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "GRP_ID",
                                                  "GRP_NAME", "GRP_PID");

        return treeBuilder;
    }

    protected SecurityGroupTreeSelect getService() {
        return (SecurityGroupTreeSelect) this.lookupService(this.getServiceId());
    }
}
