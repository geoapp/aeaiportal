package com.agileai.portal.portlets.notice;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;

import com.agileai.common.AppConfig;
import com.agileai.portal.driver.Resource;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.portlets.BaseMashupPortlet;
import com.agileai.portal.portlets.PortletCacheManager;

public class NoticePortlet extends BaseMashupPortlet {
	
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String time = preferences.getValue("time", null);
		String isHide = preferences.getValue("isHide", null);
		String immediateLoad = preferences.getValue("immediateLoad","false");

		request.setAttribute("time", time);
		request.setAttribute("isHide", isHide);
		request.setAttribute("userCode", request.getUserPrincipal().getName());
		request.setAttribute("portletId", request.getWindowID());
		
		AppConfig appConfig = (AppConfig)this.lookupService("appConfig");
    	Boolean enableKickoutPolicy = appConfig.getBoolConfig("GlobalConfig", "EnableKickoutPolicy");
    	request.setAttribute("enableKickoutPolicy", enableKickoutPolicy);
    	request.setAttribute("immediateLoad", Boolean.parseBoolean(immediateLoad));
    	
		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String dataURL = preferences.getValue("dataURL", null);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		String cacheMinutes = preferences.getValue("cacheMinutes", defaultCacheMinutes);
		String immediateLoad = preferences.getValue("immediateLoad", "false");
		
		String time = preferences.getValue("time", null);
		String isHide = preferences.getValue("isHide", null);
		
		request.setAttribute("dataURL", dataURL);
		request.setAttribute("defaultVariableValues", defaultVariableValues);
		request.setAttribute("isCache", isCache);
		request.setAttribute("cacheMinutes", cacheMinutes);
		request.setAttribute("immediateLoad",immediateLoad);
		
		request.setAttribute("time", time);
		request.setAttribute("isHide", isHide);
		
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String dataURL = request.getParameter("dataURL");
		String defaultVariableValues = request.getParameter("defaultVariableValues");
		String isCache = request.getParameter("isCache");
		String cacheMinutes = request.getParameter("cacheMinutes");
		
		String time = request.getParameter("time");
		String isHide = request.getParameter("isHide");
		String immediateLoad = request.getParameter("immediateLoad");

		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("defaultVariableValues", defaultVariableValues);
		preferWapper.setValue("dataURL", dataURL);
		preferWapper.setValue("isCache", isCache);
		preferWapper.setValue("cacheMinutes", cacheMinutes);

		preferWapper.setValue("time", time);
		preferWapper.setValue("isHide", isHide);
		preferWapper.setValue("immediateLoad", immediateLoad);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
	
	@Resource(id="getAjaxData")
	public void getAjaxData(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		String ajaxData = "";
		try {
			PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
			String cacheMinutes = (String)preferences.getValue("cacheMinutes",defaultCacheMinutes);
			String isCache = preferences.getValue("isCache", defaultIsCache);
			String dataURL = getDataURL(request);
			ajaxData = PortletCacheManager.getOnly().getCachedData(isCache, dataURL, cacheMinutes);
		} catch (Exception e) {
			this.logger.error("获取取数据失败getAjaxData", e);
		}
		PrintWriter writer = response.getWriter();
		writer.print(ajaxData);
		writer.close();
	}
}
