package com.agileai.portal.portlets.list;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;

import com.agileai.portal.driver.Resource;
import com.agileai.portal.driver.common.PortletVariableHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.portlets.BaseMashupPortlet;
import com.agileai.portal.portlets.PortletCacheManager;
import com.agileai.util.StringUtil;

public class MultiDataListPortlet extends BaseMashupPortlet {
	public static final String DefaultSpliter = ";";
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		
		String columnName = preferences.getValue("columnName", null);
		String isSetting = preferences.getValue("isSetting", null);
		String selectedAll = preferences.getValue("selectedAll", null);
		String idSpliter = preferences.getValue("idSpliter", DefaultSpliter);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		
		PortletVariableHelper variableHelper = new PortletVariableHelper(request,defaultVariableValues);
		String dataURL = preferences.getValue("dataURL", null);
		dataURL = variableHelper.getRealDataURL(dataURL);
		
		String listItemKey = variableHelper.getDefaultValue(dataURL,"listItemKey");
		String listItemValue = variableHelper.getDefaultValue(dataURL,listItemKey);
		if (listItemValue != null){
			listItemValue = listItemValue.replaceAll("\\+", " ");
		}
		request.setAttribute("selectedIds", listItemValue);
		
		selectedAll = variableHelper.getRealValue("selectedAll");
		request.setAttribute("selectedAll", selectedAll);
		
		request.setAttribute("idSpliter", idSpliter);
		request.setAttribute("columnName", columnName);
		request.setAttribute("isSetting", isSetting);
		request.setAttribute("listItemKey", listItemKey);
		request.setAttribute("defaultVariableValues", defaultVariableValues);
		
		request.setAttribute("portletId", request.getWindowID());
		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);

		String columnName = preferences.getValue("columnName", null);
		String idSpliter = preferences.getValue("idSpliter", DefaultSpliter);
		String dataURL = preferences.getValue("dataURL", null);
		String isSetting = preferences.getValue("isSetting", null);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		String cacheMinutes = preferences.getValue("cacheMinutes", defaultCacheMinutes);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		
		request.setAttribute("columnName", columnName);
		request.setAttribute("idSpliter", idSpliter);
		request.setAttribute("dataURL", dataURL);
		request.setAttribute("isSetting", isSetting);
		request.setAttribute("isCache", isCache);
		request.setAttribute("cacheMinutes", cacheMinutes);
		request.setAttribute("defaultVariableValues", defaultVariableValues);
		
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String columnName = request.getParameter("columnName");
		String idSpliter = request.getParameter("idSpliter");
		String dataURL = request.getParameter("dataURL");
		String isCache = request.getParameter("isCache");
		String cacheMinutes = request.getParameter("cacheMinutes");
		String defaultVariableValues = request.getParameter("defaultVariableValues");

		boolean isSetting = false;
		isSetting = StringUtil.isNotNullNotEmpty(dataURL) && StringUtil.isNotNullNotEmpty(columnName);
		
		PreferencesWrapper preferWrapper = new PreferencesWrapper();
		preferWrapper.setValue("columnName", columnName);
		preferWrapper.setValue("idSpliter", idSpliter);
		preferWrapper.setValue("dataURL", dataURL);
		preferWrapper.setValue("isSetting", String.valueOf(isSetting));
		preferWrapper.setValue("isCache", isCache);
		preferWrapper.setValue("cacheMinutes", cacheMinutes);
		preferWrapper.setValue("defaultVariableValues", defaultVariableValues);
		
		PreferencesHelper.savePublicPreferences(request, preferWrapper.getPreferences());
		response.setPortletMode(PortletMode.VIEW);
	}
	@Resource(id="getAjaxData")
	public void getAjaxData(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		String ajaxData = "";
		try {
			PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
			String cacheMinutes = (String)preferences.getValue("cacheMinutes",defaultCacheMinutes);
			String isCache = preferences.getValue("isCache", defaultIsCache);
			String dataURL = getDataURL(request);
			ajaxData = PortletCacheManager.getOnly().getCachedData(isCache, dataURL, cacheMinutes);
		} catch (Exception e) {
			this.logger.error("获取取数据失败getAjaxData", e);
		}
		PrintWriter writer = response.getWriter();
		writer.print(ajaxData);
		writer.close();
	}
}
