package com.agileai.portal.portlets.list;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;

import com.agileai.portal.driver.common.PortletVariableHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.portlets.BaseMashupPortlet;

public class OrgTreePortlet extends BaseMashupPortlet {
	
	@RenderMode(name="view")
	public void view(RenderRequest request, RenderResponse response) throws PortletException, IOException {
	
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String height = preferences.getValue("height", null);
		String namespace = preferences.getValue("namespace", null);
		String isSetting = preferences.getValue("isSetting", null);
		String mrpl = preferences.getValue("mrpl", "top");
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		String dataURL = preferences.getValue("dataURL", null);
		String defaultValueString = "namespace="+namespace+"&"+defaultVariableValues;
		PortletVariableHelper variableHelper = new PortletVariableHelper(request,defaultValueString);
		dataURL = variableHelper.getRealDataURL(dataURL);
		
		request.setAttribute("height", height);
		request.setAttribute("namespace", namespace);
		request.setAttribute("isSetting", isSetting);
		request.setAttribute("mrpl", mrpl);
		request.setAttribute("dataURL", dataURL);
		
		super.doView(request, response);
	}
	
	@RenderMode(name="edit")
	public void edit(RenderRequest request, RenderResponse response) throws PortletException, IOException {
	
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String height = preferences.getValue("height", null);
		String namespace = preferences.getValue("namespace", null);
		String isSetting = preferences.getValue("isSetting", null);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		String mrpl = preferences.getValue("mrpl","top");
		String cacheMinutes = preferences.getValue("cacheMinutes", defaultCacheMinutes);
		String dataURL = preferences.getValue("dataURL", null);
		String defaultVariableValues = preferences.getValue("defaultVariableValues", null);
		
		request.setAttribute("height", height);
		request.setAttribute("namespace", namespace);
		request.setAttribute("isSetting", isSetting);
		request.setAttribute("isCache", isCache);
		request.setAttribute("cacheMinutes", cacheMinutes);
		request.setAttribute("mrpl", mrpl);
		request.setAttribute("defaultVariableValues", defaultVariableValues);
		request.setAttribute("dataURL", dataURL);
		
		super.doEdit(request, response);
	}
	
	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException, IOException, PreferenceException {
		
		String height = request.getParameter("height");
		String namespace = request.getParameter("namespace");
		String isCache = request.getParameter("isCache");
		String cacheMinutes = request.getParameter("cacheMinutes");
		String mrpl = request.getParameter("mrpl");
		String defaultVariableValues = request.getParameter("defaultVariableValues");
		String dataURL = request.getParameter("dataURL");
		
		boolean isSetting = false;
		isSetting = !this.isEmpty(height);
		
		if (isSetting){
			isSetting = !this.isEmpty(namespace);
		}
		
		PreferencesWrapper preferWrapper = new PreferencesWrapper();
		
		preferWrapper.setValue("height", height);
		preferWrapper.setValue("namespace", namespace);
		preferWrapper.setValue("isSetting", String.valueOf(isSetting));
		preferWrapper.setValue("isCache", isCache);
		preferWrapper.setValue("cacheMinutes", cacheMinutes);
		preferWrapper.setValue("mrpl", mrpl);
		preferWrapper.setValue("defaultVariableValues", defaultVariableValues);
		preferWrapper.setValue("dataURL", dataURL);
		
		PreferencesHelper.savePublicPreferences(request, preferWrapper.getPreferences());
		response.setPortletMode(PortletMode.VIEW);
	}
	
	/**
	 * 验证是否为空
	 */
	private boolean isEmpty(String str){
		if (str == null || "".equals(str.trim())){
			return true;
		}else{
			return false;
		}
	}
}