package com.agileai.portal.portlets.simple;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderRequest;
import javax.portlet.ValidatorException;

import com.agileai.portal.driver.GenericPotboyPortlet;

public class ImagePortlet extends GenericPotboyPortlet {
    public void prepareView(RenderRequest request) {
    	PortletPreferences preferences = request.getPreferences();
		String imageSrc = preferences.getValue("src","10");
		String imageWidth = preferences.getValue("width","10");
		String imageHeight = preferences.getValue("height","10");
		
		request.setAttribute("imageSrc", imageSrc);
		request.setAttribute("imageWidth", imageWidth);
		request.setAttribute("imageHeight", imageHeight);
    }
    public void prepareEdit(RenderRequest request) {
    	PortletPreferences preferences = request.getPreferences();
		String imageSrc = preferences.getValue("src","10");
		String imageWidth = preferences.getValue("width","10");
		String imageHeight = preferences.getValue("height","10");
		
		request.setAttribute("imageSrc", imageSrc);
		request.setAttribute("imageWidth", imageWidth);
		request.setAttribute("imageHeight", imageHeight);
    }
    
    @ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException {
		String imageSrc = request.getParameter("imageSrc");
		String imageWidth = request.getParameter("imageWidth");
		String imageHeight = request.getParameter("imageHeight");
		PortletPreferences preferences = request.getPreferences();
		preferences.setValue("src", imageSrc);
		preferences.setValue("width", imageWidth);
		preferences.setValue("height", imageHeight);
		preferences.store();
		
		response.setPortletMode(PortletMode.VIEW);
	}
    @ProcessAction(name = "goBack")
	public void goBack(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException {
		response.setPortletMode(PortletMode.VIEW);
	}
}
