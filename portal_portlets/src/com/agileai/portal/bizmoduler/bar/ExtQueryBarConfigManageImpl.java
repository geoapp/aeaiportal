package com.agileai.portal.bizmoduler.bar;

import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class ExtQueryBarConfigManageImpl
        extends StandardServiceImpl
        implements ExtQueryBarConfigManage {
    private HashMap<String,List<DataRow>> barConfigCache = new HashMap<String,List<DataRow>>();
	
	public ExtQueryBarConfigManageImpl() {
        super();
    }
	
	public void updateRecord(DataParam param) {
		super.updateRecord(param);
		String pageId = param.get("PAGE_CODE");
		String portletId = param.get("PORTLET_CODE");
		String cacheKey = pageId + "!"+portletId;
		if (barConfigCache.containsKey(cacheKey)){
			barConfigCache.remove(cacheKey);
		}
	}
	public void deletRecord(DataParam param) {
		DataRow row = this.getRecord(param);
		String pageId = row.stringValue("PAGE_CODE");
		String portletId = row.stringValue("PORTLET_CODE");
		String cacheKey = pageId + "!"+portletId;
		if (barConfigCache.containsKey(cacheKey)){
			barConfigCache.remove(cacheKey);
		}
		super.deletRecord(param);
	}
	public List<DataRow> findRecords(DataParam param) {
		String pageId = param.get("pageId");
		String portletId = param.get("portletId");
		String cacheKey = pageId + "!"+portletId;
		if (barConfigCache.containsKey(cacheKey)){
			List<DataRow> records = barConfigCache.get(cacheKey);
			return records;
		}else{
			return super.findRecords(param);
		}
	}
}
