package com.agileai.portal.bizmoduler.notice;

import java.util.List;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class ExtMsgReceiveboxManageImpl
        extends StandardServiceImpl
        implements ExtMsgReceiveboxManage {
	
    public ExtMsgReceiveboxManageImpl() {
        super();
    }
    
	@Override
	public List<DataRow> findWinRecords(DataParam param) {
		return daoHelper.queryRecords(sqlNameSpace + "." + "findWinRecords", param);
	}

	@Override
	public DataRow findWinTotal(DataParam param) {
		return daoHelper.getRecord(sqlNameSpace + "." + "findWinTotal", param);
	}

	@Override
	public List<DataRow> findRemindRecords(DataParam param) {
		return daoHelper.queryRecords(sqlNameSpace + "." + "findRemindRecords", param);
	}
}
