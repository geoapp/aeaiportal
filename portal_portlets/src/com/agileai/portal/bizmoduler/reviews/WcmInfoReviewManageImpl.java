package com.agileai.portal.bizmoduler.reviews;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.portal.bizmoduler.reviews.WcmInfoReviewManage;

public class WcmInfoReviewManageImpl
        extends StandardServiceImpl
        implements WcmInfoReviewManage {
    public WcmInfoReviewManageImpl() {
        super();
    }

	@Override
	public DataRow getInfoRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getInfoRecord";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
}
