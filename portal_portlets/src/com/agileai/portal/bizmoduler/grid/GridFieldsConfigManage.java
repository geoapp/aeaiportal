package com.agileai.portal.bizmoduler.grid;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface GridFieldsConfigManage
        extends StandardService {
	public int getMaxSortNO(DataParam param);
	public void updateSortNO(List<DataParam> params);
}
