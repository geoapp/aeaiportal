package com.agileai.portal.bizmoduler.address;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface ContactQuery extends StandardService {
	List<DataRow> findContactRecords(DataParam param);
	DataRow queryCurrentRecord(DataParam param);
}
