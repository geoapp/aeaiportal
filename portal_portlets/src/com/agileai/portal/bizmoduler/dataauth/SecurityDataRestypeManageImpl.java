package com.agileai.portal.bizmoduler.dataauth;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.portal.bizmoduler.dataauth.SecurityDataRestypeManage;

public class SecurityDataRestypeManageImpl
        extends StandardServiceImpl
        implements SecurityDataRestypeManage {
    public SecurityDataRestypeManageImpl() {
        super();
    }

	@Override
	public DataRow getRecordByResType(DataParam param) {
		DataRow resTypeRow = this.daoHelper.getRecord("SecurityDataRestype.getRecordByResType", param);
		return resTypeRow;
	}
}
