<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String fieldType = (String)pageBean.getAttribute("fieldType");
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>添加默认值</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function setSelectedValue(value){
	ele('variableValue').value = value;
}
function doSubmit(){
	var fieldType = "<%=fieldType%>";
	if (fieldType=="SELECT"||fieldType=="CHECKBOX"){
		parent.afterAddDefaultValue(ele('variableName').value,ele('variableValue').value);
	}else{
		var name = "text";
		parent.afterAddDefaultValue(name,ele('variableValue').value);
	}
}
</script>
</head>
<body>
<%@include file="/jsp/inc/message.inc.jsp"%>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doSubmit()"><input value="&nbsp;" type="button" class="saveImgBtn" title="确定" />确定</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="B" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="取消" />取消</td>        
</tr>
</table>
</div>
<div>
<table class="queryTable">
  <tr>
  <%if(fieldType.equals("SELECT")||fieldType.equals("CHECKBOX")){ %>
    <td>元素编码：</td>
	<td><input id="variableName" label="元素名称" name="variableName" type="text" value="" size="24" class="text" /></td>
  </tr>
  <tr>
	<td>元素名称：</td>
	<td><input id="variableValue" label="元素值" name="variableValue" type="text" value="" size="24" class="text" /></td>
  <%}else{ %>
    <td>默认值：</td>
	<td><input id="variableValue" label="默认值" name="variableValue" type="text" value="" size="24" class="text" /></td>
  <%} %>
  </tr>
</table>
</div>
<%if(fieldType.equals("DATEPICK")){ %>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="省份选择.csv"
retrieveRowsCallback="process" xlsFileName="省份选择.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="auto" 
>
<ec:row styleClass="odd" ondblclick="setSelectedValue('${row.DEFAULT_DATE_ID}')">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="DEFAULT_DATE_NAME" title="内置日期默认值"/>
</ec:row>
</ec:table>
<%} %>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>