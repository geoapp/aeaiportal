<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>查询BAR扩展定义</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="CODE" label="编码" name="CODE" type="text" value="<%=pageBean.inputValue("CODE")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="NAME" label="名称" name="NAME" type="text" value="<%=pageBean.inputValue("NAME")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>字段类型</th>
	<td><select id="FIELD_TYPE" label="字段类型" name="FIELD_TYPE" class="select" onchange="showValidTr(this.value)"><%=pageBean.selectValue("FIELD_TYPE")%></select>
</td>
</tr>
<tr id="TRIGER_CASCADE_TR">
	<th width="100" nowrap>触发级联</th>
	<td>&nbsp;<%=pageBean.selectRadio("TRIGER_CASCADE")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>是否换行</th>
	<td>&nbsp;<%=pageBean.selectRadio("BREAK_LINE")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>显示长度</th>
	<td><input id="DISPLAY_LENGTH" label="显示长度" name="DISPLAY_LENGTH" type="text" value="<%=pageBean.inputValue("DISPLAY_LENGTH")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>排序</th>
	<td><input id="SORT_NO" label="排序" name="SORT_NO" type="text" value="<%=pageBean.inputValue("SORT_NO")%>" size="32" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="ID" name="ID" value="<%=pageBean.inputValue4DetailOrUpdate("ID","")%>" />
<input type="hidden" id="PAGE_CODE" name="PAGE_CODE" value="<%=pageBean.inputValue("PAGE_CODE")%>" />
<input type="hidden" id="PORTLET_CODE" name="PORTLET_CODE" value="<%=pageBean.inputValue("PORTLET_CODE")%>" />
</form>
<script language="javascript">
function showValidTr(fieldTypeValue){
	if (fieldTypeValue=="SELECT"){
		$("#TRIGER_CASCADE_TR").css({display:""});
	}
	else {
		$("#TRIGER_CASCADE_TR").css({display:"none"});		
	}
}
showValidTr($("#FIELD_TYPE").val());

requiredValidator.add("CODE");
requiredValidator.add("NAME");
requiredValidator.add("FIELD_TYPE");
requiredValidator.add("DISPLAY_LENGTH");
intValidator.add("DISPLAY_LENGTH");
requiredValidator.add("SORT_NO");
intValidator.add("SORT_NO");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
