<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>弹出表格管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link rel="stylesheet" href="/portal/css/boxy.css" type="text/css" />
<script src="/portal/js/jquery.boxy.js" language="javascript"></script>
<script src="/portal/js/jquery.boxy.iframe.js" language="javascript"></script>
<script language="javascript">
function popupGridFieldsConfig(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	var gridId = $('#ID').val();
	var boxTitle = "表格字段配置";
	var targetURL = "index?Grid4boxFieldsConfigManageList&gridId="+gridId
	qBox.iFLoad({title:boxTitle,scrolling:'auto',src:targetURL,center:true,draggable: false,w:800,h:440});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="popupGridFieldsConfig()"><input value="&nbsp;" title="字段配置" type="button" class="relateImgBtn" />字段配置</td>    
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable">
<tr><td>
&nbsp;名称<input id="gridName" label="名称" name="gridName" type="text" value="<%=pageBean.inputValue("gridName")%>" size="10" class="text" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="弹出表格管理.csv"
retrieveRowsCallback="process" xlsFileName="弹出表格管理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,{ID:'${row.ID}'});refreshConextmenu()" onclick="selectRow(this,{ID:'${row.ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="CODE" title="编码"   />
	<ec:column width="100" property="NAME" title="名称"   />
	<ec:column width="100" property="WIDTH" title="宽度"   />
	<ec:column width="100" property="HEIGHT" title="高度"   />
	<ec:column width="100" property="PAGE_SIZE" title="每页行数"   />
	<ec:column width="100" property="EXISTS_CALLBACK" title="双击回调"   />
</ec:row>
</ec:table>
<input type="hidden" name="ID" id="ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
