<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>弹出表格管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save',checkUnique:'y'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="CODE" label="编码" name="CODE" type="text" value="<%=pageBean.inputValue("CODE")%>" <%=pageBean.readonly(!"insert".equals(pageBean.getOperaType()))%> size="24" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="NAME" label="名称" name="NAME" type="text" value="<%=pageBean.inputValue("NAME")%>" size="24" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>宽度</th>
	<td><input id="WIDTH" label="宽度" name="WIDTH" type="text" value="<%=pageBean.inputValue("WIDTH")%>" size="24" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>高度</th>
	<td><input id="HEIGHT" label="高度" name="HEIGHT" type="text" value="<%=pageBean.inputValue("HEIGHT")%>" size="24" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>数据URL</th>
	<td><input id="DATA_URL" label="数据URL" name="DATA_URL" type="text" value="<%=pageBean.inputValue("DATA_URL")%>" size="65" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>默认变量</th>
	<td><input id="VARIABLE_VALUES" label="默认变量" name="VARIABLE_VALUES" type="text" value="<%=pageBean.inputValue("VARIABLE_VALUES")%>" size="65" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>每页行数</th>
	<td><input id="PAGE_SIZE" label="每页行数" name="PAGE_SIZE" type="text" value="<%=pageBean.inputValue("PAGE_SIZE")%>" size="65" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>分页列表</th>
	<td><input id="PAGE_SIZE_LIST" label="分页列表" name="PAGE_SIZE_LIST" type="text" value="<%=pageBean.inputValue("PAGE_SIZE_LIST")%>" size="65" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>双击回调</th>
	<td><%=pageBean.selectRadio("EXISTS_CALLBACK")%></td>
</tr>
<tr>
	<th width="100" nowrap>双击回调函数</th>
	<td><textarea id="CALLBACK_FUNC" label="双击回调函数" name="CALLBACK_FUNC" cols="65" rows="3" class="textarea"><%=pageBean.inputValue("CALLBACK_FUNC")%></textarea></td>
</tr>
<tr>
	<th width="100" nowrap>加载回调脚本</th>
	<td><textarea id="ONLOAD_CALLBACK" label="加载回调脚本" name="ONLOAD_CALLBACK" cols="65" rows="3" class="textarea"><%=pageBean.inputValue("ONLOAD_CALLBACK")%></textarea></td>
</tr>
<tr>
	<th width="100" nowrap>自定义表头</th>
	<td><textarea name="CUSTOM_HEAD" cols="65" rows="3" class="text" id="CUSTOM_HEAD" label="自定义表头"><%=pageBean.inputValue("CUSTOM_HEAD")%></textarea></td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="ID" name="ID" value="<%=pageBean.inputValue("ID")%>" />
</form>
<script language="javascript">
requiredValidator.add("CODE");
requiredValidator.add("NAME");
requiredValidator.add("HEIGHT");
requiredValidator.add("DATA_URL");
lengthValidators[0].set(128).add("VARIABLE_VALUES");
requiredValidator.add("PAGE_SIZE");
requiredValidator.add("PAGE_SIZE_LIST");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
