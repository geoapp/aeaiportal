<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>个人日程管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>标题</th>
	<td><input id="SCHEDULE_TITLE" label="标题" name="SCHEDULE_TITLE" type="text" value="<%=pageBean.inputValue("SCHEDULE_TITLE")%>" style="width:476px" size="65" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>时间</th>
	<td><input id="SCHEDULE_TIME" label="时间" name="SCHEDULE_TIME" type="text" value="<%=pageBean.inputTime("SCHEDULE_TIME")%>" size="24" class="text" /><img id="SCHEDULE_TIMEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>地点</th>
	<td><input id="SCHEDULE_PLACE" label="地点" name="SCHEDULE_PLACE" type="text" value="<%=pageBean.inputValue("SCHEDULE_PLACE")%>" style="width:476px" size="65" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>内容</th>
	<td><textarea id="SCHEDULE_CONTENT" label="内容" name="SCHEDULE_CONTENT" cols="64" rows="3" class="textarea" style="width:474px"><%=pageBean.inputValue("SCHEDULE_CONTENT")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>备注</th>
	<td><input id="SCHEDULE_COMMENT" label="备注" name="SCHEDULE_COMMENT" type="text" value="<%=pageBean.inputValue("SCHEDULE_COMMENT")%>" style="width:476px" size="65" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="SCHEDULE_ID" name="SCHEDULE_ID" value="<%=pageBean.inputValue4DetailOrUpdate("SCHEDULE_ID","")%>" />
<input type="hidden" id="SCHEDULE_USER_ID" name="SCHEDULE_USER_ID" value="<%=pageBean.inputValue("SCHEDULE_USER_ID")%>" />
</form>
<script language="javascript">
initCalendar('SCHEDULE_TIME','%Y-%m-%d %H:%M','SCHEDULE_TIMEPicker');
requiredValidator.add("SCHEDULE_TITLE");
datetimeValidators[0].set("yyyy-MM-dd HH:mm").add("SCHEDULE_TIME");
requiredValidator.add("SCHEDULE_TIME");
requiredValidator.add("SCHEDULE_PLACE");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
