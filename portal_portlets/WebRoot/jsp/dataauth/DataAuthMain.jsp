<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>数据权限</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
</head>
<script language="javascript">

//删除操作
function deleteOption(isFlush) {
	var _sel_resType = $("#sel_resType option:selected").val();
	var _sel_resId = $("#sel_resId option:selected").val();
	var _groupId = $("#sel_group option:selected").val();
	
	$('#RES_TYPE').val(_sel_resType);
	$('#RES_ID').val(_sel_resId);
	$('#GRP_ID').val(_groupId);

	if(isFlush) {
		$('#GRP_ID').val('');
	} else {
		if(_groupId == undefined || _groupId == '' || _groupId == null) {
			alert('请选择需要删除的群组!');
			return;
		}
	}
	
	if(confirm('是否确认删除？')) {
		var action = "<%=pageBean.getHandlerURL()%>&actionType=delGroupAuth";
		$("#form1").attr('action', action);
		postRequest('form1', {onComplete:function(responeText){
			if(responeText != 'SUCCESS') {
				alert('删除失败!');
			} else {
				displayGroupSelect();
			}
		}});
	}
}

//新增操作
var addGroupTreeBox;
function showAddGroupTreeBoxBox(){
	var _sel_resType = $("#sel_resType option:selected").val();
	var _sel_resId = $("#sel_resId option:selected").val();
	if(_sel_resId == undefined || _sel_resId == null || _sel_resId == '') {
		alert('请选择相应的实体！');
		return;
	} 
	
	if (!addGroupTreeBox){
		addGroupTreeBox = new PopupBox('addGroupTreeBox','请选择群组',{size:'normal',width:'300px',top:'3px'});
	}
	var handlerId = "SecurityGroupTreeSelectDataRes";
	var url = 'index?' + handlerId + '&sel_resType='+ _sel_resType +'&sel_resId='+ _sel_resId;
	addGroupTreeBox.sendRequest(url);
}

//保存操作
function saveGroup(idValue) {
	var _sel_resType = $("#sel_resType option:selected").val();
	var _sel_resId = $("#sel_resId option:selected").val();

	var _groupIds = '';
	$("#sel_group option").each(function(){
		_groupIds += $(this).val() + ",";
	});
	
	$('#RES_TYPE').val(_sel_resType);
	$('#RES_ID').val(_sel_resId);
	$('#GRP_ID').val('');
	$('#groupIds').val(_groupIds);

	var action = "<%=pageBean.getHandlerURL()%>&actionType=saveGroupAuth";
	$("#form1").attr('action', action);
	postRequest('form1', {onComplete:function(responeText){
		if(responeText != 'SUCCESS') {
			alert('保存失败!');
		} else {
			displayGroupSelect();
		}
	}});
}

//显示资源
function displayResSelect() {
	var action = "<%=pageBean.getHandlerURL()%>&actionType=displayAuthValue";
	$("#form1").attr('action', action);
	postRequest('form1', {onComplete:function(responeText){
		$("#sel_resId option").remove();
		$('#sel_resId').html(responeText);
 	}});
}

//显示群组
function displayGroupSelect() {
	var action = "<%=pageBean.getHandlerURL()%>&actionType=displayGroupRef";
	$("#form1").attr('action', action);
	postRequest('form1', {onComplete:function(responeText){
		$("#sel_group option").remove();
		$('#sel_group').html(responeText);
 	}});
}
</script>
<body>
	<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
		<%@include file="/jsp/inc/message.inc.jsp"%>
		<table width="100%" border="0">
			<tr>
				<td width="25%">
					<fieldset style="padding: 0 5px 5px 5px; margin-top: 9px;">
						<legend style="font-weight: bolder;">资源列表</legend>
						<table border="0" cellpadding="0" cellspacing="1">
							<tr><td height="28px"></td></tr>
						</table>
						<select size="20" id="sel_resType" name="sel_resType" style="width:100%;height:350px;" onchange="displayResSelect();"><%=pageBean.selectValue("sel_resType")%></select>
					</fieldset>
				</td>
				<td width="25%" valign="top">
					<fieldset style="padding: 0 5px 5px 5px; margin-top: 9px;">
						<legend style="font-weight: bolder;">实体列表</legend>
						<table border="0" cellpadding="0" cellspacing="1">
							<tr><td height="28px"></td></tr>
						</table>
						<select size="20" id="sel_resId" name="sel_resId" style="width:100%;height:350px;" onchange="displayGroupSelect();"><%=pageBean.selectValue("sel_resId")%></select>
					</fieldset>
				</td>
				<td width="25%" valign="top">
					<fieldset style="padding: 0 5px 5px 5px; margin-top: 9px;">
						<legend style="font-weight: bolder;">群组列表</legend>
						<table border="0" cellpadding="0" cellspacing="1">
							<tr>
								<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="showAddGroupTreeBoxBox();">
									<input value="&nbsp;" type="button" class="addImgBtn" id="addImgBtn" title="添加" />添加</td>
								<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="deleteOption(false);">
									<input value="&nbsp;" type="button" class="delImgBtn" id="delImgBtn" title="删除" />删除</td>
								<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="deleteOption(true);">
									<input value="&nbsp;" type="button" class="cancelImgBtn" id="cancelImgBtn" title="刷新" />清空</td>
							</tr>
						</table>
						<select size="20" id="sel_group" name="sel_group" style="width:100%;height:350px;"><%=pageBean.selectValue("sel_group")%></select>
					</fieldset>
				</td>
			</tr>
		</table>
		<input type="hidden" name="actionType" id="actionType" value="" />
		<input type="hidden" name="RES_TYPE" id="RES_TYPE" value="" />
		<input type="hidden" name="RES_ID" id="RES_ID" value="" />
		<input type="hidden" name="GRP_ID" id="GRP_ID" value="" />
		<input type="hidden" name="groupIds" id="groupIds" value="" />
	</form>
</body>
</html>
