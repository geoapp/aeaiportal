package com.apusic.esb.component.java.JavaTransformer1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.apusic.esb.exception.io.MessageHandlerException;
import com.apusic.esb.flow.commonobject.StringObject;
import com.apusic.esb.http.httpinput1.HTTPData;
import com.apusic.esb.io.MessageServiceContext;
import com.apusic.esb.io.RuntimeContext;

public class JavaCompute {

    /**
     * 消息服务上下文对象，可通过此接口访问流程公用资源，如数据库连接，SDO上下文等。警告：请勿删除此变量！
     */
    MessageServiceContext messageServiceContext;

    /**
	 * 在此方法中实现业务逻辑，runtimeContext对象提供了对流程运行期信息的访问。
     */
    public void handler(StringObject result, HTTPData httpData, RuntimeContext runtimeContext) throws MessageHandlerException {
    	
    	Connection connection = messageServiceContext.getConnection("dbtest");
    	ResultSet rs = null;
    	
    	Statement stm = null;
    	String resultStr = "";
    	
    	String para = httpData.getParentDim();
    	
    	if (para == null || "".equals(para.trim())){
    		para = "";
    		
    	}else{
    		
    		if ("$".equals(para.substring(0,1))){
    			para = para.substring(1,para.length());
    		}
    		
    		String[] array = para.replace("$", ",").split(",");
    		
    		StringBuffer strin = new StringBuffer();
    		for (int i = 0; i < array.length; i ++){
    			
    			if (i == 0){
    				strin.append(" '").append(array[i]).append("' ");
    			}else{
    				strin.append(" ,'").append(array[i]).append("' ");
    			}
    		}
    		
    		para = " where t.car_series in (" + strin + ") ";
    	}
    	
    	String sql = " SELECT t.car_type  FROM t_car_type t " + para;

    	try {
			stm = connection.createStatement();
			rs = stm.executeQuery(sql);

			if (rs != null){
				while (rs.next()) {
	
					resultStr += "," + rs.getString("car_type");
				}
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				rs.close();
				stm.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		if (resultStr.length() > 0){
			resultStr = resultStr.substring(1,resultStr.length());
		}
    	
    	result.setValue(resultStr);
    }
}
