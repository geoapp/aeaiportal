<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>
<%
String isSetting = (String)request.getAttribute("isSetting");
if("true".equals(isSetting)){
String portletId = (String)request.getAttribute("portletId");
int cols = Integer.parseInt((String)request.getAttribute("cols"));
String height = (String)request.getAttribute("height");
String isLs = (String)request.getAttribute("isLs");
int rows = Integer.parseInt((String)request.getAttribute("rows"));
int total = cols*rows;
%>
<script language="javascript">
function createIframe(id,url,playOptions){
	var width = "350";
	var height= "285";
	if (playOptions){
		if (playOptions.width){
			width = playOptions.width;
		}
		if (playOptions.height){
			height = playOptions.height;
		}
	}
	
	var vhtml=  '<iframe id="DynamicIframe'+id+'" frameborder="no" border="0" marginwidth="0" marginheight="0" src="'+url+'" scrolling="auto"  width="100%" height="100%"></iframe>';
    document.getElementById(id).innerHTML = vhtml;
}
function resetGrid(){
	for (var i=0;i < <%=total%> ;i++){
		var divId = 'div'+i;
		document.getElementById(divId).innerHTML = '';
	}
}
</script>
<%if("Y".equals(isLs)) {%>
<div id="<portlet:namespace/>div" style="height:${height}px;padding:2px;height:auto;">
<% }else{%>
<div id="<portlet:namespace/>div" style="height:${height}px;padding:2px;overflow:scroll;">
<% }%>

<table id="tabl1" border=1 cellspacing=1>
<%
 	String style = "width='600' height='400'";
	int videoW = 600;
	int videoH = 400;
	if(cols==1){
		style = "width='600' height='400'";
		videoW = 600; videoH=400;
	}else if(cols==2){
		style = "width='300' height='240'";
		videoW = 300; videoH=240;
	}else if(cols==3){
		style = "width='200' height='160'";
		videoW = 200; videoH=160;
	}else if(cols==4 ||cols==5 || cols==6){
		style = "width='180' height='148'";
		videoW = 180; videoH=148;
	}   
   		int num = 0;   			
   		for(int i=0;i<rows;i++){%>
   		<tr>
   			<%for(int j=0;j<cols;j++){%>
   			<td <%=style%>>
   		    <div id="div<%=num%>"></div>
   		 	<%  
   		 	num++;
   		 	%> 
   			</td>
			<%} %>
   	</tr>
  	<% }%>
</table>
</DIV>
<script language="javascript">
$(function(){
	var __renderPortlet<portlet:namespace/> = function(){
		sendAction('${getAjaxDataURL}',{dataType:'text',onComplete:function(responseText){
			if(responseText != null){
				jsonData= eval(responseText);
				resetGrid();
				for(var k=0;k<jsonData.length;k++){
					var divId = "div"+k;
					createIframe(divId,jsonData[k].url,{width:'<%=videoW%>',height:'<%=videoH%>'});
				}
			}
		}});
	};
	__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
	__renderPortlet<portlet:namespace/>();
});
</script> 
<%
}else{
	out.println("请正确相关设置属性！");
} 
%>