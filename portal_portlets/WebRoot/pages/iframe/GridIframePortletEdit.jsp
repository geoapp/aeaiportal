<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String isCache = (String)request.getAttribute("isCache"); 
String isLs = (String)request.getAttribute("isLs"); 
%>
<form id="<portlet:namespace/>CrvPlayConfig">
<table width="600" border="1" >
    <tr>
		<td width="120">高度&nbsp;<span style="color: red;">*</span></td>
		<td><input type="text" size="50" name="height" id="height" value="${height}" /></td>
	</tr>
	<tr>
	    <td width="120">列数&nbsp;<span style="color: red;">*</span></td>
		<td><select name="cols" id="cols">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
		</select></td>
	</tr>
	<tr>
	    <td width="120">默认行数&nbsp;<span style="color: red;">*</span></td>
		<td><input type="text" size="50" name="rows" id="rows" value="${rows}" /></td>
	</tr>	
	<tr>
      <td width="120">数据路径&nbsp;<span style="color: red;">*</span></td>
      <td>
        <input name="dataURL" type="text" id="dataURL" value="${dataURL}" size="60" /></td>
    </tr>
     <tr>
      <td width="120">默认值</td>
      <td>
        <input type="text" name="defaultVariableValues" id="defaultVariableValues" size="50" value="${defaultVariableValues}" />
      </td>
    </tr>
	 <tr>
      <td width="120">自动拉伸</td>
      <td>
        <input type="radio" name="isLs" id="isLsY" value="Y" />是&nbsp;&nbsp;
        <input type="radio" name="isLs" id="isLsN" value="N" />否</td>
    </tr> 
    <tr>
      <td width="120">是否缓存</td>
      <td>
        <input type="radio" name="isCache" id="isCacheY" value="Y" />是&nbsp;&nbsp;
        <input type="radio" name="isCache" id="isCacheN" value="N" />否</td>
    </tr> 
    <tr>
      <td width="120">缓存时间</td>
      <td>
        <input type="text" name="cacheMinutes" id="cacheMinutes" value="${cacheMinutes}" /> 分钟</td>
    </tr> 
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>CrvPlayConfig'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">	
$("#<portlet:namespace/>CrvPlayConfig #cols").val("${cols}");	
<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>CrvPlayConfig #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>CrvPlayConfig #isCacheN').attr("checked","checked");
<%}%>
<%if ("Y".equals(isLs)){%>
	$('#<portlet:namespace/>CrvPlayConfig #isLsY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>CrvPlayConfig #isLsN').attr("checked","checked");
<%}%>
</script>