<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<iframe id="<portlet:namespace />content" src="${src}" width="${xEnd}" height="${height}" frameborder="0" scrolling="${scroll}" style="left:${xStart}; top: ${yStart};margin-top:0px;"></iframe>
<script language="javascript">
$(function() {
    if('${autoExtend}' == 'true') {
       $('#<portlet:namespace />content').load(function(){
			resetPageBodyHeight('<portlet:namespace />content')       
       });
   }
});
</script>