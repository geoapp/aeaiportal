<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.agileai.portal.driver.common.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getDataXML" var="getDataXMLURL">
</portlet:resourceURL>
<%
String portletId = (String)request.getAttribute("portletId");
String chartHeight = (String)request.getAttribute("chartHeight");
String isTransparent = (String)request.getAttribute("isTransparent");
%>
<div id="<portlet:namespace/>chartdiv" align="center" style="height:${chartHeight}px;width:100%;text-align:center;"></div>
<script type="text/javascript">
<%if (chartHeight != null || "".equals(chartHeight)){%>
$(function(){
	var __renderPortlet<portlet:namespace/> = function(){
		var <portlet:namespace/>chart = new FusionCharts("<%=request.getContextPath()%>/charts/AngularGauge.swf", "<portlet:namespace/>Chart","${chartWidth}","${chartHeight}", "0", "0","FFFFFF", "exactFit");
		<%if ("Y".equals(isTransparent)){%>
			<portlet:namespace/>chart.setTransparent(true);
		<%}%>	
		sendAction('${getDataXMLURL}',{dataType:'text',onComplete:function(responseText){
			<portlet:namespace/>chart.setDataXML(responseText);
			<portlet:namespace/>chart.configure(<%=MessageConsts.getChartMessage()%>);
			<portlet:namespace/>chart.render("<portlet:namespace/>chartdiv");
		}});
	};
	__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
	__renderPortlet<portlet:namespace/>();

});
<%}else{%>
	$('#<portlet:namespace/>chartdiv').html('请配置图表组件相关属性!');
<%}%>
</script> 
