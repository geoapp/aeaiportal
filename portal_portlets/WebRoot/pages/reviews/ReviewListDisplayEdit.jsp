<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String isCache = (String)request.getAttribute("isCache"); 
%>

<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
  	<tr>
      <td width="120">Html模板</td>
      <td><textarea name="template" cols="70" rows="10" id="template" style="width:380px">${template}</textarea></td>
    </tr>
	<tr>
		<td width="120">正文ID参数&nbsp;<span style="color: red;">*</span></td>
		<td><input type="text" size="50" name="contentIdParamKey" id="contentIdParamKey" value="${contentIdParamKey}" /></td>
	</tr>
     <tr>
      <td width="120">默认值</td>
      <td>
        <input type="text" name="contentIdDefValue" id="contentIdDefValue" size="50" value="${contentIdDefValue}" />
      </td>
    </tr>
	<tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">	
<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>Form #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isCacheN').attr("checked","checked");
<%}%>
$(document).ready(function(){
	var idParam1 = $('#<portlet:namespace/>Form #template').val();
	idParam1 = idParam1.substring(idParam1.length-36, idParam1.length);
	var idParam2 = $('#<portlet:namespace/>Form #contentIdDefValue').attr('value');
	var url1 = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam1 + '&type=portlet';
	var url2 = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam2 + '&type=content';
	sendRequest(url1,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #template').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
	sendRequest(url2,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #contentIdDefValue').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
});
</script>
