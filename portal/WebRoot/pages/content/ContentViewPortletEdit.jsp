<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String observeViewCount = (String)request.getAttribute("observeViewCount");
String observeReviewCount = (String)request.getAttribute("observeReviewCount");
String contentIdParamMode = (String)request.getAttribute("contentIdParamMode");
%>
<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
	<tr>
      <td width="120">展现模板ID</td>
      <td>
        <input data-role="none" name="templateId" type="text" id="templateId" value="${templateId}" size="60" /></td>
    </tr>  
    <tr>
      <td width="120">最小高度</td>
      <td>
        <input data-role="none" type="text" name="minHeight" id="minHeight" value="${minHeight}" />      </td>
    </tr>
	<tr>
      <td width="120">正文ID参数</td>
      <td>
        <input data-role="none" name="contentIdParamKey" type="text" id="contentIdParamKey" value="${contentIdParamKey}" size="60" /></td>
    </tr>
	<tr>
      <td width="120">参数方式</td>
      <td>
      <input data-role="none" type="radio" name="contentIdParamMode" id="byRetrieve" value="byRetrieve" />直接获取
      &nbsp;&nbsp;
	   <input data-role="none" type="radio" name="contentIdParamMode" id="byParse" value="byParse" />URL解析       
       </td>
    </tr>
	<tr>
      <td width="120">正文ID默认值</td>
      <td>
        <input data-role="none" name="contentIdDefValue" type="text" id="contentIdDefValue" value="${contentIdDefValue}" size="60" /></td>
    </tr>   
	<tr>
      <td width="120">考查阅读数</td>
      <td>
      <input data-role="none" type="radio" name="observeViewCount" id="observeViewCountY" value="Y" />Y
      &nbsp;&nbsp;
	   <input data-role="none" type="radio" name="observeViewCount" id="observeViewCountN" value="N" />N      
       </td>
    </tr>
	<tr>
      <td width="120">考查回复数</td>
      <td>
      <input data-role="none" type="radio" name="observeReviewCount" id="observeReviewCountY" value="Y" />Y
      &nbsp;&nbsp;
	  <input data-role="none" type="radio" name="observeReviewCount" id="observeReviewCountN" value="N" />N      
       </td>
    </tr>
	<tr>
      <td colspan="2" align="center">
        <input data-role="none" type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input data-role="none" type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">
<%if ("Y".equals(observeViewCount)){%>
	$('#<portlet:namespace/>Form #observeViewCountY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #observeViewCountN').attr("checked","checked");
<%}%>

<%if ("Y".equals(observeReviewCount)){%>
$('#<portlet:namespace/>Form #observeReviewCountY').attr("checked","checked");
<%}else{%>
$('#<portlet:namespace/>Form #observeReviewCountN').attr("checked","checked");
<%}%>

<%if ("byRetrieve".equals(contentIdParamMode)){%>
$('#<portlet:namespace/>Form #byRetrieve').attr("checked","checked");
<%}else{%>
$('#<portlet:namespace/>Form #byParse').attr("checked","checked");
<%}%>

$(document).ready(function(){
	var idParam1 = $('#<portlet:namespace/>Form #templateId').val();
	var idParam2 = $('#<portlet:namespace/>Form #contentIdDefValue').attr('value');
	var url1 = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam1 + '&type=portlet';
	var url2 = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam2 + '&type=content';
	sendRequest(url1,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #templateId').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
	sendRequest(url2,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #contentIdDefValue').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
});
</script>