<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
String portletId = (String)request.getAttribute("portletId");
String recordsCount = (String)request.getAttribute("recordsCount");
String recCountPerPage = (String)request.getAttribute("recCountPerPage");
String mainNumCount = (String)request.getAttribute("mainNumCount");
String sideNumCount = (String)request.getAttribute("sideNumCount");
String themeType = (String)request.getAttribute("themeType");
%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
	<portlet:param name="themeType" value="<%=themeType%>" />
</portlet:resourceURL>
<portlet:resourceURL id="getRecordCount" var="getRecordCountURL">
</portlet:resourceURL>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/pagination.css" />
<style>
.listAreaMain{
    min-height:${minHeight}px; 
    height:auto !important; 
    height:${minHeight}px; 
    overflow:visible;
}
</style>
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/jquery.pagination.js"></script>
<div id="SearchResult" class="listAreaMain">
</div>
<div id="Pagination" class="pagination">
</div>
<script type="text/javascript">
var recordsCount;
function pageselectCallback(page_index, jq){
    var items_per_page = <%=recCountPerPage%>;
    var startIndex = page_index*items_per_page;
    var endIndex = Math.min((page_index+1) * items_per_page,recordsCount);
    var datas = "startIndex="+startIndex+"&endIndex="+endIndex;
	sendAction('${getAjaxDataURL}',{dataType:'html',data:datas,onComplete:function(responseText){
		if (responseText){
		    var newcontent = responseText;
		    $('#SearchResult').html(newcontent);
		}else{
			$('#SearchResult').html("");
		}
	}});    
    return false;
}
var __renderPortlet<portlet:namespace/> = function(){
	sendAction('${getRecordCountURL}',{dataType:'text',onComplete:function(responseText){
		recordsCount = parseInt(responseText);
		var optInit = {callback: pageselectCallback,items_per_page:<%=recCountPerPage%>,num_display_entries:<%=mainNumCount%>,num_edge_entries:<%=sideNumCount%>,prev_text:'向前',next_text:'向后'}
		$("#Pagination").pagination(recordsCount,optInit);		
	}});
};
__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
__renderPortlet<portlet:namespace/>();
</script>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>