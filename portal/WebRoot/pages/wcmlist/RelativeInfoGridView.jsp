<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
String recordsCount = (String)request.getAttribute("recordsCount");
String recCountPerPage = (String)request.getAttribute("recCountPerPage");
String mainNumCount = (String)request.getAttribute("mainNumCount");
String sideNumCount = (String)request.getAttribute("sideNumCount");
String content = (String)request.getAttribute("content");
%>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/pagination.css" />
<style>
.listAreaMain{
    min-height:${minHeight}px; 
    height:auto !important; 
    height:${minHeight}px; 
    overflow:visible;
}
</style>
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/jquery.pagination.js"></script>
<div id="SearchResult" class="listAreaMain">
<%=content%>
</div>
<div id="Pagination" class="pagination">
</div>
<script type="text/javascript">
var recordsCount = <%=recordsCount%>;
function pageselectCallback(page_index, jq){
    var items_per_page = <%=recCountPerPage%>;
    var startIndex = page_index*items_per_page;
    var endIndex = Math.min((page_index+1) * items_per_page,recordsCount);
    $("ul li.title").hide();
    $("ul li.title").slice(startIndex,endIndex).show(); 
    return false;
}
var optInit = {callback: pageselectCallback,items_per_page:<%=recCountPerPage%>,num_display_entries:<%=mainNumCount%>,num_edge_entries:<%=sideNumCount%>,prev_text:'向前',next_text:'向后'}
$("#Pagination").pagination(recordsCount,optInit);
</script>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>