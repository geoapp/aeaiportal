<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:actionURL name="cancelConfig" var="cancelConfigURL"></portlet:actionURL>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.portal.driver.PortletBean"/>
<form name="<portlet:namespace />form1" id="<portlet:namespace />form1" method="post">
<table class="detailTable" cellspacing="0" cellpadding="0" style="margin:1px 0 1px 1px;">
    <tr>
      <th width="100">SSO应用</th>
      <td>
        <select name="ssoAppId" id="ssoAppId"><%=pageBean.selectValue("ssoAppId") %>
        </select></td>
    </tr>
    <tr>
      <th>目标URL</th>
      <td><input type="text" name="targetURL" id="targetURL" size="52" value="<%=pageBean.inputValue("targetURL") %>" /></td>
    </tr>
    <tr>
      <th>宽度</th>
      <td><input type="text" name="width" id="width" value="<%=pageBean.inputValue("width") %>" /></td>
    </tr>
    <tr>
      <th>高度</th>
      <td><input type="text" name="height" id="height" value="<%=pageBean.inputValue("height") %>" /></td>
    </tr>
    <tr>
       <th>自动伸展高度</th>
       <td>
        <input type="radio" name="autoExtend" id="autoExtendY" value="Y" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="autoExtend" id="autoExtendN" value="N" />
      否</td>
    </tr>   
    <tr>
      <td colspan="2" align="center">
        <input onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace />form1'});" type="button" value="保存" />
      <input onclick="fireAction('${cancelConfigURL}');" type="button" value="取消" /></td>
    </tr>
  </table>
</form>
<script language="javascript">
<%if ("Y".equals(pageBean.inputValue("autoExtend"))){%>
	$('#<portlet:namespace/>form1 #autoExtendY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>form1 #autoExtendN').attr("checked","checked");
<%}%>
</script>