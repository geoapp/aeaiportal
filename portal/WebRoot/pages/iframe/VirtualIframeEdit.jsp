<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>

<form id="<portlet:namespace/>IframeConfig">
  <table width="90%" border="1">
    <tr>
      <td width="120">默认高度</td>
      <td>
        <input type="text" name="iframeHeight" id="iframeHeight" value="${iframeHeight}" />      </td>
    </tr>  
<tr>
      <td width="120">是否显示滚动条</td>
      <td><input type="checkbox" name="iframeScroll" value="yes" /></td>
    </tr>
<tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>IframeConfig'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script type="text/javascript">
$(function() {
	if('${iframeScroll}' == 'auto'||'${iframeScroll}' == 'yes') {
		$('#<portlet:namespace />IframeConfig input[name="iframeScroll"]').attr('checked', 'checked');
	}
 });
</script>