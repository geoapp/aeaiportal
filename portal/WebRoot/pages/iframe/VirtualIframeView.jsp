<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getIframeSrc" var="getIframeSrcURL">
</portlet:resourceURL>
<iframe id="<portlet:namespace/>VirtualIframe" frameborder="no" border="0" marginwidth="0" marginheight="0" src="${iframeSrc}" scrolling="${iframeScroll}"  width="100%" height="${iframeHeight}"></iframe>