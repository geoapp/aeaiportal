<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String isShowRoleList = (String)request.getAttribute("isShowRoleList");
%>
<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
	<tr>
      <td width="120" nowrap="nowrap">是否显示角色列表</td>
      <td>
        <input type="radio" name="isShowRoleList" id="isShowRoleListY" value="Y" />是&nbsp;&nbsp;
        <input type="radio" name="isShowRoleList" id="isShowRoleListN" value="N" />否</td>
    </tr>   
    <tr>
      <td width="120">最小高度</td>
      <td>
        <input name="minHeight" type="text" id="minHeight" value="${minHeight}" size="40" />      </td>
    </tr>    
	<tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">
<%if ("Y".equals(isShowRoleList)){%>
	$('#<portlet:namespace/>Form #isShowRoleListY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isShowRoleListN').attr("checked","checked");
<%}%>
</script>