<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
<portlet:resourceURL id="deleteFavorite" var="deleteFavoriteURL"></portlet:resourceURL>
</portlet:resourceURL>
<%
String portletId = (String)request.getAttribute("portletId");
String isSetting = (String)request.getAttribute("isSetting");
if("true".equals(isSetting)){
%>
<div id="<portlet:namespace/>Div" style="height:${height};width:100%"></div>
<script type="text/javascript">
var htmlHandler = {
	_excute:${jsHandler}
}
$(function(){
	var __renderPortlet<portlet:namespace/> = function(){
		sendAction('${getAjaxDataURL}',{dataType:'text',onComplete:function(responseText){
			if (responseText){
				$('#<portlet:namespace/>Div').html("");
				$('#<portlet:namespace/>Div').append(responseText);
				htmlHandler._excute('<portlet:namespace/>Div');
			}
		}});
	};
	__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
	__renderPortlet<portlet:namespace/>();
});

function deleteFavorite(favId){
	$("#FAV_ID").val(favId);
	postAction('${deleteFavoriteURL}',{formId:'form1',onComplete:function(responseText){
			alert("收藏已取消");
			sendAction('${getAjaxDataURL}',{dataType:'text',onComplete:function(responseText){
				if (responseText){
					$('#<portlet:namespace/>Div').html("");
					$('#<portlet:namespace/>Div').append(responseText);
					htmlHandler._excute('<portlet:namespace/>Div');
				}
			}});
	}});
}
</script> 
<%
}else{
	out.println("请正确相关设置属性！");
} 
%>