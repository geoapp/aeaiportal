<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String supportSplitSearchWords = (String)request.getAttribute("supportSplitSearchWords");
%>
<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
    <tr>
      <td width="120">支持分词查询</td>
      <td>
        <input type="radio" name="supportSplitSearchWords" id="supportSplitSearchWordsY" value="Y" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="supportSplitSearchWords" id="supportSplitSearchWordsN" value="N" />
      否</td>
    </tr>
    <tr>
      <td width="120">数据展现页面</td>
      <td title="如：03/SomeHiddenPage.ptml">
        <input name="targetPageURL" type="text" id="targetPageURL" value="${targetPageURL}" size="40" />
      </td>
    </tr>    
	<tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">
<%if ("Y".equals(supportSplitSearchWords)){%>
	$('#<portlet:namespace/>Form #supportSplitSearchWordsY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #supportSplitSearchWordsN').attr("checked","checked");
<%}%>
</script>
