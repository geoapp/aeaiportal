<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="convertWords" var="convertWordsURL">
</portlet:resourceURL>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
%>
<style type="text/css">
.search-box {
	border: 1px solid #AFC2D4;
	width: 90%;
	margin:5px 3px;
}
.search-box .keyword-box {
	width: 100%;
	height: 1.7em;
	border: 0pt none;
}
.search-box  .search-button {
	background: url(<%=request.getContextPath()%>/pages/search/images/green-search.gif) no-repeat center;
	cursor: pointer;
	font-size: 0pt;
	height: 25px;
	overflow: hidden;
	width: 25px;
	border: white 0px;
}
</style>
<form id="<portlet:namespace/>SearchForm" method="post">
<div class="search-box">
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td width="100%">
                    <input type="text" name="keyword" class="keyword-box" onkeypress="checkEnterPress()" align="middle">
                </td>
                <td>
                    <input type="button" class="search-button" value="" onclick="doSimpleSearch()">
                </td>
            </tr>
        </tbody>
    </table>
    <div style="clear:both"></div>
</div>
</form>
<script type="text/javascript">
function doSimpleSearch(){
	postAction('${convertWordsURL}',{formId:'<portlet:namespace/>SearchForm',dataType:'text',onComplete:function(responseText){
		if (responseText !=""){
			var convertWord = responseText;
			window.location.href="${targetPageURL}?searchWord="+convertWord;			
		}
	}});
}
function checkEnterPress(){	
	if ( event.keyCode == 13 ) 
    { 
		doSimpleSearch(); 
    }
}
</script>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>