<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
String content = (String)request.getAttribute("content");
%>
<script>
$(document).ready(function () {
	$('#accordion a.item').click(function () {
		if ($(this).parent().children('ul').html()){
			$('#accordion li').children('ul').slideUp('fast');	
			//remove all the "Over" class, so that the arrow reset to default
			$('#accordion a.item').each(function () {
				if ($(this).attr('rel')!='') {
					$(this).removeClass($(this).attr('rel') + 'Over');	
				}
			});
			//show the selected submenu
			$(this).siblings('ul').slideDown('fast');
			//add "Over" class, so that the arrow pointing down
			$(this).addClass($(this).attr('rel') + 'Over');
			return false;
		}else{
			$('#accordion a.selected').removeClass('selected');
			$(this).addClass('selected');
		}
	});
	
	$('#accordion li ul li a').click(function(){
		$('#accordion a.selected').removeClass('selected');
		$(this).addClass('selected');
	});
	
	if (!$('#accordion a.selected').hasClass('item')){
		$('#accordion a.selected').parent('li').parent().parent().children('a.item').trigger('click');		
	}
});
</script>
<div class="CascadeList">
<%=content%>
</div>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>