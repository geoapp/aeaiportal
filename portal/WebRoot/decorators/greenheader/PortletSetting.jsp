<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ page language="java" import="com.agileai.portal.driver.model.PortletWindowConfig" %>
<%@ page language="java" import="com.agileai.util.StringUtil" %>
<%@ page language="java" import="com.agileai.portal.driver.AttributeKeys" %>
<%@ page language="java" import="com.agileai.portal.driver.model.Theme" %>
<%@ page language="java" import="com.agileai.portal.driver.model.MenuBar"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%
String windowMode = (String)request.getAttribute("WINDOW_MODE_KEY");
String portletId = (String)request.getAttribute("portlet");
Theme theme = (Theme)request.getAttribute(AttributeKeys.THEME_KEY);
String columnIndex = (String)request.getAttribute("columnIndex");
String menuItemId = (String)request.getAttribute("menuItemId");
PortletWindowConfig windowConfig = (PortletWindowConfig)request.getAttribute(portletId);
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
String height = windowConfig.getHeight();
if (!"auto".equals(height)){
	height = height+"px";
}
String bodyStyle = "style=\"";
String defStyle = windowConfig.getBodyStyle();
bodyStyle = bodyStyle + defStyle;
bodyStyle = bodyStyle+"\"";
boolean isCircular = false;
String iconPath = windowConfig.getPropertyValue(pageContext.getServletContext(),theme,"iconPath");
boolean hasIcon = !StringUtil.isNullOrEmpty(iconPath);
String fullIconPath = null;
if (hasIcon){
	fullIconPath = iconPath;
}
String moreURL = windowConfig.getPropertyValue(pageContext.getServletContext(),theme,"moreURL");
boolean hasMoreURL = !StringUtil.isNullOrEmpty(moreURL);
String fullMoreURL = null;
if (hasMoreURL){
	if (moreURL.toLowerCase().startsWith("http://") || moreURL.toLowerCase().startsWith("https://")){
		fullMoreURL = moreURL;
	}else{
		fullMoreURL = request.getContextPath() + "/" + moreURL;
	}
}
String stateAnchorMarginTop = isCircular?"3":"5";
String modeDropDownMarginTo = isCircular?"0":"1";
%>
<pt:portlet portletId="${portlet}">
<div class="sharp greenheader">
    <div class="content droppable" style="height:<%=height%>" portletId="<%=portletId%>" columnIndex="<%=columnIndex%>">
        <h3 class="portletTitle" portletId="<%=portletId%>" columnIndex="<%=columnIndex%>" <%if(!isCircular){%> style="line-height:27px;height:27px;"<%}%>>
        <%if(hasIcon){%><span><img style="float:left;margin-left:5px;<%if(isCircular){%>margin-top:2px;<%}else{%>margin-top:4px;<%}%>" src="<%=fullIconPath%>" /></span><%}%>
        <span class="draggable ui-draggable" portletId="<%=portletId%>" columnIndex="<%=columnIndex%>" style="font-size:13px;float:left;margin-top:<%=stateAnchorMarginTop%>px;<%if(hasIcon){%>text-indent:5px;<%}else{%>text-indent:10px;<%}%>"><pt:title/></span>
		<span style="float:right;height:25px;<%if(isCircular){%>padding-top:0px;<%}else{%>padding-top:3px;<%}%>">
		<img src="<%=request.getContextPath()%>/images/layout/config.gif" style="margin:0px 3px;" width="16" height="16" onclick="personalConfigDecoratorRequest('<%=menuBar.getId()%>','<%=menuItemId%>','<%=portletId%>')" title="配置Portlet">
		<img src="<%=request.getContextPath()%>/images/layout/movePortletUp.gif" style="margin:0px 3px;" width="16" height="16" onclick="moveUpPersonalPortlet('<%=menuItemId%>','<%=portletId%>','<%=columnIndex%>')" title="上移">
		<img src="<%=request.getContextPath()%>/images/layout/movePortletDown.gif" style="margin:0px 3px;" width="16" height="16" onclick="moveDownPersonalPortlet('<%=menuItemId%>','<%=portletId%>','<%=columnIndex%>')" title="下移">
		<img src="<%=request.getContextPath()%>/images/layout/delete.gif" style="margin:0px 3px;" width="16" height="16" onclick="deletePersonalPortlet('<%=menuItemId%>','<%=portletId%>','<%=columnIndex%>')" title="删除Portlet">
		</span>        
        </h3>
		<%
		Boolean minimized = (Boolean)pageContext.getAttribute("minimized");
		if (!minimized){
		%>        
        <div <%=bodyStyle%> class="portletBox"><pt:render/></div><%}%>
     </div>
  </div>
</pt:portlet>