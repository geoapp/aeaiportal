function startPersonalSetting(navigaterId,menuItemId){
	var url = "/portal/index?Portal&actionType=startPersonalSetting&navigaterId="+navigaterId+"&menuItemId="+menuItemId;
	sendRequest(url,{onComplete:function(responseText){
		resetWindow();
	}});
}
function stopPersonalSetting(menuItemId){
	var url = "/portal/index?Portal&actionType=stopPersonalSetting&menuItemId="+menuItemId;
	sendRequest(url,{onComplete:function(responseText){
		resetWindow();
	}});
}
function applyPersonalSetting(menuItemId){
	var url = "/portal/index?Portal&actionType=applyPersonalSetting&menuItemId="+menuItemId;
	sendRequest(url,{onComplete:function(responseText){
		resetWindow();
	}});
}
function cancelPersonalSetting(menuItemId){
	var url = "/portal/index?Portal&actionType=cancelPersonalSetting&menuItemId="+menuItemId;
	sendRequest(url,{onComplete:function(responseText){
		resetWindow();
	}});
}

var _personalWidthSetupBox;
function personalWidthSetupBoxRequest(menuItemId){
	if (!_personalWidthSetupBox){
		_personalWidthSetupBox = new PopupBox('_personalWidthSetupBox','配置布局宽度比例',{top:'50px',size:'normal',width:'320px',height:'160px'});
	}
	var url = '/portal/index?PersonalWidthSetup&menuItemId='+menuItemId;
	_personalWidthSetupBox.sendRequest(url);	
}

function savePersonalSetting(menuItemId){
	var url = "/portal/index?Portal&actionType=savePersonalSetting&menuItemId="+menuItemId;
	sendRequest(url,{onComplete:function(responseText){
		resetWindow();
	}});
}

var _personalConfigPortletBox;
function personalConfigDecoratorRequest(navId,menuItemId,portletId){
	if (!_personalConfigPortletBox){
		_personalConfigPortletBox = new PopupBox('_personalConfigPortletBox','配置窗口外观',{size:'normal',height:'300px'});
	}
	var url = '/portal/index?PersonalPagePortletConfig&navId='+navId+'&menuItemId='+menuItemId+'&portletId='+portletId;
	_personalConfigPortletBox.sendRequest(url);	
}

function moveUpPersonalPortlet(menuItemId,portletId,columnIndex){
	var url = "/portal/index?Portal&actionType=isFirst&menuItemId="+menuItemId+"&portletId="+portletId+"&columnIndex="+columnIndex;
	sendRequest(url,{onComplete:function(responseText){
		if ("Y" == responseText){
			alert("该Portlet已经在最顶端,不能上移!");
		}else{
			url = "/portal/index?Portal&actionType=moveUpPersonalPortlet&menuItemId="+menuItemId+"&portletId="+portletId+"&columnIndex="+columnIndex;
			sendRequest(url,{onComplete:function(responseText){
				resetWindow();
			}});
		}			
	}});
}

function moveDownPersonalPortlet(menuItemId,portletId,columnIndex){
	var url = "/portal/index?Portal&actionType=isLast&menuItemId="+menuItemId+"&portletId="+portletId+"&columnIndex="+columnIndex;
	sendRequest(url,{onComplete:function(responseText){
		if ("Y" == responseText){
			alert("该Portlet已经在最底部,不能下移!");
		}else{
			url = "/portal/index?Portal&actionType=moveDownPersonalPortlet&menuItemId="+menuItemId+"&portletId="+portletId+"&columnIndex="+columnIndex;
			sendRequest(url,{onComplete:function(responseText){
				resetWindow();
			}});				
		}	
	}});
}

function deletePersonalPortlet(menuItemId,portletId,columnIndex){
	if (confirm("确定要移除该Portlet?")){
		var url = "/portal/index?Portal&actionType=deletePersonalPortlet&menuItemId="+menuItemId+"&portletId="+portletId+"&columnIndex="+columnIndex;
		sendRequest(url,{onComplete:function(responseText){
			resetWindow();
		}});		
	}
}

function resetPersonalSetting(menuItemId){
	var url = "/portal/index?Portal&actionType=resetPersonalSetting&menuItemId="+menuItemId;
	sendRequest(url,{onComplete:function(responseText){
		resetWindow();
	}});
}

function cleanPersonalSetting(menuItemId){
	if (confirm("确认要清空本页面个性设置？")){
		var url = "/portal/index?Portal&actionType=cleanPersonalSetting&menuItemId="+menuItemId;
		sendRequest(url,{onComplete:function(responseText){
			resetWindow();
		}});		
	}
}

function addPortletRequest(menuItemId,pageId){
	var url = "/portal/index?Portal&actionType=retrievePortletJson&menuItemId="+menuItemId+"&pageId="+pageId;
	sendRequest(url,{onComplete:function(responseText){
		$("#portletPicker .content").html("");
		$("#portletPicker .content").append(responseText);
		$("#portletPicker").show();
		$(".drag-item").draggable({revert:true,helper:'clone'});
		$("#portletPicker").css({top:'50px',left:'478px'})	
	}});
}
