﻿CKEDITOR.plugins.add('cmprofile', {
    init: function (editor) {
    	var profileCmd= {
    		    exec:function(editor){
    		    	var columnId =document.getElementById("columnId").value;
					var editMode =document.getElementById("editMode").value;
    		    	var infomationId = document.getElementById("infomationId").value;					
    		    	var url = 'index?WcmInfomationEdit&COL_ID='+columnId+'&operaType=update&INFO_ID='+infomationId+'&editMode='+editMode;
    		    	window.location.href=url;
    		    }
    		};
    	var pluginName = 'cmprofile';
        editor.addCommand(pluginName, profileCmd);
        editor.ui.addButton(pluginName,
        {
            label: '信息概要',
            command: pluginName,
            icon: this.path + 'images/cmprofile.png'
        });
    }
});