<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.hotweb.domain.core.Profile"%>
<%@ page import="com.agileai.hotweb.domain.core.User"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%
Profile profile = (Profile)request.getSession(false).getAttribute(Profile.PROFILE_KEY);
User user = (User)profile.getUser();
boolean isAdmin = "admin".equals(user.getUserCode());
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
String menuBarJson = menuBar.getMenuBarJson(user,menuItem.getCode(),request);
String currentVisitPath = menuBar.getCurrentPath(request,menuItem);
String navigaterId = menuBar.getId();

String windowState= (String)request.getAttribute("WINDOW_STATE_KEY");
String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
String personalSetting =(String)request.getAttribute("PERSONAL_SETTING_MODE_KEY");
String isOnPersonalPage = (String)request.getAttribute("IS_ON_PERSONAL_PAGE");
String usePersonalSetting =(String)request.getAttribute("PERSONAL_SETTING_USE_KEY");
Object personalSettingObject = request.getAttribute("PERSONAL_SETTING_OBJECT_KEY");
boolean isPersonalSetting = "Y".equals(personalSetting);
boolean isUsePersonalSetting = "Y".equals(usePersonalSetting);
boolean hasPersonalSetting = false;
if (personalSettingObject != null){
	hasPersonalSetting = true;
}

String modeText = "view".equals(windowMode)?"编辑组件":"展示信息";
String hostIp = request.getRemoteAddr();
if ("0:0:0:0:0:0:0:1".equals(hostIp)){
	hostIp = "localhost";
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>知识门户</title>
<meta name="keywords" content="<%=menuItem.getKeywords()%>" />
<meta name="description" content="<%=menuItem.getDescription()%>" />

<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/layout.css"  type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css"  type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" type="text/css" >

<link rel="stylesheet" type="text/css" href="<c:out value="${pageContext.request.contextPath}"/>/themes/in/concise_green/css/style.css" />
<link rel="stylesheet" type="text/css" href="<c:out value="${pageContext.request.contextPath}"/>/themes/in/concise_green/css/admin.css" />
<link rel="stylesheet" type="text/css" href="<c:out value="${pageContext.request.contextPath}"/>/themes/in/concise_green/css/main.css" />
<link rel="stylesheet" type="text/css" href="<c:out value="${pageContext.request.contextPath}"/>/themes/in/concise_green/css/display.css" />
<%if ("edit".equals(windowMode) || "Y".equals(isOnPersonalPage)){%>	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.ui.core.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.ui.resizable.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.qtip.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
<%}%>

<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=menuItem.getCustomCssURL()%>" type="text/css" />
<%}%>	
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery1.7.1.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>    
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>

<%if ("edit".equals(windowMode) || "Y".equals(isOnPersonalPage) ){%>    
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.core.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.widget.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.mouse.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.draggable.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.droppable.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.resizable.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PersonalConfig.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.qtip.js" language="javascript"></script>   
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/imagesloaded.pkgd.js" language="javascript"></script> 
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>
<%}%>

<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
	<script type="text/javascript" src="<%=menuItem.getCustomJsURL()%>"></script>
<%}%>	
	<script type="text/javascript">
	$.ajaxSetup({cache:false});
	
	var __renderPortlets = new Map();	
	var _directConfigPortletBox;
	function directConfigDecoratorRequest(portletId){
		if (!_directConfigPortletBox){
			_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'380px'});
		}
		var url = '/portal/index?PagePortletConfig&portletId='+portletId;
		_directConfigPortletBox.sendRequest(url);	
	}
	$(function(){
		<%if ("edit".equals(windowMode)){%>
		$(".content > h3").hover(
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeIn('fast');
		  }, 
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeOut('fast');
		  }
		);
		<%}%>
		$(".content > h3").find("span[id=stateAnchor]").hide();
		PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
	});
</script>
<style type="text/css">
html,body{ 
margin:0px; 
height:auto; 
} 
</style> 
</head>
<body>
<div id="main_container">

	<div id="header">

     <div id="logo">
      	<a href="/portal/request/13/KnowlegeFirstPage.ptml"><img src="/portal/themes/in/concise_green/images/logo01.png" /></a>
     </div>
     <span id="logout" style="float:right; padding:5px;padding-right:15px; ">
			<a class="button-link green" href="javascript:openPersonalThemeBox('<%=navigaterId%>')">个性主题</a>
     	<%if (isAdmin){%><a href="javascript:void(0)" class="button-link green" onClick="changeMode('<c:out value="${pageContext.request.contextPath}"/>')" ><%=modeText%></a>
     		<a hideFocus="true" href="javascript:window.location='/portal/forward?00'" target="_blank" class="button-link green" >门户管理</a>&nbsp;
     		<%}%>
     		<a href="javascript:void(0)" class="button-link orange" onClick="logout()">退出系统</a></span>

		<div class="menu">
			<div id="navigation">
				<ul id="nav" style="float:right"></ul>
			</div> 
		</div>
	</div>
 	<div style=" clear:both"></div>

	<div class="main-content" style="margin:auto;background-color:#E9E6E6;">
      <div class="nav-info" >
		<span id="now_place" style="background:url(/portal/themes/in/standard_green/images/ico_home_16.png) left center no-repeat;padding-left: 18px;">当前位置：<%=currentVisitPath%></span>
		<span id="logout" style="float:right">您好，<%=user.getUserName()%>&nbsp;&nbsp;<input type="hidden" name="currentUserId" id="currentUserId" value="<%=user.getUserCode()%>" /></span>
      </div>
	  </div>
	   <div id="pageBody" style="float:right;width:1200px;">
				 <pt:layout />
			</div>
    
	<div id="footer">
    <div class="fltlft"> 沈阳数通畅联软件技术有限公司 &copy; 版权所有 </div>
    <div class="fltrt"><a href="http://www.miitbeian.gov.cn/" style="color:#772c17;" target="_blank">辽ICP备14005586号</a></div>
	</div>
<!-- end of main_container -->
</body>
</html>
<script type="text/javascript">
<%if ("edit".equals(windowMode)){%>
$(function() {
	$(".sharp .content").resizable({handles: "s",stop:function(event,ui){
		var height = ui.size.height;
		var portletId = $(this).attr("portletId");
		var url = "/portal/index?Portal&actionType=resizePortletHeight&portletId="+portletId+"&height="+height;
		sendRequest(url,{onComplete:function(responseText){
			resetWindow();
		}});
	}});
});
<%}%>

var menudata = <%=menuBarJson%>;
var topMenuSelected;
var topMenuSelectedUrl;
var subMenuSelected;
var topMenuSelectedId;
var menuHtml = "";
var bottomHtml = "";

function createMenu(){
	var data = menudata.menus;
	for(var i = 0; i < data.length; i++) {
		var itemData = data[i];
		if (itemData.selected){
			topMenuSelected = itemData.text;
			topMenuSelectedUrl = itemData.href;
			topMenuSelectedId = itemData.name;
		}
		buildMenuHtml(itemData);
	}
	$('#nav').html(menuHtml);
	$('#bottomNav').html(bottomHtml);
}

function buildMenuHtml(itemData){
	var name = itemData.name;
	var itemType = itemData.type;
	var itemText = itemData.text;
	var submenuHtml = "";
	if(itemType == 'folder'){
		menuHtml = menuHtml + '<li><a href="javascript:void(0)">'+itemText+'</a><ul>';
		bottomHtml = bottomHtml + '<span>';
		for (var j=0;j < itemData.menus.length;j++){
			var submenuData = itemData.menus[j];
			buildMenuHtml(submenuData);
			
			if (j==0){
				if (submenuData.type == 'page'){
					var itemURL = itemData.href;
					var itemTarget = itemData.target;
					bottomHtml = bottomHtml + '<a href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a>';
				}
				else if (submenuData.type == 'link'){
					var itemURL = itemData.href;
					var itemTarget = itemData.target;
					bottomHtml = bottomHtml + '<a hideFocus="true" href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a>';
				}
			}
		}
		menuHtml = menuHtml + '</ul></li>'; 
		bottomHtml = bottomHtml + '</span>'; 
	}
	else if (itemType == 'page'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li><a href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
	else if (itemType == 'link'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li><a hideFocus="true" href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
}
$(document).ready(function($){
	createMenu();
});
</script>
