<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@page import="com.agileai.portal.driver.model.Theme"%>
<%@page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.hotweb.domain.core.Profile"%>
<%@ page import="com.agileai.hotweb.domain.core.User"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%@ page import="com.agileai.hotweb.i18n.BundleContext"%>
<%@ page import="com.agileai.hotweb.i18n.ResourceBundle"%>
<%@ page import="com.agileai.portal.extend.theme.ThemeHelper"%>
<%
ResourceBundle resourceBundle = BundleContext.getOnly().getResourceBundle();
resourceBundle.setLocale((HttpServletRequest)request);

Profile profile = (Profile)request.getSession(false).getAttribute(Profile.PROFILE_KEY);
User user = (User)profile.getUser();
boolean isAdmin = "admin".equals(user.getUserCode());
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
String menuBarJson = menuBar.getMenuBarJson(user,menuItem.getCode(),request);
String currentVisitPath = menuBar.getCurrentPath(request,menuItem);
String navigaterId = menuBar.getId();

String windowState= (String)request.getAttribute("WINDOW_STATE_KEY");
String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
String personalSetting =(String)request.getAttribute("PERSONAL_SETTING_MODE_KEY");
String isOnPersonalPage = (String)request.getAttribute("IS_ON_PERSONAL_PAGE");
String usePersonalSetting =(String)request.getAttribute("PERSONAL_SETTING_USE_KEY");
Object personalSettingObject = request.getAttribute("PERSONAL_SETTING_OBJECT_KEY");
boolean isPersonalSetting = "Y".equals(personalSetting);
boolean isUsePersonalSetting = "Y".equals(usePersonalSetting);
boolean hasPersonalSetting = false;
if (personalSettingObject != null){
	hasPersonalSetting = true;
}

HttpSession httpSession = request.getSession(false);
String httpSessionId = httpSession.getId();

String windowScreenHeight = ThemeHelper.getWindowScreenHeight((HttpServletRequest)request);
Theme theme = (Theme)request.getAttribute("_currentTheme_");
String outherHeight = theme.getOuterHeight();
int themeBodyHeight = Integer.parseInt(windowScreenHeight)-Integer.parseInt(outherHeight);

String modeText = "view".equals(windowMode)?resourceBundle.getString("CommonMenu.editComponent"):resourceBundle.getString("CommonMenu.displayComponent");
String hostIp = request.getRemoteAddr();
if ("0:0:0:0:0:0:0:1".equals(hostIp)){
	hostIp = "localhost";
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>企业应用中心</title>
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/page.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/layout.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/dcmegamenu.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skins/blue.css" type="text/css" />	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/boxy.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/gt_grid.css" type="text/css" />	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/china/skinstyle.css"  type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/vista/skinstyle.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/mac/skinstyle.css" type="text/css" />
<%if ("edit".equals(windowMode) || "Y".equals(isOnPersonalPage)){%>	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.ui.core.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.ui.resizable.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.qtip.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
<%}%>	
<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=ThemeHelper.getReletivePath(theme,menuItem.getCustomCssURL())%>" type="text/css" />
<%}%>
	<link rel="stylesheet" href="/portal/themes/in/standard_green/css/main.css" type="text/css" media="screen" /> 
	<link rel="stylesheet" href="/portal/themes/in/standard_green/css/head-menu.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="/portal/themes/in/standard_green/css/tabso.css" type="text/css" media="screen" /> 
	
	<style type="text/css">
	body, tbody, th, td{
		font-family:Verdana,Arial,Helvetica,sans-serif;
		font-size:12px;
		margin:0;
		padding:0;
	}
	#personalUL { 
		margin: 0; padding: 0; 
		list-style: none; border-left: 1px solid #d5dce8; 
		border-right: 1px solid #d5dce8; border-bottom: 1px solid #d5dce8; 
		border-bottom-left-radius: 4px; 
		-moz-border-radius-bottomleft: 4px; 
		-webkit-border-bottom-left-radius: 4px; 
		border-bottom-right-radius: 4px; 
		-moz-border-radius-bottomright: 4px; 
		-webkit-border-bottom-right-radius: 4px; 
		height: 23px; 
		padding-left: 0px; 
		padding-right:0px; 
		background: #edf3f7; 
	}
	#personalUL li { float: right;
		display: block; background: none; height:20px;
		position: relative; z-index: 999; 
		margin: 0 1px; 
	}
	#personalUL li a { display: block; padding: 0; font-weight: 700; line-height: 20px;
	 	text-decoration: none;  color: #818ba3; zoom: 1; border-left: 1px solid transparent; border-right: 1px solid transparent; padding: 0px 12px; 
	}
	#personalUL li a:hover, #personalUL li a.hov,#personalUL li ul li a.hov { background-color: #fff; border-left: 1px solid #d5dce8; border-right: 1px solid #d5dce8; color: #576482; }
	#personalUL ul { 
		position:absolute; 
		display: none; margin: 0; padding: 0; list-style: none; 
		-moz-box-shadow: 0 1px 3px rgba(0,0,0,0.2); -o-box-shadow: 0 1px 3px rgba(0,0,0,0.2); 
		box-shadow: 0 1px 3px rgba(0,0,0,0.2); -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.2); 
		padding-bottom: 1px; 
		background: #edf3f7;
		width: 130px;
	}
	#personalUL ul li { width:128px; float: left; border-top: 0px solid #fff; text-align: left;height:30px; line-height: 30px;}
	#personalUL ul a { display: block; padding: 3px 5px; color: #666; border-bottom: 1px solid transparent; text-transform:  uppercase; color: #797979; font-weight: normal; }
	
	.draggable {
		padding-left:15px;
		z-index:999;
		background-image: url('<c:out value="${pageContext.request.contextPath}"/>/images/layout/objectdrag.gif');
		background-repeat: no-repeat;
		background-position-x: 7px;
	}
	.droppable {min-height:30px;}
	.ui-state-active{
		background-color:red;
		cursor: pointer;
	}
	#portletPicker{
		z-index: 9999;
		border: 1px outset rgb(102, 153, 204);
		background-color:white;position:absolute;
		display:none;
		height:400px;
		width:200px;
	}
	#portletPicker .header{
		background: url('<c:out value="${pageContext.request.contextPath}"/>/images/decorator/bg2.gif') repeat-x;
		margin:0;
		padding:0;
		padding-top:2px;
		display:block;
		cursor:pointer;
		height:26px;
		line-height:24px;
		color:white;
		padding-left:10px;
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-size: 13px;
	}
	#portletPicker .header .title{
		margin:0;
		padding:0;
		padding-left:4px;
		padding-left: 15px;
		background-image: url('/portal/images/layout/objectdrag.gif');
		background-repeat:no-repeat;
		font-weight: bold;
	}
	#portletPicker .content {
		margin: 2px;
		height:370px;
		overflow:auto;
	}
	
	#portletPicker .content .drag-item {
		list-style-type:none;
		display: block;
		padding: 5px;
		border: 1px solid #ccc;
		margin: 2px;
		background: #fafafa;
		color: #444;
	}
	
    </style>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery1.7.1.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/FusionCharts_pc.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.hoverIntent.minified.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.dcmegamenu.1.3.3.js" language="javascript"></script>    
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.iframe.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>    
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>
    <script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_msg_cn.js"></script>
	<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_grid_all.js"></script>
	<script type="text/javascript" src="/portal_portlets/js/amq_jquery_adapter.js"></script>
	<script type="text/javascript" src="/portal_portlets/js/amq.js"></script>

<%if ("edit".equals(windowMode) || "Y".equals(isOnPersonalPage) ){%>    
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.core.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.widget.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.mouse.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.draggable.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.droppable.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.resizable.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/PersonalConfig.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.qtip.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/imagesloaded.pkgd.js" language="javascript"></script> 
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>       
<%}%>
        
<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
	<script type="text/javascript" src="<%=menuItem.getCustomJsURL()%>"></script>
<%}%>    
	<script type='text/javascript' src="/portal/themes/in/standard_green/js/jquery.prettyPhoto.js"></script>
	<script type='text/javascript' src="/portal/themes/in/standard_green/js/head-menu.js"></script>
	<script type='text/javascript' src="/portal/themes/in/standard_green/js/jquery.tabso.js"></script>
<script>
$.ajaxSetup({cache:false});

var amq = org.activemq.Amq;
amq.init({ uri: '/portal_portlets/amq', logging: true, timeout: 45, clientId:'<%=httpSessionId%>'});	

var __renderPortlets = new Map();	
var turnRef = 62;
<%if("normal".equals(windowState)){ %>
	turnRef = 168;
<%}%>
function resetPageBodyHeight(id){
	if (ele(id)){
		var frm=document.getElementById(id);
		var wantedHeight = $(window).height()-turnRef;
		//alert('wantedHeight is ' + wantedHeight);
		var subWeb=document.frames?document.frames[id].document:frm.contentDocument;
		if (document.getElementById){
			if (frm && !window.opera){
				if (frm.contentDocument && frm.contentDocument.body.offsetHeight){
					frm.height = frm.contentDocument.body.offsetHeight;
					if (frm.height < wantedHeight){					
						frm.height = wantedHeight+"px";
					}
				}else if(frm.Document && frm.Document.body.scrollHeight){
					if (subWeb.body.scrollHeight < wantedHeight){					
						frm.height = wantedHeight+"px";
					}
					else{
						frm.height = (subWeb.body.scrollHeight)+"px";
					}				
				}
			}
		}		
	}
}

var _directConfigPortletBox;
function directConfigDecoratorRequest(portletId){
	if (!_directConfigPortletBox){
		_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'420px'});
	}
	var url = '/portal/index?PagePortletConfig&menuItemId=<%=menuItem.getMenuId()%>&portletId='+portletId;
	_directConfigPortletBox.sendRequest(url);	
}


function retrieveFavoritePages(){
	var url = "/portal/index?Portal&actionType=retrieveFavoritePages&navId=<%=navigaterId%>";
	sendRequest(url,{onComplete:function(responseText){
		$("#favoriteMenu #splitLine").nextAll().remove();
		$("#favoriteMenu #splitLine").after(responseText);
		
		$("#favoriteMenu #splitLine").nextAll().hover(function() {
			$(this).children('a:first').addClass("hov");
		}, function() {
			$(this).children('a:first').removeClass("hov");		
		});			
	}});	
}

$(function(){
	$(".content > h3").hover(
	  function () {
	    $(this).find("span[id=stateAnchor]").fadeIn('fast');
	  }, 
	  function () {
	    $(this).find("span[id=stateAnchor]").fadeOut('fast');
	  }
	);
	$(".content > h3").find("span[id=stateAnchor]").hide();
	PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
}); 

function resetThemeHeight(){
	var portletbodyHeightPx = $(".portletbody").css("height");
	var portletbodyHeight = portletbodyHeightPx.substring(0,portletbodyHeightPx.length-2);
	if(portletbodyHeight < <%=themeBodyHeight%>){
		portletbodyHeight = <%=themeBodyHeight%>+35;
	}else{
		portletbodyHeight = parseInt(portletbodyHeight)+30;		
	}
	$(".left-menu").css("height",portletbodyHeight+"px");
}
</script>
</head>
<body onLoad="checkForRefresh();resetThemeHeight();">
<input type="hidden" name="currentUserId" id="currentUserId" value="<%=user.getUserCode()%>" />
<div class="container">
  <div class="header">
  	<a href="#" class="fltlft"><img src="/portal/themes/in/standard_green/images/logo_platform.png" /></a> 
    <div class="wrap_all"> 
    <div id="top" >
      <div class="navwrap"></div>                                
    </div>
</div>
<div style=" clear:both"></div>
</div>
<div>
   	<div class="left-menu">
       <ul>
      	<a id="desktopmenu" href="#"><li id="indexLi" class="index"><span><%=resourceBundle.getString("CommonMenu.workbench")%></span></li></a>
      	<a href="http://mail.163.com" target="_blank"><li id="mailLi" class="mail"><span><%=resourceBundle.getString("CommonMenu.mail")%></span></li></a>
      	<li id="favoriteLi" class="favorite"><span><a href="javascript:void(0)"><%=resourceBundle.getString("CommonMenu.favorites")%></a></span></li>
      	<li id="personLi" class="person"><span><a href="javascript:void(0)"><%=resourceBundle.getString("CommonMenu.personalSetting")%></a></span></li>
      	<li id="helpLi" class="help"><span><a href="javascript:void(0)"><%=resourceBundle.getString("CommonMenu.help")%></a></span></li>
      </ul>
    </div>
    <div class="main-content">
      <div class="nav-info">
		<span id="logout" style="float:right"><%=resourceBundle.getString("CommonMenu.hello")%>,<%=user.getUserName()%>&nbsp;&nbsp;<%if (isAdmin){%><a href="javascript:void(0)" class="button-link green" onClick="changeMode('<c:out value="${pageContext.request.contextPath}"/>')" ><%=modeText%></a>&nbsp;<a hideFocus="true" href="javascript:window.location='/portal/forward?00'" target="_blank" class="button-link green" ><%=resourceBundle.getString("CommonMenu.console")%></a>&nbsp;<%}%><a href="javascript:void(0)" class="button-link orange" onClick="logout()"><%=resourceBundle.getString("CommonMenu.exitSystem")%></a></span><%if (isPersonalSetting){%><span id="personalToolBar" style="float:right"><a href="javascript:void(0)" class="button-link green" onclick="addPortletRequest('<%=menuItem.getMenuId()%>','<%=menuItem.getPageId()%>')"><%=resourceBundle.getString("CommonMenu.add")%></a>&nbsp;&nbsp;<a href="javascript:void(0)" class="button-link orange" onclick="savePersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.save")%></a>&nbsp;<a href="javascript:void(0)" class="button-link orange" onclick="resetPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.reset")%></a>&nbsp;<a href="javascript:void(0)" class="button-link orange" onclick="cleanPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.empty")%></a>&nbsp;<a href="javascript:void(0)" class="button-link orange" onclick="stopPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.close")%></a>&nbsp;&nbsp;</span><%}%><span id="now_place" style="background:url(/portal/themes/in/standard_green/images/ico_home_16.png) left center no-repeat;padding-left: 18px;"><%=resourceBundle.getString("CommonMenu.currentLocation")%>:<%=currentVisitPath%></span>
      </div>
    </div>
    <div class="portletbody" style="float:right;width:1130px;">
		 <pt:layout />
	</div>
	</div>
	<div class="footer" style="float:right;width:1200px;">
	 <div style="float:left; padding:1px 5px;"> <span class="fltlft">数通畅联软件技术有限公司</span></div>
	 <div style="float:right; padding:1px 5px;">
		 <span style="margin:auto 30px;"><%=resourceBundle.getString("CommonMenu.LanguageSetting")%>：<a class="normal" href="javascript:changeLanguage('zh_CN');"><%=resourceBundle.getString("CommonMenu.chinese")%></a> | <a class="normal" href="javascript:changeLanguage('en_US');"><%=resourceBundle.getString("CommonMenu.english")%></a></span>
		 <span class="fltrt">Copyright © agileai.com </span>
	 </div>
	</div>	
<%if (isPersonalSetting && isUsePersonalSetting){%>    
	<div id="portletPicker">
		<div class="header">
		<span class="title">待选Portlet列表</span>
		<span style="width: 20px; height: 20px;float:right; vertical-align: middle; margin: 0px; padding: 3px 3px; text-align: center; display: inline-block;" onmousemove="PopupBox.onMover(this)" onmouseout="PopupBox.onMout(this)"><img id="_personalWidthSetupBoxImgBtn" onclick="javascript:$('#portletPicker').hide();" src="/portal/images/close.gif" width="15" height="15" style="margin:0px;cursor:pointer" alt="关闭" title="关闭"></span>		
		</div>
		<div class="content"></div>
	</div>
<%}%> 
<div id="personalUL">
	<ul id="favoriteMenu">
		<%if(!isPersonalSetting){%><li><a href="javascript:addFavoritePage('<%=menuBar.getId()%>','<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.addFavorites")%></a></li>
	    <li><a href="javascript:personalFavoriteConfigManageBox('<%=menuBar.getId()%>','<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.managFavorites")%></a></li><%}%>
	    <li id="splitLine" style="height:14px;"><a style="height:5px;line-height:5px;" href="javascript:void(0)">------------</a></li>
	</ul>   
   <ul id="personalMenu">
		<li><a href="javascript:openModifyPasswordBox()"><%=resourceBundle.getString("CommonMenu.changedPassword")%></a></li>   
	    <li><a href="javascript:openPersonalThemeBox('<%=navigaterId%>')"><%=resourceBundle.getString("CommonMenu.themeSetting")%></a></li>
<%if ("Y".equals(isOnPersonalPage)){%>
	    <%if(hasPersonalSetting && isUsePersonalSetting){%><li><a href="javascript:personalWidthSetupBoxRequest('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.layoutWidth")%></a></li><%}%>
	    <li><%if (!isPersonalSetting){%><a href="javascript:startPersonalSetting('<%=navigaterId%>','<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.activation")%></a><%}else{%><a href="javascript:stopPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.closeActivation")%></a><%}%></li>
	    <%if(hasPersonalSetting){%>
	    <li><%if (!isUsePersonalSetting){%><a href="javascript:applyPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.appSetting")%></a><%}else{%><a href="javascript:cancelPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.cancelSetting")%></a><%}%></li>
	    <%}%>
<%}%>	    
	</ul>
</div>
</div>
</body>
</html>
<script language="javascript">
var menudata = <%=menuBarJson%>;
var topMenuSelected;
var topMenuSelectedUrl;
var subMenuSelected;
var topMenuSelectedId;
var menuHtml = "";
var isOnDesktopPage = false;

function createMenu(){
	var data = menudata.menus;
	menuHtml = menuHtml + '<ul id="nav">';
	for(var i = 0; i < data.length; i++) {
		var itemData = data[i];
		if (itemData.selected){
			topMenuSelected = itemData.text;
			topMenuSelectedUrl = itemData.href;
			topMenuSelectedId = itemData.name;
		}
		var name = itemData.name;
		var itemType = itemData.type;
		var itemText = itemData.text;
		var submenuHtml = "";
		if(itemType == 'folder'){
			menuHtml = menuHtml + '<li class="current_page_item"><a href="javascript:void(0)" class="page_item_folder">'+itemText+'</a><ul class="children">';
			for (var j=0;j < itemData.menus.length;j++){
				var submenuData = itemData.menus[j];
				buildMenuHtml(submenuData);
			}
			menuHtml = menuHtml + '</ul></li>'; 
		}
		else if (itemType == 'page'){
			var itemURL = itemData.href;
			var itemTarget = itemData.target;
			if (i==0){
				$("#desktopmenu").attr("href",itemURL);
				if (topMenuSelectedUrl== itemURL){
					$("#indexLi").removeClass("index").addClass("index_active");
					isOnDesktopPage = true;
				}				
			}else{
				menuHtml = menuHtml + '<li class="current_page_item"><a href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';				
			}
		}
		else if (itemType == 'link'){
			var itemURL = itemData.href;
			var itemTarget = itemData.target;
			menuHtml = menuHtml + '<li class="current_page_item"><a hideFocus="true" href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';				
		}
	}
	menuHtml = menuHtml + '</ul>'; 
	$('.navwrap').append(menuHtml);
}

function buildMenuHtml(itemData){
	var name = itemData.name;
	var itemType = itemData.type;
	var itemText = itemData.text;
	var submenuHtml = "";
	if(itemType == 'folder'){
		menuHtml = menuHtml + '<li class="page_item"><a href="javascript:void(0)">'+itemText+'</a><ul class="children">';
		for (var j=0;j < itemData.menus.length;j++){
			var submenuData = itemData.menus[j];
			buildMenuHtml(submenuData);
		}
		menuHtml = menuHtml + '</ul></li>'; 
	}
	else if (itemType == 'page'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li class="page_item"><a href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
	else if (itemType == 'link'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li class="page_item"><a hideFocus="true" href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
}
$(function(){
	$(".index").hover(function(){
		$(this).removeClass("index").addClass("index_active");			
	},function(){
		if (!isOnDesktopPage){
			$(this).removeClass("index_active").addClass("index");			
		}
	});
	$(".mail").hover(function(){
		$(this).removeClass("mail").addClass("mail_active");
	},function(){
		$(this).removeClass("mail_active").addClass("mail");
	});
	$(".favorite").hover(function(){
		$(this).removeClass("favorite").addClass("favorite_active");
	},function(){
		$(this).removeClass("favorite_active").addClass("favorite");
	});
	$(".person").hover(function(){
		$(this).removeClass("person").addClass("person_active");
	},function(){
		$(this).removeClass("person_active").addClass("person");
	});
	$(".help").hover(function(){
		$(this).removeClass("help").addClass("help_active");
	},function(){
		$(this).removeClass("help_active").addClass("help");
	});
	createMenu();
	$("#nav").append("<li class='current_page_item' style='width:1px'></li>");
	$(".children").parent(".page_item").find("a:first").addClass("right-arrow");
	
	retrieveFavoritePages();
});

<%if ("edit".equals(windowMode)){%>
$(function() {
	$(".sharp .content").resizable({handles: "s",stop:function(event,ui){
		var height = ui.size.height;
		var portletId = $(this).attr("portletId");
		var url = "/portal/index?Portal&actionType=resizePortletHeight&portletId="+portletId+"&height="+height;
		sendRequest(url,{onComplete:function(responseText){
			resetWindow();
		}});
	}});
});
<%}%>

<%if (isPersonalSetting && isUsePersonalSetting){%>
$(function() {
	$("#portletPicker").draggable({ handle:'.header'});
	$(".sharp .content").resizable({handles: "s",stop:function(event,ui){
		var height = ui.size.height;
		var portletId = $(this).attr("portletId");
		var url = "/portal/index?Portal&actionType=resizePortletHeight&menuItemId=<%=menuItem.getMenuId()%>&portletId="+portletId+"&height="+height;
		sendRequest(url,{onComplete:function(responseText){
			resetWindow();
		}});
	}});
	$(".draggable").draggable({revert:true,helper:'clone'});
	$(".droppable").droppable({hoverClass:"ui-state-active",
	accept:".draggable,.drag-item",
		drop:function(event, ui){
			var dragPortletId = ui.draggable.attr('portletId');
			var dragColumnIndex = ui.draggable.attr('columnIndex');
			
			var dropPortletId = $(this).attr('portletId');
			var dropColumnIndex = $(this).attr('columnIndex');
			
			if (dropPortletId == dragPortletId){
				return;
			}else{
				var url = "/portal/index?Portal&actionType=dragdropPortlet&menuItemId=<%=menuItem.getMenuId()%>&dragPortletId="+dragPortletId
					+"&dragColumnIndex="+dragColumnIndex+"&dropPortletId="+dropPortletId+"&dropColumnIndex="+dropColumnIndex;
				sendRequest(url,{onComplete:function(responseText){
					resetWindow();
				}});
			}
		}
	}); 
});
<%}%>


$(document).ready(function() {	
	$('#personLi').hover(function() {
		var pos = $("#personLi").position();
		$("#personalMenu").css({left:(pos.left+71),top:pos.top,zIndex:999})
		$('#personalMenu').fadeIn(300);
	},function() {
		$('#personalMenu').fadeOut(1);	
	});

	$('#personalMenu').hover(function() {
		$("#personLi").removeClass("person").addClass("person_active");
		$('#personalMenu').fadeIn(1);
	},function() {
		$("#personLi").removeClass("person_active").addClass("person");
		$('#personalMenu').fadeOut(5);
	});
	
	
	$('#favoriteLi').hover(function() {
		var pos = $("#favoriteLi").position();
		$("#favoriteMenu").css({left:(pos.left+71),top:pos.top,zIndex:999})
		$('#favoriteMenu').fadeIn(300);
	}, function() {
		$('#favoriteMenu').fadeOut(1);
	});
	$('#favoriteMenu').hover(function() {
		$("#favoriteLi").removeClass("favorite").addClass("favorite_active");
		$('#favoriteMenu').fadeIn(1);
	}, function() {
		$("#favoriteLi").removeClass("favorite_active").addClass("favorite");
		$('#favoriteMenu').fadeOut(5);
	});
	
	
	$('#personalMenu li,#favoriteMenu li').hover(function() {
		$(this).children('a:first').addClass("hov");
	}, function() {
		$(this).children('a:first').removeClass("hov");		
	});
});
function changeLanguage(locale){
	var url = "/portal/resource?PreferSetting&actionType=configLanguage&locale="+locale;
	sendRequest(url,{onComplete:function(responseText){
		window.location.reload();
	}});		
}
</script>