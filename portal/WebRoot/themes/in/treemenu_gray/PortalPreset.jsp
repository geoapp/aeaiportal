<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.agileai.portal.driver.model.Theme"%>
<%@page import="org.codehaus.jettison.json.JSONArray"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.agileai.portal.extend.theme.TreeMenuHelper"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.hotweb.domain.core.Profile"%>
<%@ page import="com.agileai.hotweb.domain.core.User"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%@ page import="org.codehaus.jettison.json.JSONObject"%>
<%@ page import="org.codehaus.jettison.json.JSONArray"%>
<%@ page import="com.agileai.hotweb.i18n.BundleContext"%>
<%@ page import="com.agileai.hotweb.i18n.ResourceBundle"%>
<%@ page import="com.agileai.portal.extend.theme.ThemeHelper"%>
<% 
ResourceBundle resourceBundle = BundleContext.getOnly().getResourceBundle();
resourceBundle.setLocale((HttpServletRequest)request);

Profile profile = (Profile)request.getSession(false).getAttribute(Profile.PROFILE_KEY);
User user = (User)profile.getUser();
boolean isAdmin = "admin".equals(user.getUserCode());
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
JSONObject menuBarJsonObject = menuBar.getMenuBarJsonObject(user,menuItem.getCode(),request);

String currentVisitPath = menuBar.getCurrentPath(request,menuItem);
String navigaterId = menuBar.getId();

String windowState= (String)request.getAttribute("WINDOW_STATE_KEY");
String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
String personalSetting =(String)request.getAttribute("PERSONAL_SETTING_MODE_KEY");
String isOnPersonalPage = (String)request.getAttribute("IS_ON_PERSONAL_PAGE");
String usePersonalSetting =(String)request.getAttribute("PERSONAL_SETTING_USE_KEY");
Object personalSettingObject = request.getAttribute("PERSONAL_SETTING_OBJECT_KEY");
boolean isPersonalSetting = "Y".equals(personalSetting);
boolean isUsePersonalSetting = "Y".equals(usePersonalSetting);
boolean hasPersonalSetting = false;
if (personalSettingObject != null){
	hasPersonalSetting = true;
}

String modeText = "view".equals(windowMode)?resourceBundle.getString("CommonMenu.editComponent"):resourceBundle.getString("CommonMenu.displayComponent");
String hostIp = request.getRemoteAddr();
if ("0:0:0:0:0:0:0:1".equals(hostIp)){
	hostIp = "localhost";
}

String windowScreenHeight = ThemeHelper.getWindowScreenHeight((HttpServletRequest)request);
Theme theme = (Theme)request.getAttribute("_currentTheme_");
String outherHeight = theme.getOuterHeight();
int themeBodyHeight = Integer.parseInt(windowScreenHeight)-Integer.parseInt(outherHeight);

HttpSession httpSession = request.getSession(false);
String httpSessionId = httpSession.getId();
TreeMenuHelper treeMenuHelper = new TreeMenuHelper(httpSession,menuBarJsonObject);
treeMenuHelper.setShowHomePage(false);
String menuTreeSyntax = treeMenuHelper.buildMenuSyntax();
%>
<html>
<head>
<title>企业应用中心</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="keywords" content="<%=menuItem.getKeywords()%>" />
<meta name="description" content="<%=menuItem.getDescription()%>" />

<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/layout.css"  type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css"  type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" type="text/css" >

<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skins/blue.css" type="text/css" />	
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/boxy.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/gt_grid.css" type="text/css" />	
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/china/skinstyle.css"  type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/vista/skinstyle.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/skin/mac/skinstyle.css" type="text/css" />
<%if ("edit".equals(windowMode) || "Y".equals(isOnPersonalPage)){%>	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.ui.core.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.ui.resizable.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.qtip.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
<%}%>

<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/themes/in/treemenu_gray/css/default/easyui.css"  type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/themes/in/treemenu_gray/css/icon.css"  type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/themes/in/treemenu_gray/css/style.css"  type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/themes/in/treemenu_gray/css/tabso.css"  type="text/css" />

<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=ThemeHelper.getReletivePath(theme,menuItem.getCustomCssURL())%>" type="text/css" />
<%}%>

<style type="text/css">
	body, tbody, th, td{
		font-family:Verdana,Arial,Helvetica,sans-serif;
		font-size:12px;
		margin:0;
		padding:0;
	}
	.headerContainer div.selected {
		background-color:#1A4275;
		color:white; 
	}
	.headerContainer div.selected a{
		color:white; 
	}	
	#personalUL { 
		margin: 0; padding: 0; 
		list-style: none; border-left: 1px solid #d5dce8; 
		border-right: 1px solid #d5dce8; border-bottom: 1px solid #d5dce8; 
		border-bottom-left-radius: 4px; 
		-moz-border-radius-bottomleft: 4px; 
		-webkit-border-bottom-left-radius: 4px; 
		border-bottom-right-radius: 4px; 
		-moz-border-radius-bottomright: 4px; 
		-webkit-border-bottom-right-radius: 4px; 
		height:0px; 
		padding-left: 0px; 
		padding-right:0px; 
		background: #edf3f7; 
	}
	#personalUL li { float: right;
		display: block; background: none; height:20px;
		position: relative; z-index: 999; 
		margin: 0 1px; 
	}
	#personalUL li a { display: block; padding: 0; font-weight: 700; line-height: 20px;
	 	text-decoration: none;  color: #818ba3; zoom: 1; border-left: 1px solid transparent; border-right: 1px solid transparent; padding: 0px 12px; 
	}
	#personalUL li a:hover, #personalUL li a.hov,#personalUL li ul li a.hov { background-color: #fff; border-left: 1px solid #d5dce8; border-right: 1px solid #d5dce8; color: #576482; }
	#personalUL ul { 
		position:absolute; 
		display: none; margin: 0; padding: 0; list-style: none; 
		-moz-box-shadow: 0 1px 3px rgba(0,0,0,0.2); -o-box-shadow: 0 1px 3px rgba(0,0,0,0.2); 
		box-shadow: 0 1px 3px rgba(0,0,0,0.2); -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.2); 
		padding-bottom: 1px; 
		background: #edf3f7;
		width: 130px;
	}
	#personalUL ul li { width:128px; float: left; border-top: 0px solid #fff; text-align: left;height:30px; line-height: 30px;}
	#personalUL ul a { display: block; padding: 3px 5px; color: #666; border-bottom: 1px solid transparent; text-transform:  uppercase; color: #797979; font-weight: normal; }
	
	.draggable {
		padding-left:15px;
		z-index:999;
		background-image: url('<c:out value="${pageContext.request.contextPath}"/>/images/layout/objectdrag.gif');
		background-repeat: no-repeat;
		background-position-x: 7px;
	}
	.droppable {min-height:30px;}
	.ui-state-active{
		background-color:red;
		cursor: pointer;
	}
	#portletPicker{
		z-index: 9999;
		border: 1px outset rgb(102, 153, 204);
		background-color:white;position:absolute;
		display:none;
		height:400px;
		width:200px;
	}
	#portletPicker .header{
		background: url('<c:out value="${pageContext.request.contextPath}"/>/images/decorator/bg2.gif') repeat-x;
		margin:0;
		padding:0;
		padding-top:2px;
		display:block;
		cursor:pointer;
		height:26px;
		line-height:24px;
		color:white;
		padding-left:10px;
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-size: 13px;
	}
	#portletPicker .header .title{
		margin:0;
		padding:0;
		padding-left:4px;
		padding-left: 15px;
		background-image: url('/portal/images/layout/objectdrag.gif');
		background-repeat:no-repeat;
		font-weight: bold;
	}
	#portletPicker .content {
		margin: 2px;
		height:370px;
		overflow:auto;
	}
	
	#portletPicker .content .drag-item {
		list-style-type:none;
		display: block;
		padding: 5px;
		border: 1px solid #ccc;
		margin: 2px;
		background: #fafafa;
		color: #444;
	}
    </style>
	
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery1.7.1.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/FusionCharts_pc.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.hoverIntent.minified.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.boxy.iframe.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>    
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>
    
    <script src="<c:out value="${pageContext.request.contextPath}"/>/themes/in/treemenu_gray/js/jquery.easyui.min.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/themes/in/treemenu_gray/js/AccordionMenus.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/themes/in/treemenu_gray/js/jquery.tabso.js" language="javascript"></script>
    
	<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_msg_cn.js"></script>
	<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/js/gt_grid_all.js"></script>
	<script type="text/javascript" src="/portal_portlets/js/amq_jquery_adapter.js"></script>
	<script type="text/javascript" src="/portal_portlets/js/amq.js"></script>
	
<%if ("edit".equals(windowMode) || "Y".equals(isOnPersonalPage) ){%>    
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.core.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.widget.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.mouse.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.draggable.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.droppable.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.ui.resizable.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/PersonalConfig.js" language="javascript"></script>
    <script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.qtip.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/imagesloaded.pkgd.js" language="javascript"></script>  
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>	      
<%}%>

<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
	<script type="text/javascript" src="<%=ThemeHelper.getReletivePath(theme,menuItem.getCustomJsURL())%>"></script>
<%}%>	
<script>
$.ajaxSetup({cache:false});

var amq = org.activemq.Amq;
amq.init({ uri: '/portal_portlets/amq', logging: true, timeout: 45, clientId:'<%=httpSessionId%>'});	

var __renderPortlets = new Map();	
var turnRef = 62;
<%if("normal".equals(windowState)){ %>
	turnRef = 168;
<%}%>
function resetPageBodyHeight(id){
	if (ele(id)){
		var frm=document.getElementById(id);
		var wantedHeight = $(window).height()-turnRef;
		//alert('wantedHeight is ' + wantedHeight);
		var subWeb=document.frames?document.frames[id].document:frm.contentDocument;
		if (document.getElementById){
			if (frm && !window.opera){
				if (frm.contentDocument && frm.contentDocument.body.offsetHeight){
					frm.height = frm.contentDocument.body.offsetHeight;
					if (frm.height < wantedHeight){					
						frm.height = wantedHeight+"px";
					}
				}else if(frm.Document && frm.Document.body.scrollHeight){
					if (subWeb.body.scrollHeight < wantedHeight){					
						frm.height = wantedHeight+"px";
					}
					else{
						frm.height = (subWeb.body.scrollHeight)+"px";
					}				
				}
			}
		}		
	}
}

var _directConfigPortletBox;
function directConfigDecoratorRequest(portletId){
	if (!_directConfigPortletBox){
		_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'420px'});
	}
	var url = '/portal/index?PagePortletConfig&menuItemId=<%=menuItem.getMenuId()%>&portletId='+portletId;
	_directConfigPortletBox.sendRequest(url);	
}


function retrieveFavoritePages(){
	var url = "/portal/index?Portal&actionType=retrieveFavoritePages&navId=<%=navigaterId%>";
	sendRequest(url,{onComplete:function(responseText){
		$("#favoriteMenu #splitLine").nextAll().remove();
		$("#favoriteMenu #splitLine").after(responseText);
		
		$("#favoriteMenu #splitLine").nextAll().hover(function() {
			$(this).children('a:first').addClass("hov");
		}, function() {
			$(this).children('a:first').removeClass("hov");		
		});			
	}});	
}
function resetWindowWidth(){
    var computeWidth = $(document.body).width();
	if (computeWidth < screen.availWidth-50){
		computeWidth = screen.availWidth;
	}    
    $("#bodyContainer").css({width:computeWidth});   
}

function resetThemeHeight(){
	var portletbodyHeightPx = $(".portletbody").css("height");
	var portletbodyHeight = portletbodyHeightPx.substring(0,portletbodyHeightPx.length-2);
	if(portletbodyHeight < <%=themeBodyHeight%>){
		portletbodyHeight = <%=themeBodyHeight%>+5;
	}
	$("#AccordionContainer").css('height',portletbodyHeight+"px");		
	resetAccordionHeight();
}

function triggleMenu()
{       
	if ($(".leftmenu").css("display") != 'none')
	{
		$('#spliterImg').attr('src','/portal/themes/in/treemenu_gray/images/spliter1.gif');
		$(".leftmenu").css("display","none");
		$(".pbody").css("margin-left","0px");
	}
	else
	{
		$('#spliterImg').attr('src','/portal/themes/in/treemenu_gray/images/spliter0.gif');
		$(".leftmenu").css("display","block");
		$(".pbody").css("margin-left","201px");
	}
}

$(function(){
	$(".content > h3").hover(
	  function () {
	    $(this).find("span[id=stateAnchor]").fadeIn('fast');
	  }, 
	  function () {
	    $(this).find("span[id=stateAnchor]").fadeOut('fast');
	  }
	);
	$(".content > h3").find("span[id=stateAnchor]").hide();
	PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
}); 
</script> 


</head>
<body onLoad="checkForRefresh();resetWindowWidth();resetThemeHeight();" style="margin:0px; padding:0px;">
<input type="hidden" name="currentUserId" id="currentUserId" value="<%=user.getUserCode()%>" />
<div id="bodyContainer" class="container">
<div class="header">
	<div class="logo" style="float:left;">
	<img src="/portal/themes/in/treemenu_gray/images/logo.gif" />
	</div>
	<ul id="top-nav" style="float:right;width: 600px;">
		<li class="nav-item"><a href="javascript:void(0)" onClick="logout()"><img src="/portal/themes/in/treemenu_gray/images/menu8.png" alt="" /><br /><%=resourceBundle.getString("CommonMenu.exitSystem")%></a></li>
		<%if (isAdmin){%>
		<li class="nav-item"><a  href="javascript:window.location='/portal/forward?00'" target="_blank" ><img src="/portal/themes/in/treemenu_gray/images/menu7.png" alt="" /><br /><%=resourceBundle.getString("CommonMenu.console")%></a></li>
		<li class="nav-item"><span style="width:80px;height:68px;"><a href="javascript:void(0)" onClick="changeMode('<c:out value="${pageContext.request.contextPath}"/>')"  style="text-align:center;"><img src="/portal/themes/in/treemenu_gray/images/menu6.png" alt="" /><br/><%=modeText%></a>
		</span>
	   	</li>
		<%}%>
		</li>
		<li class="nav-item"><a href="#"><img src="/portal/themes/in/treemenu_gray/images/menu5.png" alt="" /><br /><%=resourceBundle.getString("CommonMenu.help")%></a></li>
		<li id="personLi" class="nav-item"><a href="#"><img src="/portal/themes/in/treemenu_gray/images/menu4.png" alt="" /><br /><%=resourceBundle.getString("CommonMenu.personalSetting")%></a></li>
		<li id="favoriteLi" class="nav-item"><a href="#"><img src="/portal/themes/in/treemenu_gray/images/menu3.png" alt="" /><br /><%=resourceBundle.getString("CommonMenu.favorites")%></a></li>
		<li class="nav-item"><a href="http://mail.163.com" target="_blank"><img src="/portal/themes/in/treemenu_gray/images/menu2.png" alt="" /><br/><%=resourceBundle.getString("CommonMenu.mail")%></a></li>
		<li class="nav-item"><a id="desktopmenu" href="" class="hover"><img src="/portal/themes/in/treemenu_gray/images/menu1.png" alt="" /><br /><%=resourceBundle.getString("CommonMenu.workbench")%></a></li>
	</ul>
</div>

<div class="pagebody" style="margin:0px;padding:0px;">
	<div class="leftmenu" style="float:left;width:201px;height:auto; ">
		<div style="float:left;position:relative">
			<img src="/portal/themes/in/treemenu_gray/images/menuleft.gif" width="200"  />
		</div>
		<div id="AccordionContainer" class="AccordionContainer" style="min-height:400px;margin-top:34px">
			<%=menuTreeSyntax%>
		</div>	
	</div>
	<div class="pbody" style="border-left:1px solid #CACACA;  margin-left:201px; border-top:1px solid #CACACA">
		<div class="nav-info" style="background:#FCFCFC;height:33px;line-height:34px;border-bottom:1px solid #CACACA;">
			<span style="display:block;float:left;height:33px;line-height:33px;">
			<img style="margin-top: 4px;margin-left: 1px;margin-right: 3px;" src="/portal/themes/in/treemenu_gray/images/spliter0.gif" border="0" onclick="javascript:triggleMenu();" id="spliterImg" name="spliterImg">
			</span>
			<span id="now_place" style="background:url(/portal/themes/in/standard_green/images/ico_home_16.png) left center no-repeat;padding-left: 18px; line-height:33px;"><%=resourceBundle.getString("CommonMenu.currentLocation")%>:<%=currentVisitPath%>
			</span>
			<span id="logout" style="float:right; line-height:33px;"><%=resourceBundle.getString("CommonMenu.hello")%>,<%=user.getUserName()%>&nbsp;&nbsp;</span>
			<%if (isPersonalSetting){%><span id="personalToolBar" style="float:right"><a href="javascript:void(0)" class="button-link green" onclick="addPortletRequest('<%=menuItem.getMenuId()%>','<%=menuItem.getPageId()%>')"><%=resourceBundle.getString("CommonMenu.add")%></a>&nbsp;&nbsp;<a href="javascript:void(0)" class="button-link orange" onclick="savePersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.save")%></a>&nbsp;<a href="javascript:void(0)" class="button-link orange" onclick="resetPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.reset")%></a>&nbsp;<a href="javascript:void(0)" class="button-link orange" onclick="cleanPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.empty")%></a>&nbsp;<a href="javascript:void(0)" class="button-link orange" onclick="stopPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.close")%></a>&nbsp;&nbsp;</span><%}%>			
	  	</div>		
		<div class="portletbody" style="border:0px;">
			<pt:layout />
		</div>
	</div>
</div>
<div id="footer" style="border-top:1px solid #CACACA; position:relative">
	<div id="copyright">
		<div style="float:left; padding:14px 10px 10px 10px;"> 沈阳数通畅联软件技术有限公司.&copy; 版权所有 
		</div>
		<div style="float:right; padding:14px 10px 10px 10px;">
        	<span style="margin:auto 30px;"><%=resourceBundle.getString("CommonMenu.LanguageSetting")%>：<a class="normal" href="javascript:changeLanguage('zh_CN');"><%=resourceBundle.getString("CommonMenu.chinese")%></a> | <a class="normal" href="javascript:changeLanguage('en_US');"><%=resourceBundle.getString("CommonMenu.english")%></a></span>
			<a href="http://www.miitbeian.gov.cn/" style="color:#772c17;">辽ICP备14005586号</a>
		</div>
    </div>
</div>

<%if (isPersonalSetting && isUsePersonalSetting){%>    
	<div id="portletPicker">
		<div class="header">
		<span class="title">待选Portlet列表</span>
		<span style="width: 20px; height: 20px;float:right; vertical-align: middle; margin: 0px; padding: 3px 3px; text-align: center; display: inline-block;" onmousemove="PopupBox.onMover(this)" onmouseout="PopupBox.onMout(this)"><img id="_personalWidthSetupBoxImgBtn" onclick="javascript:$('#portletPicker').hide();" src="/portal/images/close.gif" width="15" height="15" style="margin:0px;cursor:pointer" alt="关闭" title="关闭"></span>		
		</div>
		<div class="content"></div>
	</div>
<%}%> 
<div id="personalUL">
	<ul id="favoriteMenu">
		<%if(!isPersonalSetting){%><li><a href="javascript:addFavoritePage('<%=menuBar.getId()%>','<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.addFavorites")%></a></li>
	    <li><a href="javascript:personalFavoriteConfigManageBox('<%=menuBar.getId()%>','<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.managFavorites")%></a></li><%}%>
	    <li id="splitLine" style="height:14px;"><a style="height:5px;line-height:5px;" href="javascript:void(0)">------------</a></li>
	</ul>   
   <ul id="personalMenu">
		<li><a href="javascript:openModifyPasswordBox()"><%=resourceBundle.getString("CommonMenu.changedPassword")%></a></li>   
	    <li><a href="javascript:openPersonalThemeBox('<%=navigaterId%>')"><%=resourceBundle.getString("CommonMenu.themeSetting")%></a></li>
<%if ("Y".equals(isOnPersonalPage)){%>
	    <%if(hasPersonalSetting && isUsePersonalSetting){%><li><a href="javascript:personalWidthSetupBoxRequest('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.layoutWidth")%></a></li><%}%>
	    <li><%if (!isPersonalSetting){%><a href="javascript:startPersonalSetting('<%=navigaterId%>','<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.activation")%></a><%}else{%><a href="javascript:stopPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.closeActivation")%></a><%}%></li>
	    <%if(hasPersonalSetting){%>
	    <li><%if (!isUsePersonalSetting){%><a href="javascript:applyPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.appSetting")%></a><%}else{%><a href="javascript:cancelPersonalSetting('<%=menuItem.getMenuId()%>')"><%=resourceBundle.getString("CommonMenu.cancelSetting")%></a><%}%></li>
	    <%}%>
<%}%>	    
	</ul>
</div>
</div>
</body>
</html>
<script language="javascript">
$(function(){
	retrieveFavoritePages();
});

<%if ("edit".equals(windowMode)){%>
$(function() {
	$(".sharp .content").resizable({handles: "s",stop:function(event,ui){
		var height = ui.size.height;
		var portletId = $(this).attr("portletId");
		var url = "/portal/index?Portal&actionType=resizePortletHeight&portletId="+portletId+"&height="+height;
		sendRequest(url,{onComplete:function(responseText){
			resetWindow();
		}});
	}});
});
<%}%>

<%if (isPersonalSetting && isUsePersonalSetting){%>
$(function() {
	$("#portletPicker").draggable({ handle:'.header'});
	$(".sharp .content").resizable({handles: "s",stop:function(event,ui){
		var height = ui.size.height;
		var portletId = $(this).attr("portletId");
		var url = "/portal/index?Portal&actionType=resizePortletHeight&menuItemId=<%=menuItem.getMenuId()%>&portletId="+portletId+"&height="+height;
		sendRequest(url,{onComplete:function(responseText){
			resetWindow();
		}});
	}});
	$(".draggable").draggable({revert:true,helper:'clone'});
	$(".droppable").droppable({hoverClass:"ui-state-active",
	accept:".draggable,.drag-item",
		drop:function(event, ui){
			var dragPortletId = ui.draggable.attr('portletId');
			var dragColumnIndex = ui.draggable.attr('columnIndex');
			
			var dropPortletId = $(this).attr('portletId');
			var dropColumnIndex = $(this).attr('columnIndex');
			
			if (dropPortletId == dragPortletId){
				return;
			}else{
				var url = "/portal/index?Portal&actionType=dragdropPortlet&menuItemId=<%=menuItem.getMenuId()%>&dragPortletId="+dragPortletId
					+"&dragColumnIndex="+dragColumnIndex+"&dropPortletId="+dropPortletId+"&dropColumnIndex="+dropColumnIndex;
				sendRequest(url,{onComplete:function(responseText){
					resetWindow();
				}});
			}
		}
	}); 
});
<%}%>
$(document).ready(function() {	
	$("#desktopmenu").attr("href","<%=treeMenuHelper.getHomePageURL()%>");	
	
	$('#personLi').hover(function() {
		var pos = $("#personLi").position();
		$("#personalMenu").css({left:pos.left,top:pos.top+60,zIndex:999})
		$('#personalMenu').fadeIn(300);
	},function() {
		$('#personalMenu').fadeOut(1);	
	});

	$('#personalMenu').hover(function() {
		$("#personLi").removeClass("person").addClass("person_active");
		$('#personalMenu').fadeIn(1);
	},function() {
		$("#personLi").removeClass("person_active").addClass("person");
		$('#personalMenu').fadeOut(5);
	});
	
	
	$('#favoriteLi').hover(function() {
		var pos = $("#favoriteLi").position();
		$("#favoriteMenu").css({left:pos.left,top:(pos.top+60),zIndex:999})
		$('#favoriteMenu').fadeIn(300);
	}, function() {
		$('#favoriteMenu').fadeOut(1);
	});
	$('#favoriteMenu').hover(function() {
		$("#favoriteLi").removeClass("favorite").addClass("favorite_active");
		$('#favoriteMenu').fadeIn(1);
	}, function() {
		$("#favoriteLi").removeClass("favorite_active").addClass("favorite");
		$('#favoriteMenu').fadeOut(5);
	});
	
	
	$('#personalMenu li,#favoriteMenu li').hover(function() {
		$(this).children('a:first').addClass("hov");
	}, function() {
		$(this).children('a:first').removeClass("hov");		
	});
	
	
	
<%
for (int i=0;i < treeMenuHelper.getTreeMenuIdList().size();i++){
	String currentId = treeMenuHelper.getTreeMenuIdList().get(i);
%>
	$('#treeMenu<%=currentId%>').tree('collapseAll');
	$('#treeMenu<%=currentId%>').tree({
		onClick:function(node){
		     $(this).tree('toggle',node.target);
		}
	});
<%}%>
	
<%
if (!"-1".equals(treeMenuHelper.getTopSelectedIndex())){
	String topSelectedIndex = treeMenuHelper.getTopSelectedIndex();
	HashMap<String, Integer> selectIndexKeys = treeMenuHelper.getSelectIndexKeys();
%>
	runAccordion(<%=treeMenuHelper.getTopSelectedIndex()%>);
	$("#accordionHeader<%=topSelectedIndex%>").addClass('selected');
	selectedAccordionIndex = <%=topSelectedIndex%>;
	<%
	if (!selectIndexKeys.isEmpty()){
		String curSelectIndex = String.valueOf(selectIndexKeys.get(topSelectedIndex));
	%>
	var node = $('#treeMenu<%=topSelectedIndex%>').tree('find', <%=curSelectIndex%>);
	$('#treeMenu<%=topSelectedIndex%>').tree('expandTo',node.target);
	$('#treeMenu<%=topSelectedIndex%>').tree('expand',node.target);
	$('#treeMenu<%=topSelectedIndex%>').tree('select',node.target);
	$('#treeMenu<%=topSelectedIndex%>').tree('check',node.target);
	var currentNodeTop = $(node.target).position().top;
	var currentContentHeightPx = $("#Accordion<%=treeMenuHelper.getTopSelectedIndex()%>Content").css("height");
	var currentContentHeight = currentContentHeightPx.substring(0,currentContentHeightPx.length-2);
	if (currentNodeTop+30 > currentContentHeight){
		var topDiff = currentNodeTop - currentContentHeight; 
		$("#Accordion<%=treeMenuHelper.getTopSelectedIndex()%>Content").scrollTop(topDiff+70);
	}
	<%}%>
<%}%>
});
$(".headerContainer .AccordionTitle").click(function(){
	$(".headerContainer .AccordionTitle").removeClass('selected');	
	$(".headerContainer .AccordionTitle0").removeClass('selected');
	$(this).toggleClass("selected");
});

function changeLanguage(locale){
	var url = "/portal/resource?PreferSetting&actionType=configLanguage&locale="+locale;
	sendRequest(url,{onComplete:function(responseText){
		window.location.reload();
	}});		
}
</script>