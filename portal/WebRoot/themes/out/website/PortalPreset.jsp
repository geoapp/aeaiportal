﻿<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
String menuBarJson = menuBar.getMenuBarJson(null,menuItem.getCode(),request);
String currentVisitPath = menuBar.getCurrentPath(request,menuItem);

String windowState= (String)request.getAttribute("WINDOW_STATE_KEY");
String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
String modeText = "view".equals(windowMode)?"编辑组件":"展示信息";
String hostIp = request.getRemoteAddr();
if ("0:0:0:0:0:0:0:1".equals(hostIp)){
	hostIp = "localhost";
}
%><head>
<meta charset="utf-8" />
<title>数通畅联软件</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="<%=menuItem.getKeywords()%>" />
<meta name="description" content="<%=menuItem.getDescription()%>" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" type="text/css" />

<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/layout.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/wcm/display.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/website/css/dcmegamenu.css" />
<link rel="stylesheet" type="text/css" href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/website/css/style.css" />
<link rel="stylesheet" type="text/css" href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/website/css/boxed.css" id="layout" />
<link rel="stylesheet" type="text/css" href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/website/css/colors/blue.css" id="colors" />
<%if ("edit".equals(windowMode)){%>	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.qtip.css" type="text/css" />
<%}%>
<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=menuItem.getCustomCssURL()%>" type="text/css" />
<%}%>	
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery1.7.1.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>    
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>

<script src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/website/js/jquery.hoverIntent.minified.js"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/website/js/jquery.dcmegamenu.1.3.3.min.js"></script>
<%if ("edit".equals(windowMode)){%>	
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.qtip.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/imagesloaded.pkgd.js" language="javascript"></script>
<%}%>
<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
	<script type="text/javascript" src="<%=menuItem.getCustomJsURL()%>"></script>
<%}%>	
	<script type="text/javascript">
	$.ajaxSetup({cache:false});
	
	var __renderPortlets = new Map();	
	var _directConfigPortletBox;
	function directConfigDecoratorRequest(portletId){
		if (!_directConfigPortletBox){
			_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'380px'});
		}
		var url = '/portal/index?PagePortletConfig&portletId='+portletId;
		_directConfigPortletBox.sendRequest(url);	
	}
	$(function(){
		<%if ("edit".equals(windowMode)){%>
		$(".content > h3").hover(
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeIn('fast');
		  }, 
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeOut('fast');
		  }
		);
		<%}%>
		$(".content > h3").find("span[id=stateAnchor]").hide();
		PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
	});
</script>
<style type="text/css">
html,body{ 
	margin:0px; 
	height:auto; 
} 
.bottomText{
	margin-left: 20px;
	height: 20px;
	line-height: 20px;
}

#scrollNewsDiv{
	height:30px;
	line-height: 30px;
	border:#ccc 0px solid;
	overflow:hidden;/* 必要元素 */
} 
#scrollNewsDiv ul{
	list-style-type:none;
	margin:0px;
}
#scrollNewsDiv ul li{
	height:30px;
	line-height: 30px;
	padding-left:10px;
	list-style-type:none;
	margin:0px;
}
#scrollNewsDiv ul li a{
	float:right;
	margin-right: 30px;
    color:#000;
    font-weight: normal;
	font-family:Verdana, Arial, Helvetica, sans-serif;
}
#scrollNewsDiv ul li a:hover{
	color:red;
}
#scroll_span{
	display:inline-block;
	width:600px;
	float:right;
} 
</style> 
</head>
<body onLoad="checkForRefresh()">
<!-- Wrapper Start -->
<div id="wrapper">
<div class="container ie-dropdown-fix">
	<div id="header" style="width:940px;margin-left:10px;">
		<span style="display:inline-block;margin-top:20px;float:left;">
			<a href="index.ptml"><img src="<c:out value="${pageContext.request.contextPath}"/>/images/sitelayout/ci.png" /></a>
		</span>
		<span style="display:inline-block;margin-top:20px;float:left;">
			<a href="index.ptml"><img src="<c:out value="${pageContext.request.contextPath}"/>/images/sitelayout/title.gif" /></a>
		</span>
		<span style="float:right;display:inline-block;margin-top:12px;">
			<a href="index.ptml" title="微信公众号：数通畅联 "><img src="/HotServer/reponsitory/images/agileai.jpg" style="width:80px;height:80px" /></a>
		</span>
		<span style="float:right;display:inline-block;margin-top:27px;width:190px;">
			<ul>
				<li style="float:left;margin:2px;height:22px;">
					<a target="_blank" href="javascript:void(0)"><img border="0" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/default/images/navimg/phone.png" alt="欢迎来电咨询" title="欢迎来电咨询"></a>
					<a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=366d7f20977a19335ecbf578959f3fbd607436af9b18fea7bd3e4f9f0710d040"><img border="0" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/default/images/navimg/group.png" alt="点击加入数通畅联QQ群" title="点击加入数通畅联QQ群"></a>
				</li>
				<li style="float:left;margin:2px;height:22px;">
					<a href="http://www.agileai.com/km" target="_blank"><img border="0" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/default/images/navimg/km.png" alt="访问知识中心" title="访问知识中心"/></a>
					<a href="http://www.agileai.com/bbs" target="_blank"><img border="0" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/default/images/navimg/forum.png" alt="访问开发社区" title="访问开发社区"/></a>
				</li>
			</ul>
		</span>
	</div>
	<div style="width:940px;margin-left:10px;">
		<div class="nav-container">
			<ul id="mega-menu-1" class="mega-menu"></ul>
		</div> 
		<div class="clear"></div>
	</div>
	<div id="navigater">
		<span id="now_place">当前位置：<%=currentVisitPath%></span>
		<span id="scroll_span">
			<div id="scrollNewsDiv" class="scrollDiv">
			</div> 
		</span>
	</div>	
</div>

 <div class="container" style="width:940px;">
  <pt:layout/>
 </div>
</div>

<script type="text/javascript">
var menudata = <%=menuBarJson%>;
var topMenuSelected;
var topMenuSelectedUrl;
var subMenuSelected;
var topMenuSelectedId;
var menuHtml = "";

createMenu();

function createMenu(){
	var data = menudata.menus;
	for(var i = 0; i < data.length; i++) {
		var itemData = data[i];
		if (itemData.selected){
			topMenuSelected = itemData.text;
			topMenuSelectedUrl = itemData.href;
			topMenuSelectedId = itemData.name;
		}
		buildMenuHtml(itemData);
	}
	$('#mega-menu-1').html(menuHtml);
	$('#mega-menu-1').dcMegaMenu({
		rowItems: '5',
		speed: 'fast',
		effect: 'fade',
		fullWidth: false
	});	
}

function buildMenuHtml(itemData){
	var name = itemData.name;
	var itemType = itemData.type;
	var itemText = itemData.text;
	if(itemType == 'folder'){
		menuHtml = menuHtml + '<li><a href="javascript:void(0)">'+itemText+'</a><ul>';
		for (var j=0;j < itemData.menus.length;j++){
			var submenuData = itemData.menus[j];
			buildMenuHtml(submenuData);
		}
		menuHtml = menuHtml + '</ul></li>'; 
	}
	else if (itemType == 'page'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li><a href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
	else if (itemType == 'link'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li><a hideFocus="true" href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
}
</script>
<div id="footer">
	<div id="bottomNav"></div>
	<div id="scroll-top-top"><a href="#"></a></div>
	<div id="footer-bottom" style="padding: 10px 0px 20px 0;"> 
		<div style="margin-top:5px;">
		<span>版权所有© 沈阳数通畅联软件技术有限公司</span>
		 <span class="bottomText"><a style="vertical-align:middle;color: #aaa;" href="http://www.miitbeian.gov.cn/"> 辽ICP备14005586号</a></span>
		 <span class="bottomText">服务支持 <a href="mailto:service@agileai.com" style="vertical-align:middle;color: #aaa;">service@agileai.com</a></span>
		 <span style="margin-left:10px;"><script type="text/javascript">
var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3Fb095c3e94483b44547a8f311f04871fd' 		type='text/javascript'%3E%3C/script%3E"));
</script></span>

		<span class="bottomText" style="float:right; margin-right: 20px;height: 20px;line-height: 20px;">
				<span><a style="vertical-align:middle;padding-right:10px;">电脑版</a></span>
				<span style="border-left: 1px solid #eee;padding-left: 14px;"><a href="/portal/website/m0/index.ptml" style="color:#FF0000">手机版</a></span>
		</span>
		</div>
	</div>
</div>
</body>
</html>
<script type="text/javascript">
var bottomHtml = "";

createBottomMenu();

function createBottomMenu(){
	var data = menudata.menus;
	for(var i = 0; i < data.length; i++) {
		var itemData = data[i];
		buildBottomMenuHtml(itemData);
		
		var itemType = itemData.type;
		var itemText = itemData.text;		
		if (itemType == 'page'){
			var itemURL = itemData.href;
			var itemTarget = itemData.target;
			bottomHtml = bottomHtml + '<span>';
			bottomHtml = bottomHtml + '<a href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a>';
			bottomHtml = bottomHtml + '</span>'; 
		}
		else if (itemType == 'link'){
			var itemURL = itemData.href;
			var itemTarget = itemData.target;
			bottomHtml = bottomHtml + '<span>';
			bottomHtml = bottomHtml + '<a hideFocus="true" href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a>';
			bottomHtml = bottomHtml + '</span>';
		}
	}
	$('#bottomNav').html(bottomHtml);
}

function buildBottomMenuHtml(itemData){
	var name = itemData.name;
	var itemType = itemData.type;
	var itemText = itemData.text;
	if(itemType == 'folder'){
		bottomHtml = bottomHtml + '<span>';
		for (var j=0;j < itemData.menus.length;j++){
			var submenuData = itemData.menus[j];
			buildBottomMenuHtml(submenuData);
			
			if (j==0){
				if (submenuData.type == 'page'){
					var itemURL = itemData.href;
					var itemTarget = itemData.target;
					bottomHtml = bottomHtml + '<a href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a>';
				}
				else if (submenuData.type == 'link'){
					var itemURL = itemData.href;
					var itemTarget = itemData.target;
					bottomHtml = bottomHtml + '<a hideFocus="true" href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a>';
				}
			}
		}
		bottomHtml = bottomHtml + '</span>'; 
	}
}

function autoScroll(obj){ 
	$(obj).find("ul:first").animate({ 
		marginTop:"-25px" 
	},500,function(){ 
		$(this).css({marginTop:"0px"}).find("li:first").appendTo(this); 
	}); 
}

$(function(){
	var url = '/portal/resource?ContentProvider&actionType=getLastestNews&columnCodes=corp-news,tech-trends,industry-news';
	sendAction(url,{dataType:'text',onComplete:function(responseText){
		if (responseText){
			//alert(responseText);
			var scrollData = $.parseJSON(responseText);
			var htmlFragment = "<ul>";
			for (var i=0;i < scrollData.datas.length;i++){
				var data = scrollData.datas[i];
				htmlFragment = htmlFragment + "<li><a href='/portal"+data.url+"' target='"+data.target+"'>"+data.title+"</a></li>";	
			}
			htmlFragment = htmlFragment + "</ul>";
			$("#scrollNewsDiv").html(htmlFragment);
			setInterval('autoScroll("#scrollNewsDiv")',6000); 
		}
	}});	
});
</script>