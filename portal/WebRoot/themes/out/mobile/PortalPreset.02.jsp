<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
String navCode = menuBar.getId();
String menuCode = (String)request.getAttribute("fromMenuCode");

String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
String modeText = "view".equals(windowMode)?"编辑组件":"展示信息";
String hostIp = request.getRemoteAddr();
if ("0:0:0:0:0:0:0:1".equals(hostIp)){
	hostIp = "localhost";
}
%>
<html>
<head>
<meta charset="utf-8">
<title>手机数通畅联</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.structure-1.4.5.min.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.inline-png-1.4.5.min.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.theme-1.4.5.min.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile-1.4.5.min.css" rel="stylesheet" type="text/css" />
<%if ("edit".equals(windowMode)){%>	
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.qtip.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
<style type="text/css">
body {
	font-size:1em;
}
div,td,span,th {
	font-size:1em;
}
</style>
<%}%>
<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=menuItem.getCustomCssURL()%>" type="text/css" />
<%}%>	
<script src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.min.js" type="text/javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>
<%if ("edit".equals(windowMode)){%>	
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.qtip.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/imagesloaded.pkgd.js" language="javascript"></script>
<%}%>
<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
<script type="text/javascript" src="<%=menuItem.getCustomJsURL()%>"></script>
<%}%>	
<script type="text/javascript">
<%if ("edit".equals(windowMode)){%>	
$.ajaxSetup({cache:false});
var __renderPortlets = new Map();	
var _directConfigPortletBox;
function directConfigDecoratorRequest(portletId){
	if (!_directConfigPortletBox){
		_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'380px'});
	}
	var url = '/portal/index?PagePortletConfig&portletId='+portletId;
	_directConfigPortletBox.sendRequest(url);	
}
$(function(){
	<%if ("edit".equals(windowMode)){%>
	$("#content_container > h3").hover(
	  function () {
	    $(this).find("span[id=stateAnchor]").fadeIn('fast');
	  }, 
	  function () {
	    $(this).find("span[id=stateAnchor]").fadeOut('fast');
	  }
	);
	<%}%>
	$("#content_container > h3").find("span[id=stateAnchor]").hide();
	PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
});
<%}%>
$(document).ready(function() { 
    jQuery.mobile.ajaxEnabled = false;   
}); 
function viewDetail(infomationId){
	window.location.href="/portal/website/<%=navCode%>/content.ptml/<%=menuCode%>/"+infomationId
}
function goTop(){
	 $("body,html").animate({scrollTop:0},300);
}
</script>
<style type="text/css">
.head-item{
	height:45px;
	line-height:45px;
}
.foot-item{
	height:46px;
	line-height:46px;
	font-size:14px;
}
.contentMain div,.contentMain p,.contentMain span{
	text-shadow:none;
}
.contentMain p a,.contentMain table tbody tr td a{
	text-shadow:none;
}
.ui-footer>div.ui-grid-a{
	background: #2c3e50;
	border-color: #2c3e50;
	text-shadow:none;
	color:#FFF;
}

#footer{background-color:#000;padding:10px 7px;text-shadow:none;color:#eee;font-size:13px;}
#footer span{color:#eee;height:20px;line-height:20px;}
#footer span a{text-decoration:none;color:#eee;font-weight:normal;}
#footer span a:visited,#footer span a:hover{text-decoration:none;color:#eee;font-weight:normal;}
#footer span img{vertical-align: middle;}
ul li#viewModeLi a{
	text-decoration: none;
}
</style>
</head>
<body>
<div data-role="page" id="page">
  <div data-role="header" style="background-color:#FFF" data-position="fixed" data-tap-toggle="false">   
  <h3></h3>
	<a href="/portal/website/<%=navCode%>/index.ptml" data-role="none"><img src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/images/logo.jpg" width="133" height="37"></a>
	<a href="/portal/website/<%=navCode%>/<%=menuCode%>.ptml" data-role="none"><img src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/images/back.png" style="margin-top:5px;margin-right:5px;"></a> 
    <div data-role="navbar">
        <ul>
            <li><a href="/portal/website/<%=navCode%>/index.ptml" class="ui-btn<%if ("index".equals(menuCode)){%> ui-state-persist ui-btn-active<%}%>">主页</a></li>
            <li><a href="/portal/website/<%=navCode%>/products.ptml" class="ui-btn<%if ("products".equals(menuCode)){%> ui-state-persist ui-btn-active<%}%>">产品</a></li>
            <li><a href="/portal/website/<%=navCode%>/solutions.ptml" class="ui-btn<%if ("solutions".equals(menuCode)){%> ui-state-persist ui-btn-active<%}%>">方案</a></li>
            <li><a href="/portal/website/<%=navCode%>/others.ptml" class="ui-btn<%if ("others".equals(menuCode)){%> ui-state-persist ui-btn-active<%}%>">其他</a></li>
        </ul>
    </div>       
  </div>
<div data-role="content" style="padding:1px 7px 1px 7px;background-color:#FFF">
	<pt:mlayout/>	   
</div>
<div id="footer" data-role="none">
	<span style="float:right;"><a href="javascript:goTop();"><img border="0" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/images/gotop.png"></a></span>
	<span style="float:left;"><script type="text/javascript">
		var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
		document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3Fb095c3e94483b44547a8f311f04871fd' 		type='text/javascript'%3E%3C/script%3E"));
		</script></span>
	<ul style="margin:0px;padding-left:20px;">
		<li style="margin:2px 10px;list-style:none;">版权所有© 沈阳数通畅联软件技术有限公司</li>
		<li style="margin:2px 10px;list-style:none;">辽ICP备14005586号</li>
		<li style="margin:2px 10px;list-style:none;">服务支持:service@agileai.com&nbsp;&nbsp;&nbsp;电话:024-22962011</li>
		<li style="margin:2px 10px;list-style:none;" id="viewModeLi">手机版&nbsp;|&nbsp;<a style="color: #FF0000; font-weight:lighter" href="/portal/website/01/index.ptml">电脑版</a></li>
    </ul>
</div>
</body>
</html>