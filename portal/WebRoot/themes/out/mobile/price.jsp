<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="utf-8">
<title>报价体系</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.structure-1.4.5.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.inline-png-1.4.5.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.theme-1.4.5.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile-1.4.5.min.css"
	rel="stylesheet" type="text/css" />
<script
	src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.min.js"
	type="text/javascript"></script>
<script
	src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile-1.4.5.min.js"
	type="text/javascript"></script>
<style type="text/css">
span,div{
	text-shadow:none;
	font-size:14px;
}
table {
	border-collapse: collapse;
	border: none;
	width:100%;
}
td {
	font-size: 12px;
	padding: 5px 3px;
	border: solid #000 1px;
	text-shadow:none;
}
th {
	font-size: 13px;
	padding: 5px;
	text-align: center;
	border: solid #000 1px;
	font-weight: normal;
	background-color: #09F;
	color: white;
	text-shadow:none;
}
ul {
	margin: 0px;
	padding: 2px;
}
ul li {
	list-style:none;
	padding: 2px 0px 2px 2px;
}
ol li{
	font-size: 13px;
}
div.table-container{
	padding:3px 5px 3px 5px;
}
span.topTitle{
	font-size: 18px; font-weight: bold;line-height: 40px;height:40px;
	padding-left: 3px;
}
span.subTitle{
	font-size: 16px; font-weight: bold;line-height: 32px;height:32px;
	padding-left: 3px;
}
span.remark{
	font-size:13px;font-weight:bolder;line-height:28px;height:28px;
	padding-left:5px;
	padding-top: 10px;
}
</style>
</head>
<body>
	<div data-role="page" id="page">
		<div>
			<span class="topTitle">1、概述</span>
		</div>
		<div>
			<span>&nbsp; &nbsp;
				&nbsp;本文是沈阳数通畅联软件技术有限公司提供的公开产品及服务报价说明，欢迎来电咨询024-22962011。</span>
		</div>
		<div>
			<span class="topTitle">2、产品报价</span>
		</div>
		<div class="table-container">
			<table cellspacing="0" cellpadding="0">
				<tr>
					<th width="15%" nowrap="nowrap" rowspan="2">产品</th>
					<th width="39%" colspan="2" align="left" nowrap="nowrap">企业版</th>
					<th width="39%" nowrap="nowrap" colspan="2">标准版</th>
				</tr>
				<tr>
					<th width="20%" align="left" nowrap="nowrap">价格</th>
					<th width="18%" nowrap="nowrap">赠服务</th>
					<th width="21%" nowrap="nowrap">价格</th>
					<th width="18%" nowrap="nowrap">赠服务</th>
				</tr>
				<tr>
					<td width="21%" nowrap="nowrap">AEAI ESB</td>
					<td width="20%" align="left" nowrap="nowrap">20万元</td>
					<td width="18%" nowrap="nowrap">5人天</td>
					<td width="21%" nowrap="nowrap">5万元</td>
					<td width="18%" nowrap="nowrap">3人天</td>
				</tr>
				<tr>
					<td width="21%" nowrap="nowrap">AEAI Portal</td>
					<td width="20%" align="left" nowrap="nowrap">20万元</td>
					<td width="18%" nowrap="nowrap">5人天</td>
					<td width="21%" nowrap="nowrap">5万元</td>
					<td width="18%" nowrap="nowrap">3人天</td>
				</tr>
				<tr>
					<td width="21%" nowrap="nowrap">AEAI DP</td>
					<td width="78%" colspan="4" align="center" nowrap="nowrap"><strong>免费且开源</strong><strong>
					</strong></td>
				</tr>
			</table>
		</div>
		<div>
			<span class="remark">注意：</span>
			<div>
			<ol>
				<li><span>上述报价属于标准报价，<strong
							style="color: red;">对于合作伙伴有大幅度优惠政策，参见2.2；</strong></span></li>
				<li><span>赠送的服务为现场技术服务，其中：交通费用由对方承担，其他费用由数通畅联承担。具体参见3服务报价部分。</span></li>
			</ol>
            </div>
		</div>
		<div>
			<span class="subTitle">2.1产品说明</span>
		</div>
		<div class="table-container">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<th width="80" rowspan="4" nowrap="nowrap">AEAI ESB<br />
						集成平台
					</th>
					<th width="50" nowrap="nowrap">企业版</th>
					<td width="90%">64位，不限操作系统，无消息流程数限制。
						</li>
					</td>
				</tr>
				<tr>
					<th width="50" nowrap="nowrap">标准版</th>
					<td>64位，不限操作系统，默认50个消息流程数；每增加10个消息流程支付1万。</td>
				</tr>
				<tr>
					<th width="50" nowrap="nowrap">社区版</th>
					<td>32位，不限操作系统，默认50个消息流程数据，只能通过localhost本机来访问。</td>
				</tr>
				<tr>
					<th width="50" nowrap="nowrap">开发版</th>
					<td>32位，不限操作系统，默认50个消息流程数据，任意IP来访问，内部使用。</td>
				</tr>
			</table>
		</div>
		<br />
		<div class="table-container">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<th width="80" rowspan="4" nowrap="nowrap">AEAI Portal<br />
						门户平台
					</th>
					<th width="50" nowrap="nowrap">企业版</th>
					<td width="90%">64位，不限操作系统，无用户数限制。
						</li>
					</td>
				</tr>
				<tr>
					<th width="50" nowrap="nowrap">标准版</th>
					<td>64位，不限操作系统，默认100个用户数；每增加10个用户数支付1万。</td>
				</tr>
				<tr>
					<th width="50" nowrap="nowrap">社区版</th>
					<td>32位，不限操作系统，默认10个用户数，只能通过localhost本机来访问。</td>
				</tr>
				<tr>
					<th width="50" nowrap="nowrap">开发版</th>
					<td>32位，不限操作系统，默认10个用户数，任意IP来访问，内部使用。</td>
				</tr>
			</table>
		</div>
		<br />
		<div>
			<span  class="subTitle">2.2伙伴报价</span>
		</div>
		<div>
			<span>&nbsp; &nbsp;
				&nbsp;伙伴协议默认一年签一次，伙伴协议仅对合作方式以及优惠方案进行常规说明，用于数通畅联的合作伙伴记录备案。AEAI
				ESB和AEAI Portal报价方式一致，具体如下：</span>
		</div>
        <div class="table-container">
		<table border="1" cellpadding="0" cellspacing="0">
			<tr>
				<th width="50" rowspan="2" nowrap="nowrap">标准版</th>
				<th width="80" nowrap="nowrap">单次购买</th>
				<td width="90%"><ul>
						<li>第一次，合3万一套</li>
						<li>第二次及以上合2.5万一套，在第一次基础上支付2万，总计5万</li>
					</ul></td>
			</tr>
			<tr>
				<th nowrap="nowrap">批量购买</th>
				<td><ul>
						<li>两套按2.5万每套计，两套合计5万</li>
						<li>三套及以上按2万每套计，三套总计6万</li>
					</ul></td>
			</tr>
			<tr>
				<th rowspan="2" nowrap="nowrap">企业版</th>
				<th nowrap="nowrap">单次购买</th>
				<td><ul>
						<li>第一次，合12万一套</li>
						<li>第二次，合10万一套，在上次基础上支付8万即可；两套总计20万</li>
						<li>第三次，合8万一套，在上次基础上支付4万；三套总计24万</li>
						<li>第四次以及以上，合7万一套，在第三套基础上支付4万即可，四套总计28万</li>
					</ul></td>
			</tr>
			<tr>
				<th nowrap="nowrap">批量购买</th>
				<td>
					<ul>
						<li>两套按8万一套，总计16万</li>
						<li>三套按6万一套，总计18万</li>
						<li>四套及以上，按5万一套，四套总计20万</li>
					</ul>
				</td>
			</tr>
		</table>
        </div>
		<span class="remark">注意：如果由标准版升级为企业版，只需补差价即可。
		</span>
		<div><span class="topTitle">3、服务报价</span>
		</div>
		<div>
			<span class="subTitle">3.1现场技术服务</span>
            <div class="table-container">
			<table cellspacing="0" cellpadding="0">
				<tr>
					<th nowrap="nowrap">地域</th>
					<th nowrap="nowrap">高级服务</th>
					<th nowrap="nowrap">标准服务</th>
				</tr>
				<tr>
					<td nowrap="nowrap">北上广深</td>
					<td nowrap="nowrap">3000元/人天</td>
					<td nowrap="nowrap">2000元/人天</td>
				</tr>
				<tr>
					<td nowrap="nowrap">沈阳以外</td>
					<td nowrap="nowrap">2500元/人天</td>
					<td nowrap="nowrap">1500元/人天</td>
				</tr>
				<tr>
					<td nowrap="nowrap">沈阳本地</td>
					<td nowrap="nowrap">2000元/人天</td>
					<td nowrap="nowrap">1000元/人天</td>
				</tr>
			</table>
            </div>
		</div>
		<div>
			<span class="remark">说明：</span>
		</div>
		<div>
			<ol>
				<li><span>高级服务，一般指高级培训、需求分析、架构设计，项目管理，由高级技术人员担任；</span></li>
				<li><span>标准服务，一般指原型配置、集成开发、软件测试、基本培训，由普通技术人员担任；</span></li>
				<li><span>现场服务涉及交通费由对方承担，住宿费以及其他费由数通畅联承担；</span></li>
			</ol>
		</div>
		<div>
			<span class="remark" style="color: red;">注：如果由数通畅联承接项目，现场服务报价根据项目面议。</span>
		</div>
		<div>
			<span class="subTitle">3.2远程技术支持</span>
            <div class="table-container">
			<table cellspacing="0" cellpadding="0">
				<tr>
					<th nowrap="nowrap">按年度收费</th>
					<th nowrap="nowrap">按季度收费</th>
					<th nowrap="nowrap">按月度收费</th>
				</tr>
				<tr>
					<td nowrap="nowrap">20000元/年</td>
					<td nowrap="nowrap">10000元/季度</td>
					<td>5000元/月</td>
				</tr>
			</table>
            </div>
		</div>
		<br />
	</div>
	</div>
	</div>
	</div>
</body>
</html>
