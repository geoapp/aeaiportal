<!DOCTYPE HTML>
<html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
String menuBarJson = menuBar.getMenuBarJson(null,menuItem.getCode(),request);
String currentVisitPath = menuBar.getCurrentPath(request,menuItem);

String windowState= (String)request.getAttribute("WINDOW_STATE_KEY");
String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
String modeText = "view".equals(windowMode)?"编辑组件":"展示信息";
String hostIp = request.getRemoteAddr();
if ("0:0:0:0:0:0:0:1".equals(hostIp)){
	hostIp = "localhost";
}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>数通畅联软件</title>
<!--[if lt IE 9]><script type="text/javascript" src="scripts/modernizr.js"></script><![endif]-->
<meta name="keywords" content="<%=menuItem.getKeywords()%>" />
<meta name="description" content="<%=menuItem.getDescription()%>" />

<link rel="stylesheet" type="text/css" href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/floatnav/css/style.css" media="screen" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/layout.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/wcm/display.css" type="text/css" />
<%if ("edit".equals(windowMode)){%>	
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
	<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.qtip.css" type="text/css" />
<%}%>
<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=menuItem.getCustomCssURL()%>" type="text/css" />
<%}%>	
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery1.7.1.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>    
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>
<%if ("edit".equals(windowMode)){%>	
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.qtip.js" language="javascript"></script>
	<script src="<c:out value="${pageContext.request.contextPath}"/>/js/imagesloaded.pkgd.js" language="javascript"></script>
<%}%>
<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
	<script type="text/javascript" src="<%=menuItem.getCustomJsURL()%>"></script>
<%}%>	
	<script type="text/javascript">
	$.ajaxSetup({cache:false});
	
	var __renderPortlets = new Map();	
	var _directConfigPortletBox;
	function directConfigDecoratorRequest(portletId){
		if (!_directConfigPortletBox){
			_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'380px'});
		}
		var url = '/portal/index?PagePortletConfig&portletId='+portletId;
		_directConfigPortletBox.sendRequest(url);	
	}
	$(function(){
		<%if ("edit".equals(windowMode)){%>
		$(".content > h3").hover(
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeIn('fast');
		  }, 
		  function () {
		    $(this).find("span[id=stateAnchor]").fadeOut('fast');
		  }
		);
		<%}%>
		$(".content > h3").find("span[id=stateAnchor]").hide();
		PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
	});
</script>
<style type="text/css">
html,body{ 
margin:0px; 
height:auto; 
} 
</style>
</head>
<body>

<div class="wrapper">
  <header>
    <h1>AEAI Portal</h1>
  </header>
  <div class="nav-container">
    <nav>
      <ul id="nav"></ul>
      <div class="nav-left"></div>
      <div class="nav-right"><span style="float:left;margin-top:15px;"> 
    <a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=366d7f20977a19335ecbf578959f3fbd607436af9b18fea7bd3e4f9f0710d040"><img border="0" src="http://pub.idqqimg.com/wpa/images/group.png" alt="数通畅联" title="数通畅联"></a>
    </span></div>
      <div class="nav-above"></div>
    </nav>
  </div>
  <section id="home">
     <div id="navigater"><span id="now_place">当前位置：<%=currentVisitPath%></span></div></br>
  </section>
  <div class="copyrights"></div>
  <section id="contact">
   <pt:layout/>
  </section>
</div>
<div id="footer" style="background:#3C3C3C; width:960px;"> 
   <div style="margin-top:5px;padding:10px 0px 20px 0px;font-size:13px;">
		<span>版权所有© 沈阳数通畅联软件技术有限公司</span>
		 
<span style="margin-left:20px; float:right;"><a style="color: #aaa;" href="http://www.miitbeian.gov.cn/"> 辽ICP备14005586号</a></span>
		</div>
  </div>
<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/floatnav/js/jquery.orbit-1.2.3.min.js"></script> 
<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/floatnav/js/jquery.tweet.js"></script> 
<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/floatnav/js/jflickrfeed.js"></script> 
<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/floatnav/js/jquery.scrollTo-1.4.2-min.js"></script> 
<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/floatnav/js/waypoints.min.js"></script> 
<script type="text/javascript" src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/floatnav/js/navbar.js"></script> 

</body>
</html>
<script type="text/javascript">
var menudata = <%=menuBarJson%>;
var topMenuSelected;
var topMenuSelectedUrl;
var subMenuSelected;
var topMenuSelectedId;
var menuHtml = "";
var bottomHtml = "";

function createMenu(){
	var data = menudata.menus;
	for(var i = 0; i < data.length; i++) {
		var itemData = data[i];
		if (itemData.selected){
			topMenuSelected = itemData.text;
			topMenuSelectedUrl = itemData.href;
			topMenuSelectedId = itemData.name;
		}
		buildMenuHtml(itemData);
	}
	$('#nav').html(menuHtml);
	$('#bottomNav').html(bottomHtml);
}

function buildMenuHtml(itemData){
	var name = itemData.name;
	var itemType = itemData.type;
	var itemText = itemData.text;
	var submenuHtml = "";
	if(itemType == 'folder'){
		menuHtml = menuHtml + '<li><a href="javascript:void(0)">'+itemText+'</a><ul>';
		bottomHtml = bottomHtml + '<span>';
		for (var j=0;j < itemData.menus.length;j++){
			var submenuData = itemData.menus[j];
			buildMenuHtml(submenuData);
			
			if (j==0){
				if (submenuData.type == 'page'){
					var itemURL = itemData.href;
					var itemTarget = itemData.target;
					bottomHtml = bottomHtml + '<a href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a>';
				}
				else if (submenuData.type == 'link'){
					var itemURL = itemData.href;
					var itemTarget = itemData.target;
					bottomHtml = bottomHtml + '<a hideFocus="true" href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a>';
				}
			}
		}
		menuHtml = menuHtml + '</ul></li>'; 
		bottomHtml = bottomHtml + '</span>'; 
	}
	else if (itemType == 'page'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li><a href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
	else if (itemType == 'link'){
		var itemURL = itemData.href;
		var itemTarget = itemData.target;
		menuHtml = menuHtml + '<li><a hideFocus="true" href="'+itemURL+'" target="'+itemTarget+'">'+itemText+'</a></li>';
	}
}
$(document).ready(function($){
	createMenu();
	/*
	$('.mega-menu').dcMegaMenu({
		rowItems: '3',
		speed: 'fast',
		effect: 'fade'
	});
	*/
});
</script>