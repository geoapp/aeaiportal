<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveWidth(){
	var totalWidth = 0;
	for (var i=0;i < <%=pageBean.getStringValue("childSize")%>;i++){
		var curWidth = $('#childWidth'+i).val();
		if (validation.checkNull(curWidth)){
			alert('列宽度不能为空！');
			ele('childWidth'+i).focus();
			return;
		}
		if (!validation.isPositiveInt(curWidth)){
			alert('列宽度必须为整数！');
			ele('childWidth'+i).focus();
			return;
		}
		totalWidth = totalWidth + parseInt(curWidth);
	}
	if (totalWidth != 100){
		alert('列容器宽度之和应为100 !');
		return;
	}
	doSubmit({actionType:'setupWidth',target:'_parent'})
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveWidth()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<%
if ("C".equals(pageBean.getStringValue("containerType"))){
%>
<tr>
	<th width="100" nowrap>宽度比</th>
	<td width="80%"><%=pageBean.getStringValue("widthHtml")%></td>
</tr>
<%}%>
<tr>
	<th width="100" nowrap>Style样式</th>
	<td width="80%"><textarea name="containerStyle" cols="40" rows="3" class="text" id="containerStyle"><%=pageBean.inputValue("containerStyle") %></textarea></td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" id="PAGE_ID" name="PAGE_ID" value="<%=pageBean.inputValue("PAGE_ID")%>" />
<input type="hidden" id="containerId" name="containerId" value="<%=pageBean.inputValue("containerId")%>" />
<input type="hidden" id="containerType" name="containerType" value="<%=pageBean.inputValue("containerType")%>" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>