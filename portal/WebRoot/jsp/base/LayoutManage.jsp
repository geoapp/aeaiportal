﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>页面管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link rel="stylesheet" type="text/css" href="css/layout.css" />
<style>
.draggable {
	height:25px;
	line-height:25px;
	padding:1px; float: left; margin:0px;background-color:#9CC;
	padding-left:14px;
    background-image: url("images/layout/objectdrag.gif");
    background-position: left center;
    background-position-x:1px;
    background-repeat: no-repeat;
}
.portletArea {width:100%; height:30px; margin: 5px 0px; background-color:#9CC;clear:both;}
.droppable {width:100%;min-height:30px;}
.ui-state-active{
	background:#0F0;
}	
</style>
<script src="js/jquery.ui.core.js" language="javascript"></script>
<script src="js/jquery.ui.widget.js" language="javascript"></script>
<script src="js/jquery.ui.mouse.js" language="javascript"></script>
<script src="js/jquery.ui.draggable.js" language="javascript"></script>
<script src="js/jquery.ui.droppable.js" language="javascript"></script>
<script language="javascript">
var addChildBox;
function addChildContainerRequest(containerId){
	$('#containerId').val(containerId);
	if (!addChildBox){
		addChildBox = new PopupBox('addChildBox','添加子容器',{size:'small',width:'300px',height:'200px',top:'10px'});
	}
	var url = 'index?ChildContainerAdd&containerId='+containerId;
	addChildBox.sendRequest(url);
}
function addChildContainer(childNumber){
	$('#childNumber').val(childNumber);	
	doSubmit({actionType:'addChildContainer'});
}

var widthBox;
function setupWidthRequest(containerId){
	if (!widthBox){
		widthBox = new PopupBox('widthBox','容器属性设置',{size:'normal',height:'260px',top:'10px'});
	}
	var pageId = $('#PAGE_ID').val();
	var url = 'index?ContainerWidthSetup&containerId='+containerId+'&PAGE_ID='+pageId;
	widthBox.sendRequest(url);	
}

var splitBox;
function splitContainerRequest(containerId){
	$('#containerId').val(containerId);
	if (!splitBox){
		splitBox = new PopupBox('splitBox','拆分容器',{size:'small',height:'200px',top:'10px'});
	}
	var url = 'index?ContainerSplit&containerId='+containerId;
	splitBox.sendRequest(url);	
}
function splitContainer(splitNumber){
	$('#splitNumber').val(splitNumber);
	doSubmit({actionType:'splitContainer'});
}

function deleteContainer(containerId){
	if (confirm('确认要删除该容器吗？')){
		$('#containerId').val(containerId);
		doSubmit({actionType:'deleteContainer'});
	}
}

var addPortletBox;
function addPortletRequest(containerId){
	$('#containerId').val(containerId);
	if (!addPortletBox){
		addPortletBox = new PopupBox('addPortletBox','添加Portlet',{size:'big',height:'420px',top:'10px'});
	}
	var url = 'index?PortletAppPickSelectList&containerId='+containerId;
	addPortletBox.sendRequest(url);
}

function addPortlet(portletAppId,portletAppName,portletAppTitle){
	$('#portletAppId').val(portletAppId);
	$('#portletAppName').val(portletAppName);
	$('#portletAppTitle').val(portletAppTitle);
	doSubmit({actionType:'addPortlet'});
}

var configPortletBox;
function configPortletRequest(portletId,containerId){
	$('#portletId').val(portletId);
	$('#containerId').val(containerId);	
	if (!configPortletBox){
		configPortletBox = new PopupBox('configPortletBox','配置Portlet',{size:'normal',height:'420px',top:'2px'});
	}
	var url = 'index?PagePortletConfig&containerId='+containerId+'&portletId='+portletId;
	configPortletBox.sendRequest(url);	
}

var configSecurityBox;
function configSecurityRequest(portletId){
	if (!configSecurityBox){
		configSecurityBox = new PopupBox('configSecurityBox','安全配置',{size:'normal',width:'700px',height:'450px',top:'2px'});
	}
	var url = 'index?SecurityAuthorizationConfig&resourceType=Portlet&resourceId='+portletId;
	configSecurityBox.sendRequest(url);	
}

var copyLayoutBox;
function copyLayoutBoxRequest(navgaterId,currentPageId){
	if (!copyLayoutBox){
		copyLayoutBox = new PopupBox('copyLayoutBox','选择复制参考页面',{size:'normal',height:'430px',top:'2px'});
	}
	var url = 'index?PagePickSelectList&navgaterId='+navgaterId+'&currentPageId='+currentPageId;
	copyLayoutBox.sendRequest(url);	
}
function copyPage(selectPageId){
	var url = "<%=pageBean.getHandlerURL()%>&actionType=copyPage&selectPageId="+selectPageId+"&currentPageId=<%=pageBean.inputValue("PAGE_ID")%>"
	sendRequest(url,{onComplete:function(responseText){
		alert(responseText);
		doSubmit({actionType:'cancel'})
	}});	
}
function moveUp(portletId,containerId){
	$('#portletId').val(portletId);
	$('#containerId').val(containerId);
	doSubmit({actionType:'moveUp'});
}
function moveDown(portletId,containerId){
	$('#portletId').val(portletId);
	$('#containerId').val(containerId);
	doSubmit({actionType:'moveDown'});
}
function delPortlet(portletId,containerId){
	if (confirm('确认要删除该Portlet？')){		
		$('#portletId').val(portletId);
		$('#containerId').val(containerId);
		doSubmit({actionType:'delPortlet'});
	}
}
function saveConfig(){
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		alert(responseText);
	}});
}
var langConfBox;
function configLanguage(resourceId,resourceName){
	if (!langConfBox){
		langConfBox = new PopupBox('langConfBox','多语言配置',{size:'normal',width:'500px',height:'300px',top:'30px',scroll:'auto'});
	}
	var url = 'index?SysMultiLangManageEdit&resourceType=Portlet&resourceId='+resourceId +'&resourceName='+resourceName;
	langConfBox.sendRequest(url);
}
</script>
</head>
<body style="margin:0px;">
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveConfig();"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'cancel'})"><input value="&nbsp;" type="button" class="cancelImgBtn" id="cancelImgBtn" title="刷新" />刷新</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="copyLayoutBoxRequest('<%=pageBean.inputValue("NAV_ID")%>','<%=pageBean.inputValue("PAGE_ID")%>')"><input value="&nbsp;" type="button" class="copyImgBtn" id="copyImgBtn" title="参考布局" />参考布局</td>      
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="parent.doSubmit({actionType:'back'});"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<%=pageBean.getStringValue("CURRENT_LAYOUT_HTML")%>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" id="PAGE_ID" name="PAGE_ID" value="<%=pageBean.inputValue("PAGE_ID")%>" />
<input type="hidden" id="containerId" name="containerId" value="" />
<input type="hidden" id="portletId" name="portletId" value="" />
<input type="hidden" id="portletAppId" name="portletAppId" value="" />
<input type="hidden" id="portletAppName" name="portletAppName" value="" />
<input type="hidden" id="portletAppTitle" name="portletAppTitle" value="" />
<input type="hidden" id="childNumber" name="childNumber" value="" />
<input type="hidden" id="splitNumber" name="splitNumber" value="" />

<input type="hidden" id="sourceContainerId" name="sourceContainerId" value="" />
<input type="hidden" id="targetContainerId" name="targetContainerId" value="" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
<script>
$(function() {
	$(".draggable").draggable({revert:true});
	$(".droppable").droppable({hoverClass:"ui-state-active",
	accept:".draggable",
	drop:function(event, ui){
		var containerId = $(this).attr('id');
		var portletId = ui.draggable.attr('id');
		var portletContainerId = ui.draggable.attr('containerId'); 
		if (containerId == portletContainerId){
			return;
		}else{
			$('#portletId').val(portletId);
			$('#sourceContainerId').val(portletContainerId);
			$('#targetContainerId').val(containerId);			
			doSubmit({actionType:'movePortlet'});
		}
	}}); 
});
</script>