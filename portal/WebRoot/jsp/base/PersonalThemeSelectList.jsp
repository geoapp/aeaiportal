<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>待选主题列表</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function doSelectRequest(){
	if (!isValid(ele('themeId').value)){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if ("Y" == "<%=pageBean.inputValue("comeFromNavEdit")%>"){
		var url = "<%=pageBean.getHandlerURL()%>&actionType=selectTheme&comeFromNavEdit=Y&navId=<%=pageBean.inputTime("navId")%>&themeId="+$("#themeId").val();
		sendRequest(url,{onComplete:function(responseText){
			if("success" == responseText){
				parent.loadPersonalThemes();
				parent.PopupBox.closeCurrent();
			}else{
				writeErrorMsg('选择主题出错,请检查!');
			}
		}});		
	}else{
		var url = "<%=pageBean.getHandlerURL()%>&actionType=selectTheme&navId=<%=pageBean.inputTime("navId")%>&themeId="+$("#themeId").val();
		sendRequest(url,{onComplete:function(responseText){
			if("success" == responseText){
				parent.resetWindow();
			}else{
				writeErrorMsg('应用主题出错,请检查!');
			}
		}});		
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">
</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doSelectRequest()"><input value="&nbsp;" type="button" class="saveImgBtn" title="选择" />选择</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="B" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>        
</tr>
</table>
</div>
<table border="0" cellpadding="0" cellspacing="0" class="dataTable ecSide" id="dataTable">
<thead>
  <tr>
    <th width="80" align="center" nowrap="nowrap">序号</th>
    <th width="40%" align="center">名称</th>
    <th width="40%" align="center">路径</th>
  </tr>
</thead>
<tbody>
<%
int paramSize = pageBean.listSize();
for (int i=0;i < paramSize;i++){
%>
  <tr onmouseout="ECSideUtil.unlightRow(this);" onmouseover="ECSideUtil.lightRow(this);" onclick="ECSideUtil.selectRow(this,'form1');selectRow(this,{themeId:'<%=pageBean.inputValue(i,"THEME_ID")%>'})" ondblclick="clearSelection();doSelectRequest()">
	<td style="text-align:center"><%=i+1%></td>
	<td><%=pageBean.inputValue(i,"THEME_NAME")%></td>
	<td><%=pageBean.inputValue(i,"THEME_PATH")%></td>
  </tr>
<%}%>
</tbody>  
</table>
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="themeId" id="themeId" value="" />
<input type="hidden" name="navId" id="navId" value="<%=pageBean.inputTime("navId")%>" />
<input type="hidden" name="comeFromNavEdit" id="comeFromNavEdit" value="<%=pageBean.inputValue("comeFromNavEdit")%>" />
<script language="JavaScript">
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
