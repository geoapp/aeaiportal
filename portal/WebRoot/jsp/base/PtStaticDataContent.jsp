<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>静态数据内容</title>
<script src="js/jquery1.7.1.js" language="javascript"></script>
<script src="js/util.js" language="javascript"></script>
<style type="text/css" media="screen">
body {
    overflow: hidden;
}
#editor { 
    margin: 0;
    position: absolute;
    top:0;
    bottom: 0;
    left: 0;
    right: 0;
}
</style>
<script src="aceditor/ace.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
function closeBox(){
	parent.qBox.Close();
}
function saveContent(){
	showSplash();
	$("#actionType").val("saveContent");
	$("#DATA_CONTENT").val(editor.getSession().getValue());
	postRequest('form1',{onComplete:function(responseText){
		if (responseText=="success"){
			hideSplash();
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<pre id="editor">
</pre>
<input type="hidden" name="DATA_ID" id="DATA_ID" value="<%=pageBean.inputValue("DATA_ID")%>" />
<input type="hidden" name="DATA_CONTENT" id="DATA_CONTENT" value="" />
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
</body>
</html>
<script type="text/javascript">
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/textmate");
    //editor.setTheme("ace/theme/twilight");
    editor.setKeyboardHandler("ace/keyboard/ace");
    editor.getSession().setMode("ace/mode/<%=pageBean.inputValue("DATA_TYPE")%>");
    editor.getSession().setNewLineMode("windows");
    editor.getSession().setUseWorker(true);
    editor.session.setUseSoftTabs(true);
    editor.setWrapBehavioursEnabled(false);
    editor.setHighlightSelectedWord(true);
	editor.commands.addCommand({
	    name: 'save',
	    bindKey: {win: 'Ctrl-S',  mac: 'Command-S'},
	    exec: function(editor) {
	    	saveContent();
	    },
	    readOnly: true
	});    
	
	var url = "<%=pageBean.getHandlerURL()%>&actionType=retrieveContent&contentId=<%=pageBean.inputValue("DATA_ID")%>";
	sendRequest(url,{onComplete:function(responseText){
		editor.setValue(responseText);
	}});
</script>