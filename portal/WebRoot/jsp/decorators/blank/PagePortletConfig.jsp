<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="com.agileai.portal.driver.util.DecoratorUtil" %>
<%@ page language="java" import="com.agileai.hotweb.domain.FormSelect" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Portlet外观设置</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveConfig(){
	var title = $('#title').val();
	var height = $('#height').val();
	
	if (validation.checkNull(title)){
		alert('Porlet标题不能为空！');
		return;
	}
	if (validation.checkNull(height)){
		alert('Porlet高度不能为空！');
		return;
	}
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if ("OK"==responseText){
			parent.window.location.reload();
//			parent.PopupBox.closeCurrent();
		}else{
			alert(responseText);
			parent.PopupBox.closeCurrent();
		}
	}});
}

function changeDecorator(){
	doSubmit({actionType:'prepareDisplay'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveConfig()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>标题</th>
	<td width="80%"><input id="title" name="title" type="text" value="<%=pageBean.inputValue("title") %>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>Portlet名称</th>
	<td width="80%"><%=pageBean.inputValue("portletResName") %></td>
</tr>
<tr>
	<th width="100" nowrap>装饰器</th>
	<td width="80%"><select style="width: 180px;" onchange="changeDecorator()" id="portletDecorator" label="装饰器" name="portletDecorator" class="select">
	<%=pageBean.selectValue("portletDecorator")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>高度</th>
	<td width="80%"><input id="height" name="height" type="text" value="<%=pageBean.inputValue("height") %>" size="32" class="text" />（默认值为：auto）
</td>
</tr>
<tr>
	<th width="100" nowrap>是否显示</th>
	<td width="80%"><%=pageBean.selectRadio("portletVisiable")%></td>
</tr>
<tr>
	<th width="100" nowrap>主体样式</th>
	<td width="80%"><textarea name="portletBodyStyle" cols="52" rows="2" class="text" id="portletBodyStyle"><%=pageBean.inputValue("portletBodyStyle") %></textarea></td>
</tr>
</table>
<input type="hidden" id="containerId" name="containerId" value="<%=pageBean.inputValue("containerId")%>" />
<input type="hidden" id="portletId" name="portletId" value="<%=pageBean.inputValue("portletId")%>" />
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
