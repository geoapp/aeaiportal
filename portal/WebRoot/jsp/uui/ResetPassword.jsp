<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>数通畅联-设置密码</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function resetPassword(){
	if (!validate()){
		return;
	}
	var pwd = $("#USER_PWD").val()
	var regExpression = /^[a-zA-Z]\w{5,17}$/;
	if (!regExpression.test(pwd)){
		writeErrorMsg("密码必须以字母开头，长度6~18之间，只能为字符、数字和下划线！");
		return;
	}
	if ($("#USER_PWD").val() != $("#USER_PWD1").val()){
		writeErrorMsg("两次密码不一致，请确认！");
		return;		
	}
	doSubmit({actionType:'resetPassword'});
}
</script>
</head>
<body>
<div style="margin:auto;width:690px;">
<br />
<br /><br /><br />
<%if (pageBean.getAttribute("message") != null){%>
<span style="font-size: 16px;color:red;">
<%=pageBean.getStringValue("message") %>
</span>
<%}else{%>

<%if ("success".equals(pageBean.getStringValue("result"))){
	String userId = pageBean.inputValue("userId");
	String activeRedirectURL = pageBean.inputValue("activeRedirectURL");
%>
<span style="font-size: 16px;">
用户<%=userId%>重置密码成功！请点击<a href="<%=activeRedirectURL%>">这里</a>登录！
</span>
<%}else{%>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="80" nowrap>用户名</th>
	<td><input name="USER_CODE" type="text" class="text" id="USER_CODE" readonly="readonly" value="<%=pageBean.inputValue("userId")%>" size="30" maxlength="15" label="用户名" />英文或者数字</td>
</tr>
<tr>
	<th width="80" nowrap>密码</th>
	<td><input id="USER_PWD" label="密码" name="USER_PWD" type="password" value="" size="30" maxlength="32" class="text" />以字母开头，长度6~18之间，只能为字符、数字和下划线</td>
</tr>
<tr>
	<th width="80" nowrap>密码确认</th>
	<td><input id="USER_PWD1" label="密码确认" name="USER_PWD1" type="password" value="" size="30" maxlength="32" class="text" />再输入一遍密码,以保证密码无误</td>
</tr>
</table>
<div><input id="regButton" type="button" name="button" value="修改密码" style="height: 30px;width: 100px;font-size: 15px;font-weight: bolder;margin-top: 10px;" onclick="resetPassword()" /></div>
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
<script language="javascript">
requiredValidator.add("USER_CODE");
requiredValidator.add("USER_PWD");
requiredValidator.add("USER_PWD1");
charNumValidator.add("USER_CODE");
charNumValidator.add("USER_PWD");
charNumValidator.add("USER_PWD1");

new BlankTrimer("USER_CODE");
new BlankTrimer("USER_PWD");
new BlankTrimer("USER_PWD1");

$("#USER_CODE").focus();
</script>
<%}%>

<%}%>
</div>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
