<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>多语言功能</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<style>
.detailTable td input{
width:96%;
}
</style>
<script language="javascript">
function saveLangConf(){
	var recordSize = $('#recordSize').val();
	for ( var i = 0; i < recordSize; i++) {
		if(!isValueValid('MULTI_LANG_RESOURCE_TYPE_'+i) || !isValueValid('MULTI_LANG_RESOURCE_ID_'+i)){
			writeErrorMsg("非法操作，资源类型和资源ID不能为空，请联系管理员! ");
			return;
		}
	}
	showSplash();
	postRequest('form1', {
		actionType : 'save',
		onComplete : function(responseText) {
			hideSplash();
		}
	});
}
function closeCurrentBox(){
	if(parent.PopupBox){
		parent.PopupBox.closeCurrent();
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__" style="overflow:auto">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveLangConf();"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgButton" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="closeCurrentBox();"><input value="&nbsp;" type="button" class="closeImgBtn" id="closeImgButton" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<%
List list = (List)pageBean.getAttribute("desList");
pageBean.setRsList(list);
int iRecordSize = list.size();
for(int i=0; i < iRecordSize; i++){
%>
<tr>
	<td style="text-align:center;display: none;">
		<input type="hidden" id="MULTI_LANG_ID_<%=i%>" label="MULTI_LANG_ID" name="MULTI_LANG_ID_<%=i%>" value="<%=pageBean.inputValue(i,"MULTI_LANG_ID")%>" />
		<input type="hidden" id="MULTI_LANG_CODE_<%=i%>" label="MULTI_LANG_CODE" name="MULTI_LANG_CODE_<%=i%>" value="<%=pageBean.inputValue(i,"MULTI_LANG_CODE")%>" />
		<input type="hidden" name="MULTI_LANG_RESOURCE_TYPE_<%=i%>" id="MULTI_LANG_RESOURCE_TYPE_<%=i%>" value="<%=pageBean.inputValue(i,"MULTI_LANG_RESOURCE_TYPE")%>"/>
		<input type="hidden" name="MULTI_LANG_RESOURCE_ID_<%=i%>" id="MULTI_LANG_RESOURCE_ID_<%=i%>" value="<%=pageBean.inputValue(i,"MULTI_LANG_RESOURCE_ID")%>"/>
		<input id="state_<%=i%>" name="state_<%=i%>" type="hidden" value="<%=pageBean.inputValue(i,"_state")%>" />
	</td>
	<th><%=pageBean.inputValue(i, "MULTI_LANG_NAME")%></th>
	<td><input id="MULTI_LANG_VALUE_<%=i%>" label="语言值" name="MULTI_LANG_VALUE_<%=i%>" type="text" value="<%=pageBean.inputValue(i, "MULTI_LANG_VALUE")%>" class="text" /></td>
</tr>
<%
}
%>
</table>
<input type="hidden" name="recordSize" id="recordSize" value="<%=iRecordSize%>"/>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="resourceType" id="resourceType" value="<%=pageBean.inputValue("resourceType")%>"/>
<input type="hidden" name="resourceId" id="resourceId" value="<%=pageBean.inputValue("resourceId")%>"/>
</form>
<script language="javascript">
$(function(){
	$("#MULTI_LANG_VALUE_0").val('<%=pageBean.inputValue("resourceName")%>')
});
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
