<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://ckeditor.com" prefix="ck" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<% 
String path = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>信息编辑</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript" src="js/jquery.zclip.js"></script>
<script type="text/javascript" src="<%=path%>/ckeditor/ckeditor.js"></script>
</head>
<body style="margin:0;padding:0;">
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div ondblclick="javascript:parent.qBox.Close();"><a id="copyInfoId" style="margin-left: 5px;">复制信息ID</a></div>
<textarea name="content" id="content" cols="120" rows="5">
<%=pageBean.inputValue("content")%>
</textarea>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="editMode" id="editMode" value="<%=pageBean.inputValue("editMode")%>" />
<input type="hidden" name="userCode" id="userCode" value="<%=pageBean.inputValue("userCode")%>" />
<input type="hidden" name="columnId" id="columnId" value="<%=pageBean.inputValue("columnId")%>"/>
<input type="hidden" name="infomationId" id="infomationId" value="<%=pageBean.inputValue("infomationId")%>"/>
</form>
</body>
</html>
<script language="javascript">
CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	/* 
	 config.filebrowserBrowseUrl = '/portal/ckfinder/ckfinder.html';  
	 config.filebrowserImageBrowseUrl = '/portal/ckfinder/ckfinder.html?type=Images';  
	 config.filebrowserFlashBrowseUrl = '/portal/ckfinder/ckfinder.html?type=Flash';  
	 config.filebrowserUploadUrl = '/portal/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files'; 
	 config.filebrowserImageUploadUrl = '/portal/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images'; 
	 config.filebrowserFlashUploadUrl = '/portal/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Flash' ;
	 */
	 /*	 
	 config.filebrowserBrowseUrl = '/portal/ckfinder/ckfinder.html';
	 config.filebrowserFlashBrowseUrl = '/portal/ckfinder/ckfinder.html?type=Flash';
	 config.filebrowserFlashUploadUrl = '/portal/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Flash' ;
	 */
	 config.language = "zh-cn";
	 config.image_previewText=' '; //预览区域显示内容
	 config.skin = 'kama';//默认皮肤
	 //config.skin = 'v2';
	 //config.skin = 'office2003';
	 
	 //config.extraPlugins += (config.extraPlugins ? ',lineheight,cmprofile,cmsave,comment' : 'lineheight,cmprofile,cmsave,comment');
	 config.extraPlugins += (config.extraPlugins ? ',lineheight,cmprofile,cmsave' : 'lineheight,cmprofile,cmsave');
	 
	 config.toolbar = 'Full';
	 config.height = 440;
	 config.scayt_autoStartup = false;  //去掉自动检查
	 
	 config.enterMode=CKEDITOR.ENTER_BR;       //屏蔽换行符<br>
	 config.shiftEnterMode=CKEDITOR.ENTER_P;   //屏蔽段落<p>	 
	 config.protectedSource.push(/<pre[\s\S]*?pre>/g);
<%
if ( ("draft".equals(pageBean.inputValue("state")) && "Write".equals(pageBean.inputValue("editMode")))
	||("waitepublish".equals(pageBean.inputValue("state")) && "Publish".equals(pageBean.inputValue("editMode"))) 
	|| "admin".equals(pageBean.inputValue("userCode")) ){
%>	 
	 config.toolbar_Full =
	 [
	     ['Source','-','cmsave','Preview','-','Templates'],
	     ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
	     ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	     ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
	     '/',
	     ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	     ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
	     ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	     ['Link','Unlink','Anchor'],
	     ['Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
	     '/',
	     ['Styles','Format','Font','FontSize'],
	     ['TextColor','BGColor','lineheight'],
	     ['Maximize', 'ShowBlocks','-','cmprofile']
	 ];
<%}else{%>
	 config.toolbar_Full =
	 [
	     ['Source','-','Preview','-','Templates'],
	     ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
	     ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	     ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
	     '/',
	     ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	     ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
	     ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	     ['Link','Unlink','Anchor'],
	     ['Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
	     '/',
	     ['Styles','Format','Font','FontSize'],
	     ['TextColor','BGColor','lineheight'],
	     ['Maximize', 'ShowBlocks','-','cmprofile']
	 ];
<%}%>
	 config.toolbar_Basic =
	 [
	     ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-','About']
	 ];	
};

CKEDITOR.on('instanceReady',function(ev){
	with (ev.editor.dataProcessor.writer){
		setRules("p",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		/*
		setRules("h1",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("h2",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("h3",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("h4",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("h5",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("div",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("table",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("tr",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("td",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("iframe",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("li",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("ul",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		setRules("ol",{indent:false,breakBeforeOpen:false,breakAfterOpen:false,breakBeforeClose:false,breakAfterClose:false});
		*/
	}
});

CKEDITOR.BasePath='<%=path%>/ckeditor';
CKEDITOR.replace('content');

CKEDITOR.on('dialogDefinition', function( ev ){
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;
    if (dialogName == 'image'){
        var infoTab = dialogDefinition.getContents('info');
        
        infoTab.add({
            type : 'button',
            id : 'browse_image',
            align : 'center',
            label : '浏览图片',
            onClick : function( evt ){
                var thisDialog = this.getDialog();
                
                var txtUrlObj = thisDialog.getContentElement('info', 'txtUrl');
                var txtUrlId = txtUrlObj.getInputElement().$.id;
                uploadURLElementId = txtUrlId;
                
                var txtHeightObj = thisDialog.getContentElement('info', 'txtHeight');
                var txtHeightId = txtHeightObj.getInputElement().$.id;
                uploadHeightElementId = txtHeightId;
                
                var txtWidthObj = thisDialog.getContentElement('info', 'txtWidth');
                var txtWidthId = txtWidthObj.getInputElement().$.id;
                uploadWidthElementId = txtWidthId;
                
                addBrowseImage();
            }
        }, 'txtAlt'); //place front of the txtAlt input text
        
        infoTab.add({
            type : 'button',
            id : 'upload_image',
            align : 'center',
            label : '上传图片',
            onClick : function( evt ){
                var thisDialog = this.getDialog();
                
                var txtUrlObj = thisDialog.getContentElement('info', 'txtUrl');
                var txtUrlId = txtUrlObj.getInputElement().$.id;
                uploadURLElementId = txtUrlId;
                
                var txtHeightObj = thisDialog.getContentElement('info', 'txtHeight');
                var txtHeightId = txtHeightObj.getInputElement().$.id;
                uploadHeightElementId = txtHeightId;
                
                var txtWidthObj = thisDialog.getContentElement('info', 'txtWidth');
                var txtWidthId = txtWidthObj.getInputElement().$.id;
                uploadWidthElementId = txtWidthId;
                
                addUploadImage();
            }
        }, 'txtAlt'); //place front of the txtAlt input text
        
        
        infoTab.remove('txtAlt');
    };
});


var uploadURLElementId;
var uploadWidthElementId;
var uploadHeightElementId;

function afterSelectImage(uploadeImgObj){
	var urlObj = document.getElementById(uploadURLElementId);
	urlObj.value = uploadeImgObj.url;

	var browseObj =  $("img[id$='previewImage']");
    browseObj.prop('src',uploadeImgObj.url);
    browseObj.css('display','inline-block'); 
    
    var txtWidthObj = document.getElementById(uploadWidthElementId);
    txtWidthObj.value = uploadeImgObj.width;
    
    var txtHeightObj = document.getElementById(uploadHeightElementId);
    txtHeightObj.value = uploadeImgObj.height;
}

var uploadImageRequestBox;
function addUploadImage(){
	var title = "上载图片";
	if (!uploadImageRequestBox){
		uploadImageRequestBox = new PopupBox('uploadImageRequestBox',title,{size:'big',width:'260px',height:'400px',top:'3px'});
	}
	var columnIdValue = $("#columnId").val();
    var url = "/portal/index?ImageColumnSelect8Upload"; 
    uploadImageRequestBox.sendRequest(url);	
}


var browseImageRequestBox;
function addBrowseImage(){
	var title = "浏览图片";
	if (!browseImageRequestBox){
		browseImageRequestBox = new PopupBox('browseImageRequestBox',title,{size:'big',width:'990px',height:'525px',top:'3px'});
	}
	var columnIdValue = $("#columnId").val();
    var url = "/portal/index?PictureBrowser"; 
    browseImageRequestBox.sendRequest(url);	
}


CKEDITOR.instances["content"].on("instanceReady", function () {  
	$("#copyInfoId").zclip({
		path:"<%=request.getContextPath()%>/js/ZeroClipboard.swf",
		copy:$("#infomationId").val()
	});
}); 
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
