<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function selectRequest(){
	if (!isValid($("#targetInfoId").val())){
		writeErrorMsg('请填写关联信息标识!');
		return;
	}
	var reg = /^[0-9A-Z]{8}-[0-9A-Z]{4}-[0-9A-Z]{4}-[0-9A-Z]{4}-[0-9A-Z]{12}$/;
	if (!reg.test($("#targetInfoId").val())) {
		writeErrorMsg('关联信息ID应该是36位的UUID，请检查！');
		return;
	}	
	parent.afterInputInfoId($("#targetInfoId").val());
	parent.PopupBox.closeCurrent();
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table class="detailTable" cellspacing="0" cellpadding="0" style="margin-top:5px;">
  <tr>
    <td height="35" align="left">请填写关联信息标识</td>
  </tr>
  <tr>
    <td valign="top" nowrap><input name="targetInfoId" type="text" id="targetInfoId" size="36" />	</td>
  </tr>
  <tr>
    <td align="center">
    <input class="formbutton" type="button" name="Button23" value="确定" onclick="selectRequest()"/>&nbsp; &nbsp;
    <input class="formbutton" type="button" name="Button22" value="关闭" onclick="javascript:parent.PopupBox.closeCurrent();"/></td>
  </tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
</form>
<script type="text/javascript">
$(function(){
	selectOrFocus("targetInfoId");
});
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
