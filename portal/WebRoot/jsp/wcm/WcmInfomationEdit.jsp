<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript" src="js/jquery.zclip.js"></script>
<script language="javascript">
function saveRecord(){
	if (!validate()){
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if (responseText != 'fail'){
			if (parent.launchIframeWindow){
				parent.launchIframeWindow.refreshTree();
				reloagePage(responseText);
			}
		}
	}});	
}
function reloagePage(infomationId){
	$("#INFO_ID").val(infomationId);
	$("#operaType").val('update');
	doSubmit({actionType:'prepareDisplay'});
}

var addKeyWordsRefBox;
function addKeyWordsRef(){
	var title = "选择关键字";
	if (!addKeyWordsRefBox){
		addKeyWordsRefBox = new PopupBox('addKeyWordsRefBox',title,{size:'big',width:'260px',height:'400px',top:'3px'});
	}
    var url = "/portal/index?KeyWordsTreeSelect"; 
    addKeyWordsRefBox.sendRequest(url);	
}
function afterSelectKeyWordsRef(keyWordsRefs){
	var reqURL = "<%=pageBean.getHandlerURL()%>&actionType=addKeyWordsRef&INFO_ID=<%=pageBean.inputValue4DetailOrUpdate("INFO_ID","")%>&keyWordsRefs="+keyWordsRefs;
	sendRequest(reqURL,{onComplete:function(responseText){
		if ("alreadyAdded" == responseText){
			writeErrorMsg('该条关键字已设置，请检查!');
		}else{
			eval(responseText);
			PopupBox.closeCurrent();
		}	
	}});
}
function delKeyWordsRef(){
	if (!isValid($("#refKeyWords").val())){
		writeErrorMsg('请先选中一条关键字引用!');
		return;
	}
	if (confirm("确定要删除该关键字引用么？")){
		postRequest('form1',{actionType:'delKeyWordsRef',onComplete:function(responseText){
			eval(responseText);
		}});		
	}
}

var addInfoIdsRefBox;
function addInfoIdsRef(){
	var title = "关联信息标识";
	if (!addInfoIdsRefBox){
		addInfoIdsRefBox = new PopupBox('addInfoIdsRefBox',title,{size:'big',width:'360px',height:'230px',top:'3px'});
	}
    var url = "/portal/index?RelInfoIdInput"; 
    addInfoIdsRefBox.sendRequest(url);	
}

function afterInputInfoId(relInfoId){
	var reqURL = "<%=pageBean.getHandlerURL()%>&actionType=addInfoIdsRef&INFO_ID=<%=pageBean.inputValue4DetailOrUpdate("INFO_ID","")%>&newRelInfoId="+relInfoId;
	sendRequest(reqURL,{onComplete:function(responseText){
		if ("alreadyAdded" == responseText){
			writeErrorMsg('该信息标识已经添加关联，请检查!');
		}else{
			eval(responseText);
			PopupBox.closeCurrent();
		}	
	}});
}
function delInfoIdsRef(){
	if (!isValid($("#refInfoIds").val())){
		writeErrorMsg('请先选中一条关联信息!');
		return;
	}
	if (confirm("确定要删除该信息关联么？")){
		postRequest('form1',{actionType:'delInfoIdsRef',onComplete:function(responseText){
			eval(responseText);
		}});		
	}
}
function moveUpInfoIdsRef(){
	if (!isValid($("#refInfoIds").val())){
		writeErrorMsg('请先选中一条关联信息!');
		return;
	}
	if (ele("refInfoIds").selectedIndex == 0){
		writeErrorMsg('该关联信息是首条信息，不能向上！');
		return;
	}
	postRequest('form1',{actionType:'moveUpInfoIdsRef',onComplete:function(responseText){
		eval(responseText);
	}});		
}
function moveDownInfoIdsRef(){
	if (!isValid($("#refInfoIds").val())){
		writeErrorMsg('请先选中一条关联信息!');
		return;
	}
	if (ele("refInfoIds").selectedIndex == (ele("refInfoIds").options.length-1)){
		writeErrorMsg('该关联信息是末条信息，不能向下！');
		return;
	}	
	postRequest('form1',{actionType:'moveDownInfoIdsRef',onComplete:function(responseText){
		eval(responseText);
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<span style="float:left;">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveRecord()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.qBox.Close();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" ><a id="copyInfoId" style="display:inline-block;width:74px;height:14px;">复制信息ID</a></td>
</tr>
</table>
</span>
<%if (!pageBean.isOnCreateMode()){%>
<span style="float:right;height:29px;line-height:29px;"><a class="button-link green" href="index?InfomationContent&columnId=<%=pageBean.inputValue("COL_ID")%>&infomationId=<%=pageBean.inputValue("INFO_ID")%>&editMode=<%=pageBean.inputValue("editMode")%>">正文内容</a></span>
<%}%>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>标题</th>
	<td colspan="3"><input id="INFO_TITLE" label="标题" name="INFO_TITLE" type="text" value="<%=pageBean.inputValue("INFO_TITLE")%>" size="64" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>短标题</th>
	<td><input id="INFO_SHORTTITLE" label="短标题" name="INFO_SHORTTITLE" type="text" value="<%=pageBean.inputValue("INFO_SHORTTITLE")%>" size="24" class="text" /></td>
	<th width="100" nowrap>短标识</th>
	<td><input name="INFO_SHORTID" type="text" class="text" id="INFO_SHORTID" value="<%=pageBean.inputValue("INFO_SHORTID")%>" size="24" readonly="readonly" label="短标识" /></td>    
</tr>
<tr>
	<th width="100" nowrap>简要介绍</th>
	<td colspan="3"><textarea id="INFO_OUTLINE" label="简要介绍" name="INFO_OUTLINE" cols="105" rows="5" class="textarea"><%=pageBean.inputValue("INFO_OUTLINE")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>是否置顶</th>
	<td colspan="3">&nbsp;<%=pageBean.selectRadio("INFO_SETTOP")%></td>
</tr>
<tr>
	<th width="100" nowrap>支持评论</th>
	<td colspan="3">&nbsp;<%=pageBean.selectRadio("INFO_SUPPORT_COMMENT")%></td>
</tr>
<tr>
	<th width="100" nowrap>匿名评论</th>
	<td colspan="3">&nbsp;<%=pageBean.selectRadio("INFO_COMMENT_ANONYMOUS")%></td>
</tr>
<tr>
	<th width="100" nowrap>认证类型</th>
	<td><select style="width:188px;" id="INFO_AUTH_TYPE" label="认证类型" name="INFO_AUTH_TYPE" class="select">
	  <%=pageBean.selectValue("INFO_AUTH_TYPE")%>
	  </select></td>
	<th width="100" nowrap>当前状态</th>
	<td><%=pageBean.selectedText("INFO_STATE")%><input type="hidden" name="INFO_STATE" id="INFO_STATE" value="<%=pageBean.selectedValue("INFO_STATE")%>" /></td>
</tr>
<tr>
	<th width="100" nowrap>阅读次数</th>
	<td><input name="INFO_READ_COUNT" type="text" class="text" id="INFO_READ_COUNT" value="<%=pageBean.inputValue("INFO_READ_COUNT")%>" size="24" readonly="readonly" label="阅读次数" /></td>
	<th width="100" nowrap>同级排序</th>
	<td><input name="INFO_SORT_NO" type="text" class="text" id="INFO_SORT_NO" value="<%=pageBean.inputTime("INFO_SORT_NO")%>" size="24" label="同级排序" /></td>
</tr>
<tr>
	<th width="100" nowrap>采编人员</th>
	<td><input name="INFO_CREATER_NAME" type="text" class="text" id="INFO_CREATER_NAME" value="<%=pageBean.inputValue("INFO_CREATER_NAME")%>" size="24" readonly="readonly" label="采编人" /></td>
	<th width="100" nowrap>发布人员</th>
	<td><input name="INFO_PUBLISHER_NAME" type="text" class="text" id="INFO_PUBLISHER_NAME" value="<%=pageBean.inputValue("INFO_PUBLISHER_NAME")%>" size="24" readonly="readonly" label="发布人" /></td>
</tr>
<tr>
	<th width="100" nowrap>采编时间</th>
	<td><input name="INFO_CREATE_TIME" type="text" class="text" id="INFO_CREATE_TIME" value="<%=pageBean.inputTime("INFO_CREATE_TIME")%>" size="24" readonly="readonly" label="采编时间" /></td>
	<th width="100" nowrap>发布时间</th>
	<td><input name="INFO_PUBLISH_TIME" type="text" class="text" id="INFO_PUBLISH_TIME" value="<%=pageBean.inputTime("INFO_PUBLISH_TIME")%>" size="24" readonly="readonly" label="发布时间" /></td>
</tr>
<tr>
	<th width="100" nowrap>关键字引用</th>
	<td valign="top"><span style="float:left"><select name="refKeyWords" size="8" id="refKeyWords" style="width:270px;"><%=pageBean.selectValue("refKeyWords")%>
      </select></span>
      <%if (pageBean.isValid(pageBean.inputValue4DetailOrUpdate("INFO_ID",""))) {%>
      <span><input name="addKeyWordRef" type="button" value="添加" style="margin:2px;" onclick="addKeyWordsRef()" />
      <br />
      <input name="delKeyWordRef" type="button" value="删除" style="margin:2px;" onclick="delKeyWordsRef()" /></span>
      <%}%>
      </td>
	<th width="100" nowrap>关联信息</th>
	<td valign="top">
	<span style="float:left"><select name="refInfoIds" size="8" id="refInfoIds" style="width:270px;"><%=pageBean.selectValue("refInfoIds")%>
      </select></span>
      <%if (pageBean.isValid(pageBean.inputValue4DetailOrUpdate("INFO_ID",""))) {%>
      <span><input name="addKeyWordRef" type="button" value="添加" style="margin:2px;" onclick="addInfoIdsRef()" />
      <br />
      <input name="delKeyWordRef" type="button" value="上移" style="margin:2px;" onclick="moveUpInfoIdsRef()" />
      <br />
      <input name="delKeyWordRef" type="button" value="下移" style="margin:2px;" onclick="moveDownInfoIdsRef()" />
      <br />      
      <input name="delKeyWordRef" type="button" value="删除" style="margin:2px;" onclick="delInfoIdsRef()" /></span>            
      <%}%>    
    </td>      
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value="" />
<input type="hidden" name="editMode" id="editMode" value="<%=pageBean.inputValue("editMode")%>" />
<input type="hidden" name="userCode" id="userCode" value="<%=pageBean.inputValue("userCode")%>" />
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>" />
<input type="hidden" name="COL_ID" id="COL_ID" value="<%=pageBean.inputValue("COL_ID")%>" />
<input type="hidden" id="INFO_ID" name="INFO_ID" value="<%=pageBean.inputValue4DetailOrUpdate("INFO_ID","")%>" />
<input type="hidden" id="INFO_CREATER" name="INFO_CREATER" value="<%=pageBean.inputValue("INFO_CREATER")%>" />
<input type="hidden" id="INFO_PUBLISHER" name="INFO_PUBLISHER" value="<%=pageBean.inputValue("INFO_PUBLISHER")%>" />
</form>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);

lengthValidators[0].set(128).add("INFO_TITLE");
lengthValidators[1].set(64).add("INFO_SHORTTITLE");
intValidator.add("INFO_SORT_NO");
initDetailOpertionImage();
buttonControl('<%=pageBean.selectedValue("INFO_STATE")%>');

function buttonControl(currentState){
	if ('Write' == '<%=pageBean.inputValue("editMode")%>'){
		if ('draft' == currentState){
			//do nothing
		}
		else if ('waitepublish' == currentState && 'admin' != '<%=pageBean.inputValue("userCode")%>'){
			disableButton('saveImgBtn');
			disableButton('modifyImgBtn');
		}
		else if ('published' == currentState && 'admin' != '<%=pageBean.inputValue("userCode")%>'){
			disableButton('saveImgBtn');
			disableButton('modifyImgBtn');		
		}		
	}
	else if ('Publish' == '<%=pageBean.inputValue("editMode")%>'){
		if ('draft' == currentState && 'admin' != '<%=pageBean.inputValue("userCode")%>' ){
			disableButton('saveImgBtn');
			disableButton('modifyImgBtn');
		}
		else if ('waitepublish' == currentState){
			//do nothing
		}
		else if ('published' == currentState && 'admin' != '<%=pageBean.inputValue("userCode")%>' ){
			disableButton('saveImgBtn');
			disableButton('modifyImgBtn');
		}	
	}
}
$(function(){
	$("#copyInfoId").zclip({
		path:"<%=request.getContextPath()%>/js/ZeroClipboard.swf",
		copy:$("#INFO_ID").val()
	});
	$("#INFO_TITLE").focus();
})
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
