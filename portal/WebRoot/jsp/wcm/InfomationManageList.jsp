<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String isRootColumnId = (String)pageBean.getAttribute("isRootColumnId");
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="css/validate.css" />
<link rel="stylesheet" type="text/css" href="css/ecside.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/decorator.css" />
<link rel="stylesheet" type="text/css" href="css/calendar.css" />
<link rel="stylesheet" type="text/css" href="css/contextmenu.css" />
<link href="css/dtree.css" rel="stylesheet" type="text/css"/>
<link href="css/stree.css" rel="stylesheet" type="text/css"/>
<script src="js/jquery1.7.1.js" language="javascript"></script>

<script src="js/jquery.ui.core.js" language="javascript"></script>
<script src="js/jquery.ui.widget.js" language="javascript"></script>
<script src="js/jquery.ui.mouse.js" language="javascript"></script>
<script src="js/jquery.ui.draggable.js" language="javascript"></script>
<script src="js/jquery.ui.droppable.js" language="javascript"></script>

<script src="js/ArrayList.js" type="text/javascript"></script>
<script src="js/Map.js" type="text/javascript"></script>
<script src="js/ContextMenu.js" type="text/javascript"></script>
<script src="js/util.js" language="javascript"></script>
<script src="js/dtree.js" language="javascript"></script>
<script src="js/stree.js" language="javascript"></script>
<script src="js/calendar.js" language="javascript"></script>
<script src="js/PopupBox.js" language="javascript"></script>
<script src="js/validate/processor.js" language="javascript"></script>
<script src="js/validate/validator.js" language="javascript"></script>
<script src="js/validate/filter.js" language="javascript"></script>
<script src="js/validate/validator.config.js" language="javascript"></script>
<script src="js/ecside.js" language="javascript"></script>
<script type="text/javascript">
validateIsDisplayAllError = false;
</script>
<script language="javascript">
function openContentRequestBox(operaType,pageTitle,handlerId,subPKField){
	bachProcessIds();
	var infoIds = $("#infoIds").val();
	if(infoIds.length > 36){
		writeErrorMsg('只能选中一条记录!');
		return;
	}
	
	if ('insert' != operaType && infoIds == "" ){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	var columnIdValue = $("#curColumnId").val();
	if ('insert' == operaType){
		columnIdValue = $("#columnId").val();
	}	
	var editMode = $("#editMode").val();
	var url = '/portal/index?'+handlerId+'&COL_ID='+columnIdValue+'&operaType='+operaType
		+'&'+subPKField+'='+$("#"+subPKField).val()+'&editMode='+editMode;
	parent.doPopupPageBox(url,{title:pageTitle,width:1000,height:580});
	parent.launchIframeWindow = window;
}
function showFilterBox(){
	$('#filterBox').show();
	var clientWidth = $(document.body).width();
	var tuneLeft = (clientWidth - $("#filterBox").width())/2-2;	
	$("#filterBox").css('left',tuneLeft);
	$("#infoId").focus();
}
function judgeEditBox(infomationId){
	var reqURL = '<%=pageBean.getHandlerURL()%>&actionType=retrieveLastEditType';
	sendRequest(reqURL,{onComplete:function(responseText){
		var pageTitle = "";
		var url = "";
		if (responseText == 'profile'){
			pageTitle = "概要信息";
			url = '/portal/index?WcmInfomationEdit&COL_ID=<%=pageBean.inputValue("columnId")%>&operaType=detail&INFO_ID='+infomationId+'&editMode=<%=pageBean.inputValue("editMode")%>';		
		}else if (responseText == 'content'){
			pageTitle = "正文内容";
			url = '/portal/index?InfomationContent&columnId=<%=pageBean.inputValue("columnId")%>&infomationId='+infomationId+'&editMode=<%=pageBean.inputValue("editMode")%>';
		}	
		parent.doPopupPageBox(url,{title:pageTitle,width:1000,height:580});
		parent.launchIframeWindow = window;	
	}});
}
function doRemoveContent(){
	bachProcessIds();
	var infoIds = $("#infoIds").val();
	if(infoIds.length > 36){
		writeErrorMsg('只能选中一条记录!');
		return;
	}
	if (infoIds == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (confirm('确认要移除该条记录吗？')){
		postRequest('form1',{actionType:'isLastRelation',onComplete:function(responseText){
			if (responseText == 'true'){
				if (confirm('该信息只有一条关联记录，确认要删除吗？')){
					doSubmit({actionType:'delete'});
				}
			}else{
				doSubmit({actionType:'removeContent'});
			}
		}});	
	}
}

function isSelectedTree(){
	if (isValid($('#columnId').val())){
		return true;
	}else{
		return false;
	}
}
function refreshTree(){
	doQuery();
}
function refreshContent(curNodeId){
	if (curNodeId){
		$('#columnId').val(curNodeId);
	}
	doSubmit({actionType:'query'});
}
var targetTreeBox;
function openTargetTreeBox(curAction){
	var columnIdValue = $("#columnId").val();
	if (!isSelectedTree()){
		writeErrorMsg('请先选中一个树节点!');
		return;
	}
	if (curAction == 'copyContent' || curAction == 'moveContent'){
		bachProcessIds();
		var infoIds = $("#infoIds").val();
		if(infoIds.length > 36){
			writeErrorMsg('只能选中一条记录!');
			return;
		}
		if (infoIds == ""){
			writeErrorMsg('请先选中一条记录!');
			return;
		}
		columnIdValue = $("#curColumnId").val()
	}	
	if (!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标分组',{size:'normal',width:'300px',top:'3px'});
	}
	var handlerId = "WcmColumnPick";
	var url = 'index?'+handlerId+'&COL_ID='+columnIdValue;
	targetTreeBox.sendRequest(url);
	$("#actionType").val(curAction);
}
function doChangeParent(){
	var curAction = $('#actionType').val();
	postRequest('form1',{actionType:curAction,onComplete:function(responseText){
		if (responseText == 'success'){
			if (curAction == 'moveTree'){
				refreshTree();			
			}else{
				refreshContent($("#targetParentId").val());
			}
		}else {
			writeErrorMsg('迁移父节点出错啦！');
		}
	}});
}
function clearFilter(){
	$("#filterBox input[type!='button'],select").val('');
}
function buttonControl(currentState,currentAuthType,indexId){
	
	if ('Write' == '<%=pageBean.inputValue("editMode")%>'){
		if ('draft' == currentState){
			enableButton('removeImgBtn');
			enableButton('delImgBtn');		
		
			enableButton('unsubmitImgBtn');
			enableButton('submitImgBtn');
		}
		else if ('waitepublish' == currentState){
			disableButton('removeImgBtn');
			disableButton('delImgBtn');
			
			enableButton('submitImgBtn');
			enableButton('unsubmitImgBtn');
		}
		else if ('published' == currentState){
			disableButton('removeImgBtn');
			disableButton('delImgBtn');
			
			enableButton('submitImgBtn');
			enableButton('unsubmitImgBtn');
		}		
	}
	else if ('Publish' == '<%=pageBean.inputValue("editMode")%>'){
		if ('draft' == currentState){
			enableButton('approveImgBtn');
			enableButton('unapproveImgBtn');
			if('admin' == '<%=pageBean.inputValue("userCode")%>'){
				enableButton('editImgBtn');
			}else{
				disableButton('editImgBtn');
			}
		}
		else if ('waitepublish' == currentState){
			enableButton('unapproveImgBtn');
			enableButton('approveImgBtn');
			enableButton('editImgBtn');
		}
		else if ('published' == currentState){
			enableButton('approveImgBtn');
			enableButton('unapproveImgBtn');
			if('admin' == '<%=pageBean.inputValue("userCode")%>'){
				enableButton('editImgBtn');
			}else{
				disableButton('editImgBtn');
			}
		}			
	}
	
	if ('public' == currentAuthType){
		disableButton('assignImgBtn');
	}
	else if ('protected' == currentAuthType){
		disableButton('assignImgBtn');
	}
	else if ('private' == currentAuthType){
		enableButton('assignImgBtn');
	}
	
	var idInt = parseInt(indexId) +1;
	var currentIndexId = idInt ;
	if($("#ec_table tr:eq("+currentIndexId+") input[name = 'INFO_ID']").is(':checked')){
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'INFO_ID']").attr('checked',false);
	}else{
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'INFO_ID']").attr('checked',true);
	}
}
function modifyInfomationState(bizActionType){
	bachProcessIds();
	if ($("#infoIds").val() == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	$("#bizActionType").val(bizActionType);
	
	postRequest('form1',{actionType:'modifyInfomationStates',onComplete:function(responseText){
		if ("success" == responseText){
			refreshTree();
		}else if("submit" == bizActionType){
			writeErrorMsg('只有拟稿中的信息才可提交！');
			return;
		}
		else if("unsubmit" == bizActionType){
			writeErrorMsg('只有待发布的信息才可反提交！');
			return;
		}
		else if("approve" == bizActionType){
			writeErrorMsg('只有待发布的信息才可发布！');
			return;
		}
		else if("unapprove" == bizActionType){
			writeErrorMsg('只有已发布的信息才可反发布！');
			return;
		}
	}});
}

function bachProcessIds(){
	var infoIds = "";
	$("input:[name = 'INFO_ID'][checked]").each(function(){   
		infoIds = infoIds+$(this).val()+",";
	});
	if (infoIds.length > 0){
		infoIds = infoIds.substring(0,infoIds.length-1);
	}
	$("#infoIds").val(infoIds);
}

var configSecurityBox;
function configSecurityRequest(){
	bachProcessIds();
	var infoIds = $("#infoIds").val();
 	if(infoIds.length > 36){
		writeErrorMsg('只能选中一条记录!');
		return;
	}
	
 	if (infoIds == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}	
	var infomationId = $("#INFO_ID").val();
	if (!configSecurityBox){
		configSecurityBox = new PopupBox('configSecurityBox','安全配置',{size:'normal',width:'700px',height:'450px',top:'2px'});
	}
	var url = 'index?SecurityAuthorizationConfig&resourceType=Infomation&resourceId='+infomationId;
	configSecurityBox.sendRequest(url);	
}

function tryDelete(infoId){
	bachProcessIds();
	var infoIds = $("#infoIds").val();
	if(infoIds.length > 36){
		writeErrorMsg('只能选中一条记录!');
		return;
	}
	if (infoIds == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if(confirm(confirmMsg)) {
		var url = '<%=pageBean.getHandlerURL()%>&actionType=isExistRelation&infoId='+infoId;
		sendRequest(url,{onComplete:function(responseText){
			if (responseText == 'Y'){
				writeErrorMsg('该信息存在授权关联，请先删除授权关联！');
			}	
			else{
				showSplash(deleteMsg);
			  	$("#actionType").val(deleteActionValue);				
				$('#'+formTagId).submit();
			}
		}});
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;padding:0px;" cellpadding="1" cellspacing="0">
<tr>
	<td valign="top">
    <div id="leftTree" class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">&nbsp;&nbsp;分组列表 </h3>        
        <div id="treeArea" style="overflow:auto; height:420px;width:230px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
    <%=pageBean.getStringValue("menuTreeSyntax")%></div>
    </div>
    <b class="b9"></b>
    </div>
    <input type="hidden" id="columnId" name="columnId" value="<%=pageBean.inputValue("columnId")%>" />
    <input type="hidden" id="targetParentId" name="targetParentId" value="" />
    </td>
	<td width="85%" valign="top">
<div id="__ToolBar__">
<span style="float:right;height:28px;line-height:28px;"><input style="vertical-align:middle; margin-top:-2px; margin-bottom:1px;" name="showChildNodeRecords" type="checkbox" id="showChildNodeRecords" onclick="doQuery()" value="Y" <%=pageBean.checked(pageBean.inputValue("showChildNodeRecords"))%> />&nbsp;显示子节点记录&nbsp;&nbsp;状态&nbsp;<select id="infoState" label="信息状态" name="infoState" onchange="doQuery()" class="select"><%=pageBean.selectValue("infoState")%></select></span>
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if("false".equals(isRootColumnId) && "Write".equals(pageBean.inputValue("editMode"))){%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="openContentRequestBox('insert','信息列表','WcmInfomationEdit','INFO_ID')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" style="margin-right:" />新增</td>
<%}%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="openContentRequestBox('update','信息列表','WcmInfomationEdit','INFO_ID')"><input id="editImgBtn" value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
<%if ("Write".equals(pageBean.inputValue("editMode"))){%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="openContentRequestBox('copy','信息列表','WcmInfomationEdit','INFO_ID')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
<%}%>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="openContentRequestBox('detail','信息列表','WcmInfomationEdit','INFO_ID')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="S" align="center" onclick="configSecurityRequest()"><input value="&nbsp;" title="安全" type="button" class="assignImgBtn" id="assignImgBtn" />安全</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="F" align="center" onclick="showFilterBox()"><input value="&nbsp;" title="过滤" type="button" class="filterImgBtn" />过滤</td>
<%if ("Write".equals(pageBean.inputValue("editMode"))){%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="tryDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" id="delImgBtn" title="删除" type="button" class="delImgBtn" />删除</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="R" align="center" onclick="doRemoveContent()"><input value="&nbsp;" id="removeImgBtn" title="删除" type="button" class="removeImgBtn" />移除</td>
<%}%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="Z" align="center" onclick="openTargetTreeBox('copyContent')"><input value="&nbsp;" title="分发" type="button" class="addImgBtn" />分发</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openTargetTreeBox('moveContent')"><input value="&nbsp;" title="迁移" type="button" class="moveImgBtn" />迁移</td>
<%if ("Write".equals(pageBean.inputValue("editMode"))){%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="Q" align="center" onclick="modifyInfomationState('submit')"><input id="submitImgBtn" value="&nbsp;" title="提交" type="button" class="submitImgBtn" />提交</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="modifyInfomationState('unsubmit')"><input id="unsubmitImgBtn" value="&nbsp;" title="反提交" type="button" class="unsubmitImgBtn" />反提交</td>
<%}else{%>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="W" align="center" onclick="modifyInfomationState('approve')"><input id="approveImgBtn" value="&nbsp;" title="发布" type="button" class="approveImgBtn" />发布</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="modifyInfomationState('unapprove')"><input id="unapproveImgBtn" value="&nbsp;" title="反发布" type="button" class="unapproveImgBtn" />反发布</td>
<%}%>
</tr>
</table>
</div>
<div id="rightArea">    
<ec:table
form="form1"
var="row"
items="pageBean.rsList" csvFileName="信息列表.csv"
retrieveRowsCallback="process" xlsFileName="信息列表.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px" 
>
<ec:row styleClass="odd" ondblclick="clearSelection();judgeEditBox('${row.INFO_ID}')" oncontextmenu="buttonControl('${row.INFO_STATE}','${row.INFO_AUTH_TYPE}');selectRow(this,{curColumnId:'${row.COL_ID}'});refreshConextmenu()" onclick="buttonControl('${row.INFO_STATE}','${row.INFO_AUTH_TYPE}','${row.INDEX_ID}');selectRow(this,{curColumnId:'${row.COL_ID}' })">
	<ec:column width="25" style="text-align:center" property="INFO_ID" cell="checkbox" headerCell="checkbox" />
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="300" property="INFO_TITLE" title="标题"  maxLength="28"  />
	<ec:column width="100" property="INFO_AUTH_TYPE" title="认证类型" mappingItem="INFO_AUTH_TYPE"/>
	<ec:column width="100" property="INFO_STATE" title="当前状态"   mappingItem="INFO_STATE"/>
	<ec:column width="80" property="INFO_SETTOP" title="是否置顶"   mappingItem="INFO_SETTOP"/>
	<ec:column width="80" property="INFO_SUPPORT_COMMENT" title="支持评论"   mappingItem="INFO_SUPPORT_COMMENT"/>
	<ec:column width="80" property="INFO_READ_COUNT" title="阅读次数"   />
	<ec:column width="80" property="INFO_SORT_NO" title="同级排序"  />
</ec:row>
</ec:table>
<div id="filterBox" class="sharp color2" style="position:absolute;top:30px;z-index:10; width:480px;display:none;">
<b class="b9"></b>
<div class="content" style="background-color:white;">
<h3>&nbsp;&nbsp;条件过滤框</h3>
<table class="detailTable" cellpadding="0" cellspacing="0" style="width:99%;margin:1px;">
<tr>
	<th width="100" nowrap>信息标识</th>
	<td><input id="infoId" label="信息标识" name="infoId" type="text" value="<%=pageBean.inputValue("infoId")%>" size="40" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>信息标题</th>
	<td><input id="infoTitle" label="标题" name="infoTitle" type="text" value="<%=pageBean.inputValue("infoTitle")%>" size="40" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>认证类型</th>
	<td><select id="infoAuthType" label="认证类型" name="infoAuthType" class="select"><%=pageBean.selectValue("infoAuthType")%></select></td>
</tr>
<tr>
	<th width="100" nowrap>是否置顶</th>
	<td><select id="infoSetTop" label="是否置顶" name="infoSetTop" class="select">
	  <%=pageBean.selectValue("infoSetTop")%>
	  </select></td>
</tr>
<tr>
	<th width="100" nowrap>支持评论</th>
	<td><select id="supportComment" label="是否支持评论" name="supportComment" class="select">
	  <%=pageBean.selectValue("supportComment")%>
	  </select></td>
</tr>
<tr>
	<th width="100" nowrap>匿名评论</th>
	<td><select id="commentAnonymous" label="能否匿名评论" name="commentAnonymous" class="select">
	  <%=pageBean.selectValue("commentAnonymous")%>
	  </select></td>
</tr>
<tr>
	<th width="100" nowrap>采编人员</th>
	<td><input id="infoCreater" label="采编人" name="infoCreater" type="text" value="<%=pageBean.inputValue("infoCreater")%>" size="10" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>发布人员</th>
	<td><input id="infoPublisher" label="发布人" name="infoPublisher" type="text" value="<%=pageBean.inputValue("infoPublisher")%>" size="10" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>阅读次数</th>
	<td>从
	  <input id="minReadCount" label="阅读次数：从" name="minReadCount" type="text" value="<%=pageBean.inputValue("minReadCount")%>" size="10" class="text" />次
	  至
	  <input id="maxReadCount" label="至" name="maxReadCount" type="text" value="<%=pageBean.inputValue("maxReadCount")%>" size="10" class="text" />次</td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td>从
	  <input id="startCreateTime" label="创建时间，从" name="startCreateTime" type="text" value="<%=pageBean.inputDate("startCreateTime")%>" size="10" class="text" /><img id="startCreateTimePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
	  至
	  <input id="endCreateTime" label="至" name="endCreateTime" type="text" value="<%=pageBean.inputDate("endCreateTime")%>" size="10" class="text" /><img id="endCreateTimePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" /></td>
</tr>
<tr>
	<th width="100" nowrap>发布时间</th>
	<td>从
	  <input id="startPublishTime" label="发布时间，从" name="startPublishTime" type="text" value="<%=pageBean.inputDate("startPublishTime")%>" size="10" class="text" /><img id="startPublishTimePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
	  至
	    <input id="endPublishTime" label="至" name="endPublishTime" type="text" value="<%=pageBean.inputDate("endPublishTime")%>" size="10" class="text" /><img id="endPublishTimePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" /></td>
</tr>
</table>
<div style="width:100%;text-align:center;height:33px;line-height:33px;">
<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
&nbsp;&nbsp;
<input type="button" name="button" id="button" value="清空" class="formbutton" onclick="clearFilter()" />
&nbsp;&nbsp;<input type="button" name="button" id="button" value="关闭" class="formbutton" onclick="javascript:$('#filterBox').hide();" /></div>
</div>
<b class="b9"></b>
</div>
<input type="hidden" id="_tabId_" name="_tabId_" value="<%=pageBean.inputValue("_tabId_")%>" />
<input type="hidden" name="curColumnId" id="curColumnId" value="" />
<input type="hidden" name="indexId" id="indexId" value="" />
<input type="hidden" name="bizActionType" id="bizActionType" value="" />
<input type="hidden" id="infoIds" name="infoIds" value="<%=pageBean.inputValue("infoIds")%>" />
<input type="hidden" id="userCode" name="userCode" value="<%=pageBean.inputValue("userCode")%>" />
<script language="JavaScript">
setRsIdTag('INFO_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
initCalendar('startCreateTime','%Y-%m-%d','startCreateTimePicker');
initCalendar('endCreateTime','%Y-%m-%d','endCreateTimePicker');
initCalendar('startPublishTime','%Y-%m-%d','startPublishTimePicker');
initCalendar('endPublishTime','%Y-%m-%d','endPublishTimePicker');


$(window).load(function() {
	setTimeout(function(){
		resetTreeHeight(70);
		resetRightAreaHeight(72);
	},1);	
});
</script>
</div>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="editMode" id="editMode" value="<%=pageBean.inputValue("editMode")%>" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>