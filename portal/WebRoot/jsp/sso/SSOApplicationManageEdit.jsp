<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String currentSubTableId = pageBean.getStringValue("currentSubTableId");
String currentSubTableIndex = pageBean.getStringValue("currentSubTableIndex");
%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>单点应用管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveMasterRecord(){
	if (validate()){
		if (ele("currentSubTableId")){
			var subTableId = $("currentSubTableId").val();
			if (!checkEntryRecords(subTableId)){
				return;
			}
		}
		if ($("#APP_TYPE").val() == "BS" && $("#APP_AUTH_TYPE").val() == "FORM"){
			if (!isValueValid("APP_TIMEOUT")){
				writeErrorMsg('表单登录方式，过期时间属性不能为空！');
				return;
			}
			if (!validation.isPositiveInt($("#APP_TIMEOUT").val())){
				writeErrorMsg('过期时间属性过期时间只能为数字！');
				return;
			}
		}
		postRequest('form1',{actionType:'saveMasterRecord',onComplete:function(responseText){
			if ("fail" != responseText){
				alert('保存成功！');
				$('#operaType').val('update');
				$('#APP_ID').val(responseText);
				doSubmit({actionType:'prepareDisplay'});
			}else{
				alert('保存操作出错啦！');
			}
		}});
	}
}
function changeSubTable(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'changeSubTable'});
}
function refreshPage(){
	doSubmit({actionType:'changeSubTable'});
}
function addEntryRecord(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'addEntryRecord'});
}
function deleteEntryRecord(subTableId){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (confirm('确认要删除该条记录吗？')){
		$('#currentSubTableId').val(subTableId);
		doSubmit({actionType:'deleteEntryRecord'});	
	}
}

function checkEntryRecords(subTableId){
	var result = true;
	var currentRecordSize = $('#currentRecordSize').val();
	for (var i=0;i < currentRecordSize;i++){
		var codeId = "PARAM_CODE_"+i;
		var nameId = "PARAM_NAME_"+i;
		var sortId = "PARAM_SORT_"+i;
		var encriptionId = "ENCRIPTION_"+i;
		
		if (validation.checkNull($("#"+codeId).val())){
			writeErrorMsg($("#"+codeId).attr("label")+"不能为空!");
			selectOrFocus(codeId);
			return false;
		}
		if (validation.checkNull($("#"+nameId).val())){
			writeErrorMsg($("#"+nameId).attr("label")+"不能为空!");
			selectOrFocus(nameId);
			return false;
		}
		if (validation.checkNull($("#"+sortId).val())){
			writeErrorMsg($("#"+sortId).attr("label")+"不能为空!");
			selectOrFocus(sortId);
			return false;
		}
		
		if (validation.checkNull($("#"+encriptionId).val())){
			writeErrorMsg($("#"+encriptionId).attr("label")+"不能为空!");
			selectOrFocus(sortId);
			return false;
		}	
		
		if (!validation.isPositiveInt($("#"+sortId).val())){
			writeErrorMsg($("#"+sortId).attr("label")+"必须为整数!");
			selectOrFocus(sortId);
			return false;
		}	
	}		
	return result;
}
var insertSubRecordBox;
function insertSubRecordRequest(title,handlerId){
	if (!insertSubRecordBox){
		insertSubRecordBox = new PopupBox('insertSubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType=insert&APP_ID='+$('#APP_ID').val();
	insertSubRecordBox.sendRequest(url);	
}
var copySubRecordBox;
function copySubRecordRequest(title,handlerId,subPKField){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!copySubRecordBox){
		copySubRecordBox = new PopupBox('copySubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType=insert&'+subPKField+'='+$("#"+subPKField).val();
	copySubRecordBox.sendRequest(url);	
}
var viewSubRecordBox;
function viewSubRecordRequest(operaType,title,handlerId,subPKField){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!viewSubRecordBox){
		viewSubRecordBox = new PopupBox('viewSubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	viewSubRecordBox.sendRequest(url);
}

var configSecurityBox;
function configSecurityRequest(appId){
	if (!configSecurityBox){
		configSecurityBox = new PopupBox('configSecurityBox','安全配置',{size:'normal',width:'700px',height:'450px',top:'2px'});
	}
	var url = 'index?SecurityAuthorizationConfig&resourceType=Application&resourceId='+appId;
	configSecurityBox.sendRequest(url);	
}

function deleteSubRecord(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (confirm('确认要删除该条记录吗？')){
		doSubmit({actionType:'deleteSubRecord'});	
	}
}

function controlAppType(appType){
	if (appType == "CS"){
		$('#form1 #authTypeTr').hide();
		$('#form1 #timeOutTr').hide();
	}else{
		$('#form1 #authTypeTr').show();
		$('#form1 #timeOutTr').show();
	}
	$('#form1 #extURLTr').show();
}
function controlAuthType(authType){
	if (authType == "CAS"){
		$('#form1 #timeOutTr').hide();
	}else{
		$('#form1 #timeOutTr').show();
	}
	$('#form1 #extURLTr').show();
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveMasterRecord();"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
<%if (!"insert".equals(pageBean.getOperaType())){%>	
    <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="configSecurityRequest('<%=pageBean.inputValue("APP_ID")%>');"><input value="&nbsp;" type="button" class="assignImgBtn" id="saveImgBtn" title="安全设置" />安全设置</td>
<%}%>     
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="APP_NAME" label="名称" name="APP_NAME" type="text" value="<%=pageBean.inputValue("APP_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分组</th>
	<td><select id="APP_GROUP" label="分组" name="APP_GROUP" class="select"><%=pageBean.selectValue("APP_GROUP")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>类型</th>
	<td><select id="APP_TYPE" label="类型" name="APP_TYPE" class="select" onchange="controlAppType(this.value)"><%=pageBean.selectValue("APP_TYPE")%></select>
</td>
</tr>
<tr id="authTypeTr">
	<th width="100" nowrap>认证类型</th>
	<td><select id="APP_AUTH_TYPE" label="认证类型" name="APP_AUTH_TYPE" class="select" onchange="controlAuthType(this.value)"><%=pageBean.selectValue("APP_AUTH_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>访问URL</th>
	<td><textarea name="APP_PATH" cols="120" rows="1" class="text" id="APP_PATH" label="访问URL"><%=pageBean.inputValue("APP_PATH")%></textarea></td>
</tr>
<tr id="extURLTr">
	<th width="100" nowrap>扩展URL</th>
	<td><textarea name="EXT_URL" cols="120" rows="1" class="text" id="EXT_URL" label="扩展URL"><%=pageBean.inputValue("EXT_URL")%></textarea></td>
</tr>
<tr>
	<th width="100" nowrap>样式类</th>
	<td><input id="APP_CSS_CLASS" label="样式类" name="APP_CSS_CLASS" type="text" value="<%=pageBean.inputValue("APP_CSS_CLASS")%>" size="24" class="text" />
</td>
</tr>
<tr id="timeOutTr">
	<th width="100" nowrap>过期时间</th>
	<td><input id="APP_TIMEOUT" label="过期时间" name="APP_TIMEOUT" type="text" value="<%=pageBean.inputValue4DetailOrUpdate("APP_TIMEOUT","10")%>" size="24" class="text" />
	  分钟</td>
</tr>
<tr>
	<th width="100" nowrap>排序</th>
	<td><input id="APP_SORT" label="排序" name="APP_SORT" type="text" value="<%=pageBean.inputValue("APP_SORT")%>" size="24" class="text" />
</td>
</tr>
</table>
<%if (!"insert".equals(pageBean.getOperaType())){%>
<div class="photobg1" id="tabHeader">
<div class="newarticle1" onclick="changeSubTable('param')">应用参数</div>
</div>
<%if ("param".equals(currentSubTableId)){ %>
<div class="photobox newarticlebox" id="Layer0" style="height:auto;">    
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="addEntryRecord('param')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="deleteEntryRecord('param')"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="refreshPage()"><input value="&nbsp;" title="取消" type="button" class="cancelImgBtn" />取消</td>
</tr>   
   </table>
</div>
<div style="margin:2px;">
<table border="0" cellpadding="0" cellspacing="0" class="dataTable ecSide" id="dataTable">
<thead>
  <tr>
    <th width="80" align="center" nowrap="nowrap">序号</th>
    <th width="20%" align="center">编码</th>
    <th width="20%" align="center">名称</th>
    <th width="20%" align="center">加密</th>
    <th width="30%" align="center">赋值表达式</th>
    <th width="10%" align="center">排序</th>
  </tr>
</thead>
<tbody>
<%
List paramRecords = (List)pageBean.getAttribute("paramRecords");
pageBean.setRsList(paramRecords);
int paramSize = pageBean.listSize();
for (int i=0;i < paramSize;i++){
%>
  <tr onmouseout="ECSideUtil.unlightRow(this);" onmouseover="ECSideUtil.lightRow(this);" onclick="ECSideUtil.selectRow(this,'form1');selectRow(this,{currentRecordIndex:'<%=i%>'})">
	<td style="text-align:center"><%=i+1%>
	<input id="PARAM_ID_<%=i%>" name="PARAM_ID_<%=i%>" type="hidden" value="<%=pageBean.inputValue(i,"PARAM_ID")%>" />
	<input id="APP_ID_<%=i%>" name="APP_ID_<%=i%>" type="hidden" value="<%=pageBean.inputValue(i,"APP_ID")%>" />
	<input id="state_<%=i%>" name="state_<%=i%>" type="hidden" value="<%=pageBean.inputValue(i,"_state")%>" /></td>
	<td><input id="PARAM_CODE_<%=i%>" label="编码" name="PARAM_CODE_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"PARAM_CODE")%>" size="20" class="text" /></td>
	<td><input id="PARAM_NAME_<%=i%>" label="名称" name="PARAM_NAME_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"PARAM_NAME")%>" size="20" class="text" /></td>
	<td><select id="ENCRIPTION_<%=i%>" label="是否加密" name="ENCRIPTION_<%=i%>" class="select"><%=pageBean.selectValue(i,"ENCRIPTION","ENCRIPTION")%></select></td>    
	<td><input id="VALUE_EXPRESSION_<%=i%>" label="赋值表达式" name="VALUE_EXPRESSION_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"VALUE_EXPRESSION")%>" size="30" class="text" /></td>    
	<td><input id="PARAM_SORT_<%=i%>" label="排序" name="PARAM_SORT_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"PARAM_SORT")%>" size="10" class="text" /></td>            
  </tr>
<%}%>
</tbody>  
</table>
</div>
</div>
<input type="hidden" id="currentRecordSize" name="currentRecordSize" value="<%=pageBean.listSize()%>" />
<input type="hidden" id="currentRecordIndex" name="currentRecordIndex" value="" />
<script language="javascript">
<%for (int i=0;i < paramSize;i++){%>
	$("input[id$='_<%=i%>'],select[id$='_<%=i%>']").change(function(){
		if ($("#state_<%=i%>").val()==""){
			$("#state_<%=i%>").val('update');
		}
	});
<%}%>
setRsIdTag('currentRecordIndex');
</script>
<%}%>
<input type="hidden" id="currentSubTableId" name="currentSubTableId" value="<%=pageBean.inputValue("currentSubTableId")%>" />
<script language="javascript">
new Tab('tab','tabHeader','Layer',<%=currentSubTableIndex%>);
</script>
<%}%>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="APP_ID" name="APP_ID" value="<%=pageBean.inputValue("APP_ID")%>" />
</form>
<script language="javascript">
intValidator.add("APP_SORT","APP_TIMEOUT");
requiredValidator.add("APP_NAME","APP_PATH","APP_TYPE","APP_SORT","APP_GROUP");
initDetailOpertionImage();
controlAppType('<%=pageBean.selectedValue("APP_TYPE")%>');
controlAuthType('<%=pageBean.selectedValue("APP_AUTH_TYPE")%>');
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
