<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>单点应用配置</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveParam(){
	if (validate()){
		postRequest('form1',{actionType:'saveParam',onComplete:function(responseText){
			if (responseText=="sucess"){
				parent.PopupBox.closeCurrent();	
				parent.refreshSSOList();
			}else{
				alert('保存单点应用参数出错了，请确认！');
				parent.PopupBox.closeCurrent();		
			}
		}});
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveParam()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>是否显示</th>
	<td width="80%"><%=pageBean.selectRadio("IS_VISIABLE") %></td>
</tr>
<%for (int i=0;i < pageBean.listSize();i++){ %>
<tr>
	<th width="100" nowrap><%=pageBean.labelValue(i,"PARAM_NAME")%></th>
<%if(!"Y".equals(pageBean.inputValue(i,"ENCRIPTION"))){%>    
	<td width="80%">
	<input label="<%=pageBean.labelValue(i,"PARAM_NAME")%>" id="PARAM_VALUE<%=i%>" name="PARAM_VALUE<%=i%>" type="text" value="<%=pageBean.inputValue(i,"PARAM_VALUE")%>" size="50" class="text" />
	<input id="PARAM_ID<%=i%>" name="PARAM_ID<%=i%>" type="hidden" value="<%=pageBean.inputValue(i,"PARAM_ID")%>" />
	</td>
<%}else{ %>	
	<td width="80%"><input label="<%=pageBean.labelValue(i,"PARAM_NAME")%>" id="PARAM_VALUE<%=i%>" name="PARAM_VALUE<%=i%>" type="password" value="<%=pageBean.inputValue(i,"PARAM_VALUE")%>" size="50" class="text" />
	<input id="PARAM_ID<%=i%>" name="PARAM_ID<%=i%>" type="hidden" value="<%=pageBean.inputValue(i,"PARAM_ID")%>" />
	</td>
<%}%>    
</tr>
<%}%>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="TICKET_ID" id="TICKET_ID" value="<%=pageBean.getStringValue("TICKET_ID")%>"/>
<input type="hidden" name="entrySize" id="entrySize" value="<%=pageBean.listSize()%>"/>
</form>
</body>
</html>
<script language="JavaScript">
<%for (int i=0;i < pageBean.listSize();i++){ %>
requiredValidator.add("PARMA_VALUE<%=i%>");
<%}%>
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
