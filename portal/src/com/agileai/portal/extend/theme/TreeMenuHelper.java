package com.agileai.portal.extend.theme;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

public class TreeMenuHelper {
	private final static String LastSelectIndexKeysSessionKey = "LastSelectIndexKeysSessionKey";
	private HttpSession session = null;
	private JSONObject menuJsonObject = null;
	
	private String topSelectedIndex = "-1";
	private boolean isTopSelected = false;
	private Integer selectedIndex = -1;
	private HashMap<String,Integer> selectIndexKeys = new HashMap<String,Integer>();
	private  ArrayList<String> treeMenuIdList = new ArrayList<String>();
	
	private boolean showHomePage = false;
	private String homePageURL = null;
	
	public TreeMenuHelper(HttpSession session,JSONObject menuJsonObject){
		this.menuJsonObject = menuJsonObject;
		this.session = session;
	}
	
	public boolean isShowHomePage() {
		return showHomePage;
	}

	public void setShowHomePage(boolean showHomePage) {
		this.showHomePage = showHomePage;
	}

	public String buildMenuSyntax(){
		StringBuffer result = new StringBuffer();
		try{
			JSONArray jsonArray =  (JSONArray)menuJsonObject.get("menus");
			for (int i=0;i < jsonArray.length();i++){
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String type = jsonObject.getString("type");
				String text = jsonObject.getString("text");
				
				if (jsonObject.has("selected") && "true".equals(String.valueOf(jsonObject.get("selected")))){
					topSelectedIndex = String.valueOf(i);
				}
				if ("folder".equals(type)){
					selectedIndex = -1;
					treeMenuIdList.add(String.valueOf(i));
					
					if (jsonObject.has("selected") && "true".equals(String.valueOf(jsonObject.get("selected")))){
						isTopSelected = true;
					}
					
					result.append("<div class=\"headerContainer\"><div id=\"accordionHeader").append(i).append("\" class=\"AccordionTitle\" onClick=\"runAccordion(").append(i).append(");\" onselectstart=\"return false;\">").append(text);
					
					if (jsonObject.has("remark")){
						result.append("<div class=\"").append(jsonObject.get("remark")).append("\"></div>");
					}else{
						result.append("<div class=\"headerIconOk\"></div>");						
					}
					
					result.append("<div class=\"headerTool\"><a href=\"javascript:void(0)\" class=\"accordion-collapse accordion-expand\"></a></div>");
					result.append("</div>");
					result.append("</div>");
					result.append("\r\n");
					result.append("<div id=\"Accordion").append(i).append("Content\" class=\"AccordionContent\">");
					result.append("<ul id=\"treeMenu").append(i).append("\" class=\"easyui-tree\">");
					loopMenuSyntax(result, jsonObject);
					result.append("</li>");
					result.append("</ul>");
					result.append("</div>");
				}
				else if ("page".equals(type)){
					String itemURL = jsonObject.getString("href");
					String itemTarget = jsonObject.getString("target");
					if (i==0){
						this.homePageURL = itemURL;
						if (showHomePage){
							if (jsonObject.has("selected") && "true".equals(String.valueOf(jsonObject.get("selected")))){
								isTopSelected = true;
							}	
							result.append("<div class=\"headerContainer\"><div id=\"accordionHeader").append(i).append("\" class=\"AccordionTitle0\" onselectstart=\"return false;\"><a href=\"").append(itemURL).append("\" target=\"").append(itemTarget).append("\">").append(text).append("</a>");
							if (jsonObject.has("remark")){
								result.append("<div class=\"").append(jsonObject.get("remark")).append("\"></div>");
							}else{
								result.append("<div class=\"headerIconOk\"></div>");						
							}
							result.append("</div>");
							result.append("</div>");
							result.append("\r\n");	
						}
					}else{
						if (jsonObject.has("selected") && "true".equals(String.valueOf(jsonObject.get("selected")))){
							isTopSelected = true;
						}	
						result.append("<div class=\"headerContainer\"><div id=\"accordionHeader").append(i).append("\" class=\"AccordionTitle0\" onselectstart=\"return false;\"><a href=\"").append(itemURL).append("\" target=\"").append(itemTarget).append("\">").append(text).append("</a>");
						if (jsonObject.has("remark")){
							result.append("<div class=\"").append(jsonObject.get("remark")).append("\"></div>");
						}else{
							result.append("<div class=\"headerIconOk\"></div>");						
						}
						result.append("</div>");
						result.append("</div>");
						result.append("\r\n");						
					}
				}
				else if ("link".equals(type)){
					if (jsonObject.has("selected") && "true".equals(String.valueOf(jsonObject.get("selected")))){
						isTopSelected = true;
					}
					String itemURL = jsonObject.getString("href");
					String itemTarget = jsonObject.getString("target");
					result.append("<div class=\"headerContainer\"><div id=\"accordionHeader").append(i).append("\" class=\"AccordionTitle0\" onselectstart=\"return false;\"><a href=\"").append(itemURL).append("\" target=\"").append(itemTarget).append("\">").append(text).append("</a>");
					if (jsonObject.has("remark")){
						result.append("<div class=\"").append(jsonObject.get("remark")).append("\"></div>");
					}else{
						result.append("<div class=\"headerIconOk\"></div>");						
					}
					result.append("</div>");
					result.append("</div>");
					result.append("\r\n");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return result.toString();
	}

	void loopMenuSyntax(StringBuffer syntaxBuffer,JSONObject menuJsonObject){
		try{
			JSONArray jsonArray =  (JSONArray)menuJsonObject.get("menus");
			for (int i=0;i < jsonArray.length();i++){
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String type = jsonObject.getString("type");
				String text = jsonObject.getString("text");
				if ("folder".equals(type)){
					selectedIndex++;
					syntaxBuffer.append("<li id=\"").append(selectedIndex).append("\"><span>").append(text).append("</span>");
					syntaxBuffer.append("<ul>");
					syntaxBuffer.append("\r\n");
					loopMenuSyntax(syntaxBuffer, jsonObject);
					syntaxBuffer.append("</ul>");
					syntaxBuffer.append("\r\n");
					syntaxBuffer.append("</li>");
				}
				else if ("page".equals(type)){
					selectedIndex++;
					String itemURL = jsonObject.getString("href");
					String itemTarget = jsonObject.getString("target");

					if (jsonObject.has("selected") && "true".equals(String.valueOf(jsonObject.get("selected")))){
						selectIndexKeys.put(topSelectedIndex, selectedIndex);
						this.session.setAttribute(LastSelectIndexKeysSessionKey, selectIndexKeys);
					}
					syntaxBuffer.append("<li id=\"").append(selectedIndex).append("\"><span><a href=\"").append(itemURL).append("\" target=\"").append(itemTarget).append("\">").append(text).append("</a></span>");
					syntaxBuffer.append("\r\n");
					syntaxBuffer.append("</li>");
				}
				else if ("link".equals(type)){
					selectedIndex++;
					if (jsonObject.has("selected") && "true".equals(String.valueOf(jsonObject.get("selected")))){
						selectIndexKeys.put(topSelectedIndex, selectedIndex);
						this.session.setAttribute(LastSelectIndexKeysSessionKey, selectIndexKeys);
					}
					String itemURL = jsonObject.getString("href");
					String itemTarget = jsonObject.getString("target");
					syntaxBuffer.append("<li id=\"").append(selectedIndex).append("\"><span><a href=\"").append(itemURL).append("\" target=\"").append(itemTarget).append("\">").append(text).append("</a></span>");
					syntaxBuffer.append("\r\n");
					syntaxBuffer.append("</li>");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getTopSelectedIndex() {
		return topSelectedIndex;
	}

	public boolean isTopSelected() {
		return isTopSelected;
	}

	public Integer getSelectedIndex() {
		return selectedIndex;
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, Integer> getSelectIndexKeys() {
		if (selectIndexKeys.isEmpty()){
			HashMap<String,Integer> tempSelectIndexKeys = (HashMap<String,Integer>)this.session.getAttribute(LastSelectIndexKeysSessionKey);
			if (tempSelectIndexKeys != null){
				return tempSelectIndexKeys;
			}else{
				return selectIndexKeys;
			}
		}else{
			return selectIndexKeys;			
		}
	}

	public ArrayList<String> getTreeMenuIdList() {
		return treeMenuIdList;
	}
	public String getHomePageURL() {
		return homePageURL;
	}
}
