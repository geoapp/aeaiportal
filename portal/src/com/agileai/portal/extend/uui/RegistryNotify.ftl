<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>数通畅联-用户注册</title>
</head>

<body>
<p>
您好，${model.userName}！您在数通畅联网站已注册用户成功，请点击下面的链接激活用户状态！
<br />
<br />
激活链接：<a href="${model.activeLink}">${model.activeLink}</a>
</p>
<p>
--------------------------------------------------------------------------------------------<br />
  沈阳数通畅联软件技术有限公司  http://www.agileai.com<br />
  沈阳市沈河区大西路43号怀远商务大厦1单元11层   邮编：110011<br />
Phone：024 - 22962011<br />
E-mail：service@agileai.com<br />
--------------------------------------------------------------------------------------------
</p>
</body>
</html>