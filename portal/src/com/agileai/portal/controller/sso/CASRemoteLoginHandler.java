package com.agileai.portal.controller.sso;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.CryptionUtil;
import com.agileai.util.StringUtil;

public class CASRemoteLoginHandler extends BaseHandler{
	
	public ViewRenderer prepareDisplay(DataParam param){
		String comeFromKey = param.get("comeFromKey");
		String username = param.get("username");
		String password = param.get("password");
		String service = param.get("service");
		
		if (StringUtil.isNotNullNotEmpty(comeFromKey)
				&& StringUtil.isNotNullNotEmpty(username) && StringUtil.isNotNullNotEmpty(password)
				&&  StringUtil.isNotNullNotEmpty(password)){
			String secretKey = buildSecretKey(username);
			String encPassword = CryptionUtil.encryption(password, secretKey);
			this.setAttribute("comeFromKey", comeFromKey);
			this.setAttribute("username", username);
			this.setAttribute("password", encPassword);
			this.setAttribute("service", service);
		}else{
			this.setErrorMsg("comeFromKey or username or password or service cannt be empty !!");
		}
		return new LocalRenderer(this.getPage());
	}
	
	private String buildSecretKey(String username){
		String result = null;
		if (username.length() >= 8){
			result = username.substring(0,8);
		}else{
			result = (username+"xxxxxxxx").substring(0,8);
		}
		return result;
	}
}