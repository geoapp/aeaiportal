package com.agileai.portal.controller.wcm;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.controller.core.TreeAndContentManageListHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.portal.bizmoduler.wcm.WcmPictureGroup8ContentManage;

public class PictureBrowserHandler
        extends TreeAndContentManageListHandler {
	
    public PictureBrowserHandler() {
        super();
        this.serviceId = buildServiceId(WcmPictureGroup8ContentManage.class);
        this.rootColumnId = "00000000-0000-0000-00000000000000000";
        this.defaultTabId = "WcmPictureResource";
        this.columnIdField = "GRP_ID";
        this.columnNameField = "GRP_NAME";
        this.columnParentIdField = "GRP_PID";
        this.columnSortField = "GRP_ORDERNO";
    }

    @SuppressWarnings("unchecked")
	protected void processPageAttributes(DataParam param) {
        String tabId = param.get(TreeAndContentManage.TAB_ID, this.defaultTabId);
        if ("WcmPictureResource".equals(tabId)) {
        }
        StringBuffer temp = new StringBuffer();
        List<DataRow> records = this.getRsList();
        for (int i=0;i < records.size();i++){
        	DataRow row = records.get(i);
        	String resId = row.stringValue("RES_ID");
        	String resName = row.stringValue("RES_NAME");
        	String resWidth = row.stringValue("RES_WIDTH");
        	String resHeight = row.stringValue("RES_HEIGHT");
        	String resSize = row.stringValue("RES_SIZE");

        	JSONObject jsonObject = new JSONObject();
        	try {
				jsonObject.put("name", resName);
	        	jsonObject.put("width", resWidth);
	        	jsonObject.put("height", resHeight);
	        	jsonObject.put("size", resSize);
			} catch (JSONException e) {
				e.printStackTrace();
			}
        	
        	String imageThumbSrc = row.stringValue("RES_THUMB_LOCATION");
        	temp.append("<li>").append("<img src='").append(imageThumbSrc).append("' onclick='selectPicture(this,\"").append(resId).append("\")' ondblclick='showPicture(\"").append(resId).append("\")' ");
        	temp.append("ref='targetBox' options='").append(jsonObject.toString()).append("' /></li>");
        }
        this.setAttribute("imageItemsContent", temp.toString());
        this.setAttribute("recordsCount", String.valueOf(records.size()));
    }

    protected void initParameters(DataParam param) {
        String tabId = param.get(TreeAndContentManage.TAB_ID, this.defaultTabId);

        if ("WcmPictureResource".equals(tabId)) {
            initParamItem(param, "resName", "");
            initParamItem(param, "suffix", "");
        }
    }
    
    protected TreeBuilder provideTreeBuilder(DataParam param) {
        WcmPictureGroup8ContentManage service = this.getService();
        List<DataRow> menuRecords = service.findTreeRecords(new DataParam());
        TreeBuilder treeBuilder = new TreeBuilder(menuRecords,
                                                  this.columnIdField,
                                                  this.columnNameField,
                                                  this.columnParentIdField);
        return treeBuilder;
    }

    protected List<String> getTabList() {
        List<String> result = new ArrayList<String>();
        result.add("WcmPictureResource");
        return result;
    }

    protected WcmPictureGroup8ContentManage getService() {
        return (WcmPictureGroup8ContentManage) this.lookupService(this.getServiceId());
    }
}
