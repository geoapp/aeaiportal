package com.agileai.portal.controller.wcm;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.portal.bizmoduler.ColumnTreeManage;

public class ColumnParentSelectHandler
        extends TreeSelectHandler {
    public ColumnParentSelectHandler() {
        super();
        this.serviceId = buildServiceId(ColumnTreeManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "COL_ID",
                                                  "COL_NAME", "COL_PID");

        String excludeId = param.get("COL_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

    protected ColumnTreeManage getService() {
        return (ColumnTreeManage) this.lookupService(this.getServiceId());
    }
}
