package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.portal.bizmoduler.base.PtDecoratorManage;

public class PtDecoratorManageEditHandler extends StandardEditHandler{
	public PtDecoratorManageEditHandler(){
		super();
		this.listHandlerClass = PtDecoratorManageListHandler.class;
		this.serviceId = buildServiceId(PtDecoratorManage.class);
	}
	protected void processPageAttributes(DataParam param) {
		this.setAttribute("HEADER_HEIGHT",this.getAttribute("HEADER_HEIGHT","0"));
	}
	protected PtDecoratorManage getService() {
		return (PtDecoratorManage)this.lookupService(this.getServiceId());
	}
}
