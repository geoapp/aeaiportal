package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PortletAppPickSelect;
import com.agileai.util.StringUtil;

public class PortletAppPickSelectListHandler extends PickFillModelHandler{
	public PortletAppPickSelectListHandler(){
		super();
		this.serviceId = buildServiceId(PortletAppPickSelect.class);
	}
	protected void processPageAttributes(DataParam param) {
		setAttribute("paGroup",FormSelectFactory.create("PA_GROUP").addSelectedValue(param.get("paGroup")));
		setAttribute("paType",FormSelectFactory.create("PA_TYPE").addSelectedValue(param.get("paType")));
		setAttribute("paContext",FormSelectFactory.create("PA_CONTEXT").addSelectedValue(param.get("paContext")));
		initMappingItem("PA_GROUP",FormSelectFactory.create("PA_GROUP").getContent());
		initMappingItem("PA_TYPE",FormSelectFactory.create("PA_TYPE").getContent());
		initMappingItem("PA_CONTEXT",FormSelectFactory.create("PA_CONTEXT").getContent());	
	}
	protected void initParameters(DataParam param) {
		String paGroup = param.get("paGroup");
		if (StringUtil.isNullOrEmpty(paGroup)){
			paGroup = (String)this.getSessionAttribute("last_paGroup");
			if (paGroup == null){
				paGroup = "dataview";
				this.getSessionAttributes().put("last_paGroup",paGroup);
			}
		}else{
			this.getSessionAttributes().put("last_paGroup",paGroup);
		}
		initParamItem(param,"paTitle","");
		initParamItem(param,"paGroup",paGroup);
		initParamItem(param,"paType","");
		initParamItem(param,"paContext","");
	}
	protected PortletAppPickSelect getService() {
		return (PortletAppPickSelect)this.lookupService(this.getServiceId());
	}
}	
