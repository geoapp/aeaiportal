package com.agileai.portal.controller.base;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.driver.model.Theme;
import com.agileai.util.StringUtil;

public class ThemeLayoutConfigHandler extends BaseHandler{
	
	public ThemeLayoutConfigHandler(){
		super();
	}
	@PageAction
	public ViewRenderer setWindowSize(DataParam param){
		String rspText = FAIL;
		HttpSession session = this.request.getSession();
		if (StringUtil.isNotNullNotEmpty(param.get(Theme.WindowScreenWidth))){
			String windowScreenWidth = param.get(Theme.WindowScreenWidth);
			session.setAttribute(Theme.WindowScreenWidth,windowScreenWidth);
			this.writeCookieWidth(response, windowScreenWidth);
		}
		if (StringUtil.isNotNullNotEmpty(param.get(Theme.WindowScreenHeight))){
			String windowScreenHeight = param.get(Theme.WindowScreenHeight);
			session.setAttribute(Theme.WindowScreenHeight,windowScreenHeight);
			this.writeCookieHeight(response, windowScreenHeight);
		}
		return new AjaxRenderer(rspText);
	}
	
	private void writeCookieWidth(HttpServletResponse response,String windowScreenWidth){
		Cookie widthCookie = new Cookie(Theme.WindowScreenWidth, windowScreenWidth);
		widthCookie.setMaxAge(-1);		//设为负值，在浏览器内存中保存，关闭浏览器，cookie失效
		widthCookie.setPath("/");
		response.addCookie(widthCookie);
	}
	
	private void writeCookieHeight(HttpServletResponse response,String windowScreenHeight){
		Cookie heightCookie = new Cookie(Theme.WindowScreenHeight, windowScreenHeight);
		heightCookie.setMaxAge(-1);		//设为负值，在浏览器内存中保存，关闭浏览器，cookie失效
		heightCookie.setPath("/");
		response.addCookie(heightCookie);
	}	
}