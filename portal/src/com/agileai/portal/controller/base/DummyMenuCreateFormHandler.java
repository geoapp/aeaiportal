package com.agileai.portal.controller.base;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.base.MenuBarManage;
import com.agileai.portal.bizmoduler.base.NavigaterManage;
import com.agileai.portal.bizmoduler.base.PtThemeManage;
import com.agileai.portal.driver.model.MenuBar;
import com.agileai.portal.driver.model.MenuItem;
import com.agileai.portal.driver.model.Theme;
import com.agileai.util.StringUtil;

public class DummyMenuCreateFormHandler extends BaseHandler {
	public DummyMenuCreateFormHandler(){
		this.serviceId = buildServiceId(MenuBarManage.class);
	}
	public ViewRenderer prepareDisplay(DataParam param){
		this.setAttributes(param);
		
		String navId = param.get("NAV_ID");
		NavigaterManage navigaterManage = this.lookupService(NavigaterManage.class);
		DataParam queryParam = new DataParam("NAV_ID",navId);
		DataRow row = navigaterManage.getRecord(queryParam);
		String naviterType = row.stringValue("NAV_TYPE");
		FormSelect themeSelect = new FormSelect();
		themeSelect.setKeyColumnName("THEME_ID");
		themeSelect.setValueColumnName("THEME_NAME");
		PtThemeManage themeManage = this.lookupService(PtThemeManage.class);
		if (MenuBar.Type.LOGIN_BEFORE.equals(naviterType)){
			queryParam = new DataParam("themeType",MenuBar.Type.LOGIN_BEFORE);
		}else if (MenuBar.Type.LOGIN_AFTER.equals(naviterType)){
			queryParam = new DataParam("themeType",MenuBar.Type.LOGIN_AFTER);
		}
		List<DataRow> records = themeManage.findRecords(queryParam);
		themeSelect.putValue(Theme.EXTEND_THEME_ID, Theme.EXTEND_THEME_NAME);
		themeSelect.putValues(records);
		
		this.setAttribute("MENU_THEME", themeSelect.addSelectedValue(this.getAttributeValue("MENU_THEME")).addHasBlankValue(false));
		
		String visiable = this.getAttributeValue("MENU_VISIABLE");
		if (StringUtil.isNullOrEmpty(visiable)){
			visiable = MenuItem.Visiable.visable;
		}
		FormSelect visiableSelect = FormSelectFactory.create("MENU_VISIABLE");
		this.setAttribute("MENU_VISIABLE", visiableSelect.addSelectedValue(visiable));
		return new LocalRenderer(this.getPage());
	}
	
	public ViewRenderer doSaveAction(DataParam param){
		String navId = param.get("NAV_ID");
		String menuCode = param.get("CHILD_MENU_CODE");
		boolean isCodeDuplicate = getService().isCodeDuplicate(navId,"", menuCode);
		if (isCodeDuplicate){
			return new AjaxRenderer("duplicate");
		}else{
			getService().insertChildRecord(param);
			String newMenuId = param.get("MENU_ID");
			return new AjaxRenderer(newMenuId);			
		}
	}
	
	protected MenuBarManage getService(){
		return (MenuBarManage)this.lookupService(serviceId);
	}	
}
