package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PtPageTemptManage;

public class PtPageTemptManageListHandler
        extends StandardListHandler {
    public PtPageTemptManageListHandler() {
        super();
        this.editHandlerClazz = PtPageTemptManageEditHandler.class;
        this.serviceId = buildServiceId(PtPageTemptManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("temptGrp",
                     FormSelectFactory.create("PAGE_TEMPT_GRP")
                                      .addSelectedValue(param.get("temptGrp")));
        initMappingItem("TEMPT_GRP",
                        FormSelectFactory.create("PAGE_TEMPT_GRP").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "temptCode", "");
        initParamItem(param, "temptGrp", "");
    }

    protected PtPageTemptManage getService() {
        return (PtPageTemptManage) this.lookupService(this.getServiceId());
    }
}
