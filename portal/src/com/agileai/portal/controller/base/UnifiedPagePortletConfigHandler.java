package com.agileai.portal.controller.base;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.base.PtDecoratorManage;
import com.agileai.portal.driver.model.Theme;
import com.agileai.portal.driver.service.PortalConfigService;
import com.agileai.portal.driver.util.DecoratorUtil;
import com.agileai.util.StringUtil;

public class UnifiedPagePortletConfigHandler extends BaseHandler{
	public static final String PORTLET_DECORATOR = "portletDecorator";
	
	public UnifiedPagePortletConfigHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		this.setAttributes(param);
		
		String portletDecorator = param.get(PORTLET_DECORATOR);
		PtDecoratorManage decortorManage = this.lookupService(PtDecoratorManage.class);
		
		String themeId = param.get("themeId");
		PortalConfigService portalConfigService = (PortalConfigService)this.lookupService("PortalConfigService");
		Theme theme = portalConfigService.getTheme(themeId);
		if (StringUtil.isNullOrEmpty(portletDecorator)){
			portletDecorator = theme.getDecoratorId();
		}
		
		DataParam queryParam = new DataParam();
		DecoratorUtil decoratorUtil = new DecoratorUtil(this.request.getServletContext());
		List<String> unifiedDecoratorIdList = decoratorUtil.getUnifiedDecoratorIdList();
		List<DataRow> records = decortorManage.findRecords(queryParam);
		List<DataRow> tempRecords = new ArrayList<DataRow>();
		for (int i=0;i < records.size();i++){
			DataRow row = records.get(i);
			String decoratorId = row.getString("DECOR_ID");
			if (unifiedDecoratorIdList.contains(decoratorId)){
				tempRecords.add(row);
			}
		}
		if (StringUtil.isNullOrEmpty(portletDecorator)){
			DataRow row = tempRecords.get(0);
			portletDecorator = row.stringValue("DECOR_ID");
		}
		
		FormSelect decoratorSelect = new FormSelect();
		decoratorSelect.setKeyColumnName("DECOR_ID");
		decoratorSelect.setValueColumnName("DECOR_NAME");
		decoratorSelect.putValues(tempRecords);
		decoratorSelect.setSelectedValue(portletDecorator);
		
		this.setAttribute(PORTLET_DECORATOR,decoratorSelect);
		
		if (StringUtil.isNotNullNotEmpty(param.get(PORTLET_DECORATOR))){
			portletDecorator = param.get(PORTLET_DECORATOR);
		}
		
		List<String> decoratorPropertyList = decoratorUtil.getDecoratorUnifiedPropertyList(portletDecorator);
		for (int i=0;i < decoratorPropertyList.size();i++){
			String key = decoratorPropertyList.get(i);
			String value = theme.getPropertyValue(key);
			this.setAttribute(key, value);
		}
		
		String jspPage = decoratorUtil.getDecoratorUnifiedEditPage(portletDecorator);
		return new LocalRenderer(jspPage);
	}
	
	public ViewRenderer doSaveAction(DataParam param){
		String themeId = param.get("themeId");
		PortalConfigService portalConfigService = (PortalConfigService)this.lookupService("PortalConfigService");
		
		String portletDecorator = param.get(PORTLET_DECORATOR);
		Theme theme = portalConfigService.getTheme(themeId);
		theme.setDecoratorId(portletDecorator);
		
		DecoratorUtil decoratorUtil = new DecoratorUtil(this.request.getServletContext());
		List<String> decoratorPropertyList = decoratorUtil.getDecoratorUnifiedPropertyList(portletDecorator);
		for (int i=0;i < decoratorPropertyList.size();i++){
			String key = decoratorPropertyList.get(i);
			String value = param.get(key);
			theme.setPropertyValue(key, value);
		}
		String rspText = FAIL;
		if (!StringUtil.isNullOrEmpty(themeId)){
			portalConfigService.saveThemeDecorator(theme);
			portalConfigService.refreshTheme(themeId);
			rspText = "OK";
		}
		return new AjaxRenderer(rspText);
	}
}