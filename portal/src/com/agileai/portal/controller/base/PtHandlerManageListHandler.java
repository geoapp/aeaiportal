package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PtHandlerManage;

public class PtHandlerManageListHandler
        extends StandardListHandler {
    public PtHandlerManageListHandler() {
        super();
        this.editHandlerClazz = PtHandlerManageEditHandler.class;
        this.serviceId = buildServiceId(PtHandlerManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("accessType",
                     FormSelectFactory.create("ACCESS_TYPE")
                                      .addSelectedValue(param.get("accessType")));
        initMappingItem("ACCESS_TYPE",
                        FormSelectFactory.create("ACCESS_TYPE").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "handlerCode", "");
        initParamItem(param, "accessType", "");
    }

    protected PtHandlerManage getService() {
        return (PtHandlerManage) this.lookupService(this.getServiceId());
    }
}
