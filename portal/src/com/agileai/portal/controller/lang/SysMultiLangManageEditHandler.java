package com.agileai.portal.controller.lang;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.lang.SysMultiLangManage;

public class SysMultiLangManageEditHandler
        extends StandardEditHandler {

	private static final String MULTI_LANG_TYPE="MULTI_LANG_TYPE";
	
	public SysMultiLangManageEditHandler() {
        super();
        this.serviceId = buildServiceId(SysMultiLangManage.class);
    }
	
	@Override
	public ViewRenderer doSaveAction(DataParam param) {
		String resourceType = param.get("resourceType");
		param.buildParam("state_");
		List<DataParam> updateList = param.getUpdateParam();
		List<DataParam> insertList = param.getInsertParam();
		if (updateList.size() > 0 || insertList.size() > 0){
			this.getService().saveLangConf(resourceType,param, insertList, updateList);			
		}
		return new AjaxRenderer("");
	}
	
	@Override
	public ViewRenderer prepareDisplay(DataParam param) {
//		String resourceType = param.get("resourceType");
//		String resourceId = param.get("resourceId");
//		String resourceName = param.get("resourceName");
		List<DataRow> sysLangList = getService().findRecords(param);
		
		StandardService codeListService = (StandardService) this.lookupService("codeListService");
		param.put("TYPE_ID", MULTI_LANG_TYPE);
		List<DataRow> codeList = codeListService.findRecords(param);
		
		//构建map，每条记录的“语言编码”作为key，每条记录作为value
		Map<String, Object> sysLangMap = buildMap(sysLangList);
		//构建目标list
		List<DataRow> desList = new ArrayList<DataRow>();
		fillDesList(desList, sysLangMap, codeList, param);
		
		this.setAttribute("desList", desList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

	private void fillDesList(List<DataRow> desList, Map<String, Object> sysLangMap, List<DataRow> codeList, DataParam param) {
		String sResourceType = param.stringValue("resourceType");
		String sResourceId = param.stringValue("resourceId");
		for (int i = 0; i < codeList.size(); i++) {
			DataRow codeListRow = codeList.get(i);
			String codeId = codeListRow.stringValue("CODE_ID");
			String codeName = codeListRow.stringValue("CODE_NAME");
			//如果 编码表SYS_CODE_LIST 当前语言编码 和 多语言表SYS_MUTI_LANG 当前语言编码 相同
			if(sysLangMap.containsKey(codeId)){
				DataRow sysLangRow = (DataRow) sysLangMap.get(codeId);
				String langValue = sysLangRow.stringValue("MULTI_LANG_VALUE");
				String sysLangId = sysLangRow.stringValue("MULTI_LANG_ID");
				DataRow desRow = new DataRow();
				desRow.put("MULTI_LANG_RESOURCE_TYPE", sResourceType).put("MULTI_LANG_RESOURCE_ID", sResourceId)
				.put("_state", "update")
				.put("MULTI_LANG_ID", sysLangId)
				.put("MULTI_LANG_CODE", codeId)
				.put("MULTI_LANG_NAME", codeName)
				.put("MULTI_LANG_VALUE", langValue);
				desList.add(desRow);
				continue;
			}
			//如果 编码表SYS_CODE_LIST 当前语言编码 和 多语言表SYS_MUTI_LANG 当前语言编码 不相同
			DataRow desRow = new DataRow();
			desRow.put("MULTI_LANG_RESOURCE_TYPE", sResourceType).put("MULTI_LANG_RESOURCE_ID", sResourceId)
			.put("_state", "insert")
			.put("MULTI_LANG_CODE", codeId)
			.put("MULTI_LANG_NAME", codeName)
			.put("MULTI_LANG_VALUE", "");
			desList.add(desRow);
		}
	}

	private Map<String, Object> buildMap(List<DataRow> sysLangList) {
		Map<String,Object> sysLangMap = new HashMap<String, Object>();
		for (int i = 0; i < sysLangList.size(); i++) {
			DataRow sysLangRow = sysLangList.get(i);
			String langCode = sysLangRow.stringValue("MULTI_LANG_CODE");
			sysLangMap.put(langCode, sysLangRow);
		}
		return sysLangMap;
	}

    protected void processPageAttributes(DataParam param) {
    	mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
    }

    protected SysMultiLangManage getService() {
        return (SysMultiLangManage) this.lookupService(this.getServiceId());
    }
}
