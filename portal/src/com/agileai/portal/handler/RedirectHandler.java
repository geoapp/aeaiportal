package com.agileai.portal.handler;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.Role;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class RedirectHandler extends BaseHandler {
	
	public ViewRenderer prepareDisplay(DataParam param) {
		User user = (User) getUser();
		List<Role> roles =  user.getRoleList();
		for(int i=0;i<roles.size();i++){
			Role userRoles = roles.get(i);
			String userRole = userRoles.getRoleCode();
			if("agileai".equals(userRole)){
				return new RedirectRenderer("/portal/request/03/Workbench.ptml");
			}else if("RigistryUser".equals(userRole)){
				return new RedirectRenderer("/portal/request/04/Workbench.ptml");
			}else if("guest".equals(user.getUserCode())){
				return new RedirectRenderer("/portal/request/04/Workbench.ptml");
			}
		}
		return new RedirectRenderer("/portal/request/03/Workbench.ptml");
	}
}
