package com.agileai.portal.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.MenuItemPersonalManage;
import com.agileai.portal.bizmoduler.auth.SecurityGroupManage;
import com.agileai.portal.driver.AttributeKeys;
import com.agileai.portal.driver.common.UserPersonalHelper;
import com.agileai.portal.driver.model.LayoutContainer;
import com.agileai.portal.driver.model.MenuBar;
import com.agileai.portal.driver.model.MenuItem;
import com.agileai.portal.driver.model.Page;
import com.agileai.portal.driver.model.PersonalFavorite;
import com.agileai.portal.driver.model.PersonalSetting;
import com.agileai.portal.driver.model.PortletVariable;
import com.agileai.portal.driver.model.PortletWindowConfig;
import com.agileai.portal.driver.service.DriverConfiguration;
import com.agileai.portal.driver.service.PortalConfigService;
import com.agileai.portal.driver.servlet.ParamInteractiveServlet;
import com.agileai.portal.filter.UserCacheManager;
import com.agileai.util.StringUtil;

public class PortalHandler extends BaseHandler {
	private static final String NEW_LINE = "\r\n";
	
	public ViewRenderer prepareDisplay(DataParam param) {
		return new LocalRenderer(getPage());
	}
	
	@PageAction
	public ViewRenderer triggleWindow(DataParam param){
		ViewRenderer result = null;
		String windowState = (String)this.getSessionAttribute(AttributeKeys.WINDOW_STATE_KEY);
		String toState = null;
		if ("normal".equals(windowState)){
			toState = "maxed";
			this.putSessionAttribute(AttributeKeys.WINDOW_STATE_KEY, toState);
			result = new AjaxRenderer(toState);
		}else{
			toState = "normal";
			this.putSessionAttribute(AttributeKeys.WINDOW_STATE_KEY, toState);
			result = new AjaxRenderer(toState);			
		}
		return result;
	}
	
	@PageAction
	public ViewRenderer kickout(DataParam param){
		String responseText = No;
		User user = (User)this.getUser();
		String loginTimeParam = param.get("loginTime");
		Long loginTime = (Long)user.getTempProperties().get("loginTime");
		if (Long.parseLong(loginTimeParam) > loginTime){
			responseText = Yes;
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer changeMode(DataParam param){
		ViewRenderer result = null;
		String windowMode = (String)this.getSessionAttribute(AttributeKeys.WINDOW_MODE_KEY);
		String toMode = null;
		if ("view".equals(windowMode)){
			toMode = "edit";
			this.removeSessionAttribute(AttributeKeys.PERSONAL_SETTING_MODE_KEY);
			this.putSessionAttribute(AttributeKeys.WINDOW_MODE_KEY, toMode);
			result = new AjaxRenderer(toMode);
		} else {
			toMode = "view";
			this.removeSessionAttribute(AttributeKeys.PERSONAL_SETTING_MODE_KEY);
			this.putSessionAttribute(AttributeKeys.WINDOW_MODE_KEY, toMode);
			result = new AjaxRenderer(toMode);			
		}
		return result;
	}
	@PageAction
	public ViewRenderer retrievePageName(DataParam param){
		String targetPage = param.get("targetPage");
		String[] tempArray = targetPage.split("/");
		String menuBarId = tempArray[0];
		String menuItemCode = tempArray[1];
		if (menuItemCode.endsWith(".ptml")){
			menuItemCode = menuItemCode.substring(0,menuItemCode.length()-5);
		}
		MenuBar menuBar = getPortalConfigService().getMenuBar(menuBarId);
		MenuItem menuItem = menuBar.getMenuItemCodeMap().get(menuItemCode);
		String title = menuItem.getName();
		StringBuffer responseText = new StringBuffer();
		responseText.append("{\"title\":\"").append(title).append("\"}");
		return new AjaxRenderer(responseText.toString());
	}
	
	@PageAction
	public ViewRenderer copySessionParams(DataParam param){
		String currentURL = param.get("currentURL");
		HashMap<String,PortletVariable> currentParamMap = ParamInteractiveServlet.getParamMap(request, currentURL);
		String targetURL = param.get("targetURL");
		if (targetURL.startsWith("/portal/")){
			targetURL = targetURL.substring(16,targetURL.length());
		}
		HashMap<String,PortletVariable> targetParamMap = ParamInteractiveServlet.getParamMap(request, targetURL);
		targetParamMap.putAll(currentParamMap);
		
		return new AjaxRenderer(SUCCESS);
	}
	
	@PageAction
	public ViewRenderer isExistFavoritePage(DataParam param){
		ViewRenderer result = null;
		String menuBarId = param.get("menuBarId");
		String menuItemId = param.get("menuItemId");
		String currentMenuId = param.get("currentMenuId");
		User user = (User)this.getUser();
		
		PersonalFavorite personalFavorite = (PersonalFavorite)user.getExtendProperties().get(AttributeKeys.PERSONAL_FAVORITE_OBJECT_KEY);
		List<String> favoritePages = personalFavorite.getFavoritePages(menuBarId);
		if (StringUtil.isNotNullNotEmpty(currentMenuId)){
			if (favoritePages.contains(menuBarId+"/"+menuItemId+"/"+currentMenuId)){
				result = new AjaxRenderer("Y");
			}else{
				result = new AjaxRenderer("N");
			}
		}else{
			if (favoritePages.contains(menuBarId+"/"+menuItemId)){
				result = new AjaxRenderer("Y");
			}else{
				result = new AjaxRenderer("N");
			}			
		}
		return result;
	}
	
	@PageAction
	public ViewRenderer addFavoritePage(DataParam param){
		ViewRenderer result = null;
		String responseText = null;
		String menuBarId = param.get("menuBarId");
		String menuItemId = param.get("menuItemId");
		String currentMenuId = param.get("currentMenuId");
		User user = (User)this.getUser();
		String userCode = user.getUserCode();
		SecurityGroupManage securityGroupManage = this.lookupService(SecurityGroupManage.class);
		PersonalFavorite personalFavorite = (PersonalFavorite)user.getExtendProperties().get(AttributeKeys.PERSONAL_FAVORITE_OBJECT_KEY);
		
		if (StringUtil.isNotNullNotEmpty(currentMenuId)){
			personalFavorite.getFavoritePages(menuBarId).add(menuBarId+"/"+menuItemId+"/"+currentMenuId);
		}else{
			personalFavorite.getFavoritePages(menuBarId).add(menuBarId+"/"+menuItemId);			
		}
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonString = objectMapper.writeValueAsString(personalFavorite);
			securityGroupManage.updateSecurityUserFavorites(userCode, jsonString);
			UserCacheManager userCacheManager = UserCacheManager.getOnly();
			userCacheManager.removeUser(userCode);
			responseText = this.buildFavoritePages(menuBarId,personalFavorite);			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		result = new AjaxRenderer(responseText);
		
		return result;
	}
	
	private String buildFavoritePages(String navId ,PersonalFavorite personalFavorite){
		StringBuffer buffer = new StringBuffer();
		List<String> favoritePages = personalFavorite.getFavoritePages(navId);
		for (int i=0;i < favoritePages.size();i++){
			String[] temp = favoritePages.get(i).split("/");
			String menuBarId = temp[0];
			String menuItemId = temp[1];
			String currentMenuId = null;
			if (temp.length > 2){
				currentMenuId = temp[2];
			}
			MenuBar menuBar = getPortalConfigService().getMenuBar(menuBarId);
			if (menuBar == null){
				continue;
			}
			MenuItem menuItem = menuBar.getMenuItemIdMap().get(menuItemId);
			if (menuItem == null){
				continue;
			}
			String title = menuItem.getName();
			String href = "/portal/request/"+menuBarId+"/"+menuItem.getCode()+".ptml";
			if (currentMenuId != null){
				href = href+"?currentMenuId="+currentMenuId;
				HttpSession session = request.getSession();
				HashMap<String,org.codehaus.jettison.json.JSONObject> menuJSONObjectMap = (HashMap<String,org.codehaus.jettison.json.JSONObject>)MenuBar.getMenuJSONObjectMap(session);
				org.codehaus.jettison.json.JSONObject jsonObject = menuJSONObjectMap.get(currentMenuId);
				try {
					title = jsonObject.getString("text");					
				} catch (Exception e) {
					this.log.error(e.getLocalizedMessage(), e);
				}
			}
			buffer.append("<li>").append("<a href='").append(href).append("'>").append(title).append("</a></li>").append(NEW_LINE);
		}
		return buffer.toString();
	}
	
	@PageAction
	public ViewRenderer retrieveFavoritePages(DataParam param){
		ViewRenderer result = null;
		User user = (User)this.getUser();
		String navId = param.get("navId");
		PersonalFavorite personalFavorite = (PersonalFavorite)user.getExtendProperties().get(AttributeKeys.PERSONAL_FAVORITE_OBJECT_KEY);
		String responseText = this.buildFavoritePages(navId,personalFavorite);
		result = new AjaxRenderer(responseText);
		return result;
	}
	
	@PageAction
	public ViewRenderer isFirst(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		String portletId = param.get("portletId");
		String columnIndex = param.get("columnIndex");
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		List<String> portletIdList = getPortletIdList(personalSetting, columnIndex);
		if (portletIdList.get(0).equals(portletId)){
			result = new AjaxRenderer("Y");
		}else{
			result = new AjaxRenderer("N");
		}
		return result;
	}
	
	@PageAction
	public ViewRenderer isLast(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		String portletId = param.get("portletId");
		String columnIndex = param.get("columnIndex");
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		List<String> portletIdList = getPortletIdList(personalSetting, columnIndex);
		if (portletIdList.get(portletIdList.size()-1).equals(portletId)){
			result = new AjaxRenderer("Y");
		}else{
			result = new AjaxRenderer("N");
		}
		return result;
	}	
	
	private PortalConfigService getPortalConfigService(){
		ServletContext servletContext = this.dispatchServlet.getServletContext();
		DriverConfiguration driverConfiguration = (DriverConfiguration)servletContext.getAttribute(AttributeKeys.DRIVER_CONFIG);
		return driverConfiguration.getPortalConfigService();
	}
	
	@PageAction
	public ViewRenderer startPersonalSetting(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		this.putSessionAttribute(AttributeKeys.PERSONAL_SETTING_MODE_KEY, "Y");
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		if (personalSetting == null){
			String navigaterId = param.get("navigaterId");
			MenuBar menuBar = getPortalConfigService().getMenuBar(navigaterId);
			MenuItem menuItem = menuBar.getMenuItemIdMap().get(menuItemId);
			String pageId = menuItem.getPageId();
			Page page = getPortalConfigService().getPage(pageId);
			
			personalSetting = new PersonalSetting();
			this.initPersonalSetting(personalSetting,page);
			HashMap<String,PersonalSetting> personalSettingMap = userPersonalHelper.getPersonalSettings();
			personalSettingMap.put(menuItemId,personalSetting);
			HashMap<String,String> personalSettingUseKeys = userPersonalHelper.getPersonalSettingUseKeys();
			personalSettingUseKeys.put(menuItemId,"Y");
		}else{
			HashMap<String,String> personalSettingUseKeys = userPersonalHelper.getPersonalSettingUseKeys();
			personalSettingUseKeys.put(menuItemId,"Y");
		}
		result = new AjaxRenderer(SUCCESS);
		return result;
	}
	
	private void initPersonalSetting(PersonalSetting personalSetting,Page page){
		List<LayoutContainer> layoutContainers = page.getLayoutContainers();
		
		if (layoutContainers.size() > 0 ){
			LayoutContainer layoutContainer = layoutContainers.get(0);
			int width = Integer.parseInt(layoutContainer.getWidth().substring(0,layoutContainer.getWidth().length()-1));
			personalSetting.getColumnWidthScales()[0] = width;
			List<PortletWindowConfig> portletWindowConfigs = layoutContainer.getPortlets();
			
			for (int i=0;i < portletWindowConfigs.size();i++){
				PortletWindowConfig portletWindowConfig = portletWindowConfigs.get(i);
				String portletId = portletWindowConfig.getId();
				personalSetting.getFirstColumnPortletIdList().add(portletId);
			}
		}
		
		if (layoutContainers.size() > 1 ){
			LayoutContainer layoutContainer = layoutContainers.get(1);
			int width = Integer.parseInt(layoutContainer.getWidth().substring(0,layoutContainer.getWidth().length()-1));
			personalSetting.getColumnWidthScales()[1] = width;
			List<PortletWindowConfig> portletWindowConfigs = layoutContainer.getPortlets();
			
			for (int i=0;i < portletWindowConfigs.size();i++){
				PortletWindowConfig portletWindowConfig = portletWindowConfigs.get(i);
				String portletId = portletWindowConfig.getId();
				personalSetting.getSecondColumnPortletIdList().add(portletId);
			}
		}
		
		if (layoutContainers.size() > 2 ){
			LayoutContainer layoutContainer = layoutContainers.get(2);
			int width = Integer.parseInt(layoutContainer.getWidth().substring(0,layoutContainer.getWidth().length()-1));
			personalSetting.getColumnWidthScales()[2] = width;
			List<PortletWindowConfig> portletWindowConfigs = layoutContainer.getPortlets();
			
			for (int i=0;i < portletWindowConfigs.size();i++){
				PortletWindowConfig portletWindowConfig = portletWindowConfigs.get(i);
				String portletId = portletWindowConfig.getId();
				personalSetting.getThirdColumnPortletIdList().add(portletId);
			}
		}else{
			personalSetting.getColumnWidthScales()[2] = 0;	
		}
	}
	
	@PageAction
	public ViewRenderer stopPersonalSetting(DataParam param){
		ViewRenderer result = null;
		this.removeSessionAttribute(AttributeKeys.PERSONAL_SETTING_MODE_KEY);
		String menuItemId = param.get("menuItemId");
		User user = (User)this.getUser();
		String userCode = user.getUserCode();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		MenuItemPersonalManage menuItemPersonalManage = this.getMenuItemPersonalManage();
		DataRow userRow = menuItemPersonalManage.getRecord(userCode, menuItemId);
		if (userRow != null){
			String personalSetting = userRow.stringValue("USER_PERSONAL");
			if (!StringUtil.isNullOrEmpty(personalSetting)){
				try {
					ObjectMapper objectMapper = new ObjectMapper();
					PersonalSetting personalSettingBean = objectMapper.readValue(personalSetting, PersonalSetting.class);
					HashMap<String,PersonalSetting> personalSettingMap = userPersonalHelper.getPersonalSettings();
					personalSettingMap.put(menuItemId, personalSettingBean);			
					
					HashMap<String,String> personalSettingUseKeys = userPersonalHelper.getPersonalSettingUseKeys();
					boolean usePersonal = "Y".equals(userRow.stringValue("USE_PERSONAL"));
					if (usePersonal){
						personalSettingUseKeys.put(menuItemId, "Y");
					}else{
						personalSettingUseKeys.put(menuItemId, "N");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}			
		}
		result = new AjaxRenderer(SUCCESS);
		return result;
	}
	
	@PageAction
	public ViewRenderer savePersonalSetting(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		
		MenuItemPersonalManage menuItemPersonalManage = this.getMenuItemPersonalManage();
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		String userCode = user.getUserCode();
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		menuItemPersonalManage.savePersonalSetting(userCode, menuItemId, personalSetting);
		menuItemPersonalManage.updateUsePersonal(userCode, menuItemId, true);
		
		UserCacheManager userCacheManager = UserCacheManager.getOnly();
		userCacheManager.removeUser(userCode);
		result = new AjaxRenderer(SUCCESS);
		return result;
	}
	
	@PageAction
	public ViewRenderer retrievePortletJson(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		String pageId = param.get("pageId");
		
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		
		List<String> portletIdList = this.getPortletIdList(personalSetting);
		StringBuffer buffer = new StringBuffer();
		buffer.append("<ul style='margin:0;padding:0;'>").append(NEW_LINE);
		PortalConfigService portalConfigService = this.getPortalConfigService();
		Page page = portalConfigService.getPage(pageId);
		HashMap<String, PortletWindowConfig> portletWindowConfigs = page.getPortletWindowConfigs();
		Iterator<String> keys = portletWindowConfigs.keySet().iterator();
		while (keys.hasNext()){
			String key = keys.next();
			if (portletIdList.contains(key)){
				continue;
			}
			String title = portletWindowConfigs.get(key).getTitle();
			buffer.append("<li class='drag-item' portletId='").append(key).append("'>").append(title).append("</li>").append(NEW_LINE);
		}
		
		buffer.append("</ul>");
		result = new AjaxRenderer(buffer.toString());
		return result;
	}
	
	private List<String> getPortletIdList(PersonalSetting personalSetting){
		List<String> result = new ArrayList<String>();
		result.addAll(personalSetting.getFirstColumnPortletIdList());
		result.addAll(personalSetting.getSecondColumnPortletIdList());
		result.addAll(personalSetting.getThirdColumnPortletIdList());
		return result;
	}
	
	@PageAction
	public ViewRenderer resetPersonalSetting(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		String userCode = user.getUserCode();
		MenuItemPersonalManage menuItemPersonalManage = this.getMenuItemPersonalManage();
		DataRow userRow = menuItemPersonalManage.getRecord(userCode, menuItemId);
		String personalSetting = userRow.stringValue("USER_PERSONAL");
		if (!StringUtil.isNullOrEmpty(personalSetting)){
			try {
				ObjectMapper objectMapper = new ObjectMapper();
				PersonalSetting personalSettingBean = objectMapper.readValue(personalSetting, PersonalSetting.class);
				userPersonalHelper.getPersonalSettings().put(menuItemId, personalSettingBean);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		result = new AjaxRenderer(SUCCESS);
		return result;
	}	
	
	private MenuItemPersonalManage getMenuItemPersonalManage(){
		return this.lookupService(MenuItemPersonalManage.class);
	}
	
	@PageAction
	public ViewRenderer cleanPersonalSetting(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		String userCode = user.getUserCode();
		MenuItemPersonalManage menuItemPersonalManage = this.getMenuItemPersonalManage();

		menuItemPersonalManage.cleanPersonalSetting(userCode, menuItemId);
		
		userPersonalHelper.getPersonalSettings().remove(menuItemId);
		userPersonalHelper.getPersonalSettingUseKeys().remove(menuItemId);
		this.removeSessionAttribute(AttributeKeys.PERSONAL_SETTING_MODE_KEY);
		
		UserCacheManager userCacheManager = UserCacheManager.getOnly();
		userCacheManager.removeUser(userCode);
		
		result = new AjaxRenderer(SUCCESS);
		return result;
	}
	
	@PageAction
	public ViewRenderer applyPersonalSetting(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		
		User user = (User)this.getUser();
		String userCode = user.getUserCode();
		String usePersonal = "Y";
		
		MenuItemPersonalManage menuItemPersonalManage = this.getMenuItemPersonalManage();
		menuItemPersonalManage.updateUsePersonal(userCode, menuItemId, true);

		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		HashMap<String,String> personalSettingUseKeyMap = userPersonalHelper.getPersonalSettingUseKeys();
		personalSettingUseKeyMap.put(menuItemId, usePersonal);
		
		UserCacheManager userCacheManager = UserCacheManager.getOnly();
		userCacheManager.removeUser(userCode);
		
		result = new AjaxRenderer(SUCCESS);
		return result;
	}
	
	@PageAction
	public ViewRenderer cancelPersonalSetting(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		
		result = new AjaxRenderer(SUCCESS);
		User user = (User)this.getUser();
		String userCode = user.getUserCode();
		String usePersonal = "N";
		MenuItemPersonalManage menuItemPersonalManage = this.getMenuItemPersonalManage();
		menuItemPersonalManage.updateUsePersonal(userCode, menuItemId, false);
		
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		HashMap<String,String> personalSettingUseKeyMap = userPersonalHelper.getPersonalSettingUseKeys();
		personalSettingUseKeyMap.put(menuItemId, usePersonal);
		this.removeSessionAttribute(AttributeKeys.PERSONAL_SETTING_MODE_KEY);
		
		UserCacheManager userCacheManager = UserCacheManager.getOnly();
		userCacheManager.removeUser(userCode);
		return result;
	}
	
	private List<String> getPortletIdList(PersonalSetting personalSetting,String columnIndex){
		List<String> result = null;
		if ("1".equals(columnIndex)){
			result = personalSetting.getFirstColumnPortletIdList();
		}else if ("2".equals(columnIndex)){
			result = personalSetting.getSecondColumnPortletIdList();
		}else if ("3".equals(columnIndex)){
			result = personalSetting.getThirdColumnPortletIdList();
		}
		return result;
	}
	
	@PageAction
	public ViewRenderer moveUpPersonalPortlet(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		String portletId = param.get("portletId");
		String columnIndex = param.get("columnIndex");
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		List<String> portletIdList = getPortletIdList(personalSetting, columnIndex);
		for (int i=0;i < portletIdList.size();i++){
			String tempPortletId = portletIdList.get(i);
			if (portletId.equals(tempPortletId)){
				portletIdList.remove(portletId);
				portletIdList.add(i-1,portletId);
				break;
			}
		}
		result = new AjaxRenderer(SUCCESS);
		return result;
	}	

	@PageAction
	public ViewRenderer resizePortletHeight(DataParam param){
		ViewRenderer result = null;
		String height = param.get("height");
		String menuItemId = param.get("menuItemId");
		String portletId = param.get("portletId");

		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		String personalSettingMode = (String)this.getSessionAttribute(AttributeKeys.PERSONAL_SETTING_MODE_KEY);
		if ("Y".equals(personalSettingMode)){
			PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
			HashMap<String,String> portletAttrs = personalSetting.getPortletConfig(portletId);
			portletAttrs.put("height",height);
		}else{
			PortletWindowConfig portletWindowConfig = this.getPortalConfigService().getPortletWindowConfig(portletId);
			portletWindowConfig.setHeight(height);
			this.getPortalConfigService().savePortletWindowConfig(portletWindowConfig);
			String pageId = portletWindowConfig.getPageId();
			this.getPortalConfigService().refreshPorletConfig(true, pageId);
		}
		result = new AjaxRenderer(SUCCESS);
		return result;
	}
	
	@PageAction
	public ViewRenderer moveDownPersonalPortlet(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		String portletId = param.get("portletId");
		String columnIndex = param.get("columnIndex");
		
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		List<String> portletIdList = getPortletIdList(personalSetting, columnIndex);
		for (int i=0;i < portletIdList.size();i++){
			String tempPortletId = portletIdList.get(i);
			if (portletId.equals(tempPortletId)){
				portletIdList.remove(portletId);
				portletIdList.add(i+1,portletId);
				break;
			}
		}
		result = new AjaxRenderer(SUCCESS);
		return result;
	}	
	
	@PageAction
	public ViewRenderer dragdropPortlet(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		String dragPortletId = param.get("dragPortletId");
		String dragColumnIndex = param.get("dragColumnIndex");
		String dropPortletId = param.get("dropPortletId");
		String dropColumnIndex = param.get("dropColumnIndex");
		
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);

		List<String> dragPortletIdList = getPortletIdList(personalSetting, dragColumnIndex);
		if (dragPortletIdList != null && dragPortletIdList.contains(dragPortletId)){
			dragPortletIdList.remove(dragPortletId);			
		}
		
		List<String> dropPortletIdList = getPortletIdList(personalSetting, dropColumnIndex);
		if (dropPortletIdList.size() == 0){
			dropPortletIdList.add(dragPortletId);
		}else{
			for (int i=0;i < dropPortletIdList.size();i++){
				String tempPortletId = dropPortletIdList.get(i);
				if (dropPortletId.equals(tempPortletId)){
					dropPortletIdList.add(i+1,dragPortletId);
					break;
				}
			}			
		}
		
		result = new AjaxRenderer(SUCCESS);
		return result;
	}
	
	@PageAction
	public ViewRenderer deletePersonalPortlet(DataParam param){
		ViewRenderer result = null;
		String menuItemId = param.get("menuItemId");
		String portletId = param.get("portletId");
		String columnIndex = param.get("columnIndex");
		
		User user = (User)this.getUser();
		UserPersonalHelper userPersonalHelper = new UserPersonalHelper(user);
		PersonalSetting personalSetting = userPersonalHelper.getPersonalSetting(menuItemId);
		List<String> portletIdList = getPortletIdList(personalSetting, columnIndex);
		portletIdList.remove(portletId);
		
		result = new AjaxRenderer(SUCCESS);
		return result;
	}
}