
package com.agileai.portal.wsclient.uui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for modifyUser complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="modifyUser">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tokenId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userModel" type="{http://integrate.service.portal.agileai.com/}userModel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "modifyUser", propOrder = {
    "tokenId",
    "userModel"
})
public class ModifyUser {

    protected String tokenId;
    protected UserModel userModel;

    /**
     * Gets the value of the tokenId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTokenId() {
        return tokenId;
    }

    /**
     * Sets the value of the tokenId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTokenId(String value) {
        this.tokenId = value;
    }

    /**
     * Gets the value of the userModel property.
     * 
     * @return
     *     possible object is
     *     {@link UserModel }
     *     
     */
    public UserModel getUserModel() {
        return userModel;
    }

    /**
     * Sets the value of the userModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserModel }
     *     
     */
    public void setUserModel(UserModel value) {
        this.userModel = value;
    }

}
