
package com.agileai.portal.wsclient.uui;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.agileai.portal.wsclient.uui package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RemoveUser_QNAME = new QName("http://integrate.service.portal.agileai.com/", "removeUser");
    private final static QName _CreateUserResponse_QNAME = new QName("http://integrate.service.portal.agileai.com/", "createUserResponse");
    private final static QName _ActiveUserResponse_QNAME = new QName("http://integrate.service.portal.agileai.com/", "activeUserResponse");
    private final static QName _SendMail_QNAME = new QName("http://integrate.service.portal.agileai.com/", "sendMail");
    private final static QName _CreateUser_QNAME = new QName("http://integrate.service.portal.agileai.com/", "createUser");
    private final static QName _RemoveUserResponse_QNAME = new QName("http://integrate.service.portal.agileai.com/", "removeUserResponse");
    private final static QName _ActiveUser_QNAME = new QName("http://integrate.service.portal.agileai.com/", "activeUser");
    private final static QName _ModifyUser_QNAME = new QName("http://integrate.service.portal.agileai.com/", "modifyUser");
    private final static QName _ModifyPassword_QNAME = new QName("http://integrate.service.portal.agileai.com/", "modifyPassword");
    private final static QName _ModifyPasswordResponse_QNAME = new QName("http://integrate.service.portal.agileai.com/", "modifyPasswordResponse");
    private final static QName _SendMailResponse_QNAME = new QName("http://integrate.service.portal.agileai.com/", "sendMailResponse");
    private final static QName _ModifyUserResponse_QNAME = new QName("http://integrate.service.portal.agileai.com/", "modifyUserResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.agileai.portal.wsclient.uui
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NameValuePair }
     * 
     */
    public NameValuePair createNameValuePair() {
        return new NameValuePair();
    }

    /**
     * Create an instance of {@link SendMailResponse }
     * 
     */
    public SendMailResponse createSendMailResponse() {
        return new SendMailResponse();
    }

    /**
     * Create an instance of {@link ModifyUserResponse }
     * 
     */
    public ModifyUserResponse createModifyUserResponse() {
        return new ModifyUserResponse();
    }

    /**
     * Create an instance of {@link RemoveUser }
     * 
     */
    public RemoveUser createRemoveUser() {
        return new RemoveUser();
    }

    /**
     * Create an instance of {@link ModifyPassword }
     * 
     */
    public ModifyPassword createModifyPassword() {
        return new ModifyPassword();
    }

    /**
     * Create an instance of {@link ActiveUser }
     * 
     */
    public ActiveUser createActiveUser() {
        return new ActiveUser();
    }

    /**
     * Create an instance of {@link ModifyUser }
     * 
     */
    public ModifyUser createModifyUser() {
        return new ModifyUser();
    }

    /**
     * Create an instance of {@link ModifyPasswordResponse }
     * 
     */
    public ModifyPasswordResponse createModifyPasswordResponse() {
        return new ModifyPasswordResponse();
    }

    /**
     * Create an instance of {@link ActiveUserResponse }
     * 
     */
    public ActiveUserResponse createActiveUserResponse() {
        return new ActiveUserResponse();
    }

    /**
     * Create an instance of {@link ResultStatus }
     * 
     */
    public ResultStatus createResultStatus() {
        return new ResultStatus();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link UserModel }
     * 
     */
    public UserModel createUserModel() {
        return new UserModel();
    }

    /**
     * Create an instance of {@link CreateUser }
     * 
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link SendMail }
     * 
     */
    public SendMail createSendMail() {
        return new SendMail();
    }

    /**
     * Create an instance of {@link RemoveUserResponse }
     * 
     */
    public RemoveUserResponse createRemoveUserResponse() {
        return new RemoveUserResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "removeUser")
    public JAXBElement<RemoveUser> createRemoveUser(RemoveUser value) {
        return new JAXBElement<RemoveUser>(_RemoveUser_QNAME, RemoveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActiveUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "activeUserResponse")
    public JAXBElement<ActiveUserResponse> createActiveUserResponse(ActiveUserResponse value) {
        return new JAXBElement<ActiveUserResponse>(_ActiveUserResponse_QNAME, ActiveUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "sendMail")
    public JAXBElement<SendMail> createSendMail(SendMail value) {
        return new JAXBElement<SendMail>(_SendMail_QNAME, SendMail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "removeUserResponse")
    public JAXBElement<RemoveUserResponse> createRemoveUserResponse(RemoveUserResponse value) {
        return new JAXBElement<RemoveUserResponse>(_RemoveUserResponse_QNAME, RemoveUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActiveUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "activeUser")
    public JAXBElement<ActiveUser> createActiveUser(ActiveUser value) {
        return new JAXBElement<ActiveUser>(_ActiveUser_QNAME, ActiveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "modifyUser")
    public JAXBElement<ModifyUser> createModifyUser(ModifyUser value) {
        return new JAXBElement<ModifyUser>(_ModifyUser_QNAME, ModifyUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "modifyPassword")
    public JAXBElement<ModifyPassword> createModifyPassword(ModifyPassword value) {
        return new JAXBElement<ModifyPassword>(_ModifyPassword_QNAME, ModifyPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "modifyPasswordResponse")
    public JAXBElement<ModifyPasswordResponse> createModifyPasswordResponse(ModifyPasswordResponse value) {
        return new JAXBElement<ModifyPasswordResponse>(_ModifyPasswordResponse_QNAME, ModifyPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendMailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "sendMailResponse")
    public JAXBElement<SendMailResponse> createSendMailResponse(SendMailResponse value) {
        return new JAXBElement<SendMailResponse>(_SendMailResponse_QNAME, SendMailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integrate.service.portal.agileai.com/", name = "modifyUserResponse")
    public JAXBElement<ModifyUserResponse> createModifyUserResponse(ModifyUserResponse value) {
        return new JAXBElement<ModifyUserResponse>(_ModifyUserResponse_QNAME, ModifyUserResponse.class, null, value);
    }

}
