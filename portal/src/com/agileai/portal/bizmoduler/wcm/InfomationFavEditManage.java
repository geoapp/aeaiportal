package com.agileai.portal.bizmoduler.wcm;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface InfomationFavEditManage
        extends StandardService {

	List<DataRow> findListRecords(DataParam param);

	List<DataRow> findInfoRecords(DataParam param);
}
