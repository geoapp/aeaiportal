package com.agileai.portal.bizmoduler.wcm;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.domain.core.User;

public interface InfomationManage
        extends TreeAndContentManage {
	public static final class EditType{
		public static final String Profile = "profile";
		public static final String Content = "content";
	}
	
	public static final class AuthType{
		public static final String Public = "PUBLIC";
		public static final String Protected = "PROTECTED";
		public static final String Private = "PRIVATE";

	}
	
    public static final class EditMode {
    	public static final String Write = "Write";
    	public static final String Publish = "Publish";
    }
    public static final class BizActionType{
    	public static final String Submit = "submit";
    	public static final String UnSubmit = "unsubmit";
    	public static final String Approve = "approve";
    	public static final String UnApprove = "unapprove";
    }
    public static final class InfoState{
    	public static final String Draft = "draft";
    	public static final String WaitePublish = "waitepublish";
    	public static final String Published = "published";
    }
	
	public static class SortPolicy{
		public static final String Top8PublishTime = "Top8PublishTime";
		public static final String PublishTimeDesc = "PublishTimeDesc";
		public static final String Top8InfomationSort = "Top8InfomationSort";
		public static final String InfomationSort = "InfomationSort";
		public static final String ReadCountDesc = "ReadCountDesc";
	}
	
	void modifyInfomationState(String infomationId,String bizActionType,User user);
	void saveInfomationContent(String infomationId,String content);
	String getInfomationContent(String infomationId);
	DataRow getContentRecord(String infomationId);
	
	DataRow getCurrentInfoRecord(String infomationId);
	List<DataRow> getContentRecords(String infomationId);
	
	List<DataRow> findContentRecords(String columnId,String state,String sortPolicy);
	
	List<DataRow> findInfomationKeyWordsRelations(String infomationId);
	void addInfomationKeyWordsRelation(String infomationId,List<DataRow> relRecords);
	void delInfomationKeyWordsRelation(String infomationId,String keyWordsId);
	
	void increaseReadCount(String infomationId);
	int findReadCount(String infomationId);
	int findInfoReviewCount(String infomationId);
	
	List<DataRow> findRelativeInfomationRecordsByKeyWordsId(String keyWordsId,String state,String sortPolicy);
	List<DataRow> findRelativeInfomationRecordsBysearchWord(String searchWord,String state,String sortPolicy);
	
	List<DataRow> findRefInfoRecords(List<String> infoIdList);
	void updateInfoRelInfoIds(String infoId,String relInfoIdsJsonString);
	List<DataRow> getLastestNewsRecords(DataParam param);
}
