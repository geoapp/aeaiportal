package com.agileai.portal.bizmoduler.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface PtPortletTemptManage
        extends StandardService {
	public void updateContent(DataParam param);
	public String retrieveContent(String templateId);
}
