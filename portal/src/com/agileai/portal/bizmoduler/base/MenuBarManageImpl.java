package com.agileai.portal.bizmoduler.base;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeManageImpl;
import com.agileai.hotweb.domain.core.Resource;
import com.agileai.portal.driver.service.PortalConfigService;
import com.agileai.portal.filter.UserCacheManager;
import com.agileai.util.MapUtil;
import com.agileai.util.StringUtil;

public class MenuBarManageImpl extends TreeManageImpl implements MenuBarManage{
	public MenuBarManageImpl(){
		super();
		this.idField = "MENU_ID";
		this.nameField = "MENU_NAME";
		this.parentIdField = "MENU_PID";
		this.sortField = "MENU_SORT";
	}
	public void insertChildRecord(DataParam param) {
		String parentId = param.get(idField);
		String newMenuSort = String.valueOf(this.getNewMaxSort(parentId));
		param.put("CHILD_"+sortField,newMenuSort);
		param.put("CHILD_"+idField,KeyGenerator.instance().genKey());
		param.put("CHILD_"+parentIdField,param.get(idField));
		String statementId = this.sqlNameSpace+"."+"insertTreeRecord";
		String pageId = null;
		if ("L".equals(param.get("CHILD_MENU_TYPE"))){
			pageId = KeyGenerator.instance().genKey();
			param.put("CHILD_PAGE_ID",pageId);	
		}
		this.daoHelper.insertRecord(statementId, param);
		
		if ("L".equals(param.get("CHILD_MENU_TYPE"))){
			statementId = "page.createLayout";
			DataParam layoutParam = new DataParam();
			layoutParam.put("PAGE_ID",pageId);
			layoutParam.put("MAIN_LAYOUT",param.get("MAIN_LAYOUT"));
			this.daoHelper.insertRecord(statementId, layoutParam);
		}
		
		PortalConfigService portalConfigService = (PortalConfigService)this.lookupService("PortalConfigService");
		String navigaterId = param.get("NAV_ID");
		portalConfigService.refreshMenuBar(navigaterId);
		
		UserCacheManager.getOnly().truncateUsers();
	}
	public void deleteCurrentRecord(String currentId) {
		DataParam queryParam = new DataParam(this.idField,currentId);
		DataRow row = this.queryCurrentRecord(queryParam);
		String menuType = row.stringValue("MENU_TYPE");
		if ("F".equals(menuType)){
			String statementId = this.sqlNameSpace+"."+"deleteTreeRecord";
			this.daoHelper.deleteRecords(statementId, currentId);	
		}else{
			String sqlNameSpace = "page";
			String statementId=sqlNameSpace+"."+"deletePagePortletsByPageId";
			this.daoHelper.deleteRecords(statementId, currentId);

			statementId=sqlNameSpace+"."+"deletePageChildLayoutsByPageId";
			this.daoHelper.deleteRecords(statementId, currentId);
			
			statementId=sqlNameSpace+"."+"deleteLayoutByPageId";
			this.daoHelper.deleteRecords(statementId, currentId);
			
			PortalConfigService portalConfigService = (PortalConfigService)this.lookupService("PortalConfigService");
			portalConfigService.refreshPorletConfig(true,currentId);
			
			statementId = this.sqlNameSpace+"."+"deleteTreeRecord";
			this.daoHelper.deleteRecords(statementId, currentId);
		}
		
		DataParam param = new DataParam();
		param.put("RES_TYPE",Resource.Type.Menu);
		param.put("RES_ID",currentId);
		
		String statementId = "SecurityAuthorizationConfig.delRoleAuthRelation";
		this.daoHelper.deleteRecords(statementId, param);
		
		statementId = "SecurityAuthorizationConfig.delUserAuthRelation";
		this.daoHelper.deleteRecords(statementId, param);
		
		statementId = "SecurityAuthorizationConfig.delGroupAuthRelation";
		this.daoHelper.deleteRecords(statementId, param);
	}
	public boolean isCodeDuplicate(String navId,String menuId,String menuCode) {
		boolean result = false;
		String statementId = this.sqlNameSpace+".queryMenuRecord";
		DataParam param = new DataParam();
		if (StringUtil.isNotNullNotEmpty(menuId)){
			param.put("menuId",menuId);
		}
		if (StringUtil.isNotNullNotEmpty(navId)){
			param.put("navId",navId);
		}
		param.put("menuCode",menuCode);
		DataRow dataRow = this.daoHelper.getRecord(statementId, param);
		if (!MapUtil.isNullOrEmpty(dataRow)){
			result = true;
		}
		return result;
	}
}
