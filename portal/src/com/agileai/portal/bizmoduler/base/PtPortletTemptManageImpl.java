package com.agileai.portal.bizmoduler.base;

import java.util.HashMap;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class PtPortletTemptManageImpl
        extends StandardServiceImpl
        implements PtPortletTemptManage {
	private static HashMap<String,String> TemplateCache = new HashMap<String,String>();
	
    public PtPortletTemptManageImpl() {
        super();
    }
    
    
	@Override
	public void updateContent(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateContent";
		this.daoHelper.updateRecord(statementId, param);
		String templateId = param.get("TEMPT_ID");
		TemplateCache.remove(templateId);
	}

	@Override
	public String retrieveContent(String templateId) {
		String result = null;
		if (!TemplateCache.containsKey(templateId)){
			DataParam queryParam = new DataParam("TEMPT_ID",templateId);
			PtPortletTemptManage ptStaticDataManage = this.lookupService(PtPortletTemptManage.class);
			DataRow record = ptStaticDataManage.getRecord(queryParam);
			String responseText = record.stringValue("TEMPT_CONTENT");		
			TemplateCache.put(templateId, responseText);
		}
		result = TemplateCache.get(templateId);
		return result;
	}
}
