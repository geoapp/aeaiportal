package com.agileai.portal.bizmoduler.base;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface PtThemeManage extends StandardService{
	List<DataRow> findPersonalRecords(DataParam param);
}
