package com.agileai.portal.bizmoduler.base;

public class PtHandler {
	public static class AcccessType{
		public static String SYSTEM = "system";
		public static String MANAGE = "manage";
		public static String AUTHED = "authed";
		public static String PUBLIC = "public";
	}
	
	private String handlerId = null;
	private String acccessType = null;
	
	public String getHandlerId() {
		return handlerId;
	}
	public void setHandlerId(String handlerId) {
		this.handlerId = handlerId;
	}
	public String getAcccessType() {
		return acccessType;
	}
	public void setAcccessType(String acccessType) {
		this.acccessType = acccessType;
	}
}