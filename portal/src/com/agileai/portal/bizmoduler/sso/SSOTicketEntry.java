package com.agileai.portal.bizmoduler.sso;

public class SSOTicketEntry {
	private String paramCode = null;
	private String paramValue = null;
	private String valueExpression = null;
	private boolean encription = false;
	
	public String getParamCode() {
		return paramCode;
	}
	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}
	public String getParamValue() {
		return paramValue;
	}
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	public String getValueExpression() {
		return valueExpression;
	}
	public void setValueExpression(String valueExpression) {
		this.valueExpression = valueExpression;
	}
	public boolean isEncription() {
		return encription;
	}
	public void setEncription(boolean encription) {
		this.encription = encription;
	}
}
