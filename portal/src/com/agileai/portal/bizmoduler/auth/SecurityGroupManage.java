package com.agileai.portal.bizmoduler.auth;

import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;

public interface SecurityGroupManage
        extends TreeAndContentManage {
	public void updateSecurityUserFavorites(String userCode,String userFavorites);
}
